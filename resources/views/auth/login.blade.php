@extends('auth.layouts.registration')
@section('content')
<section class="login-block">
   <div class="container logcontainer">
   <div class="row">
      <div class="col-md-4 login-sec">
         <h2 class="text-center">Login Now</h2>
         <form class="login-form" method="POST" action="{{ route('user.frontlogin') }}">
           @csrf
            <div class="form-group">
               <label for="exampleInputEmail1" class="text-uppercase">Username</label>
               <input class="form-control @error('email') is-invalid @enderror" name="email" placeholder="" id="email" type="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
               @error('email')
               <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
            </div>
            <div class="form-group">
               <label for="exampleInputPassword1" class="text-uppercase">Password</label>
               <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
               @error('password')
               <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
            </div>
            <div class="form-check" style="padding: 0;">
               <label class="form-check-label">
               <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} style="margin-left: -3.25rem;">
               <small>Remember Me</small>
               </label>
               <button type="submit" class="btn btn-login float-right">Submit</button>
            </div>
         </form>
         <div class="copy-text"></div>
      </div>
      <div class="col-md-8 banner-sec">
         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox" style="border-radius: 0 9px 10px 0; width: 102%;">
               <div class="carousel-item active">
                  <img class="d-block img-fluid" src="{{ asset('frontassets/images/login1.jpg') }}" alt="First slide" style="height: 560px;">
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
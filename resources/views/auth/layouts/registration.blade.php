<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>|| KALAKSHI ||</title>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style.css') }}">
	
    
</head>
<body>
<!--Body Start -->
<section id="mainbody">
    @yield('content')
</section>

<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>About Kalakshi</h4>
				<ul>
					<li><a href="">About Us</a></li>
					<li><a href="">Contact Us</a></li>
					<li><a href="">Blog</a></li>
					<li><a href="">Testimonial</a></li>
					<li><a href="">Press</a></li>
					<li><a href="">Lookbook</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>My Account</h4>
				<ul>
					<li><a href="">Login</a></li>
					<li><a href="">Shopping Bag</a></li>
					<li><a href="">Wish List</a></li>
					<li><a href="">Order History</a></li>
					<li><a href="">Order Tracking</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>WE ACCEPT</h4>
				<span><img src="{{ asset('frontassets/images/payment.png') }}"></span>
				<h4>NEWSLETTER IN YOUR INBOX</h4>
				<div class="newsBox">
					<label>
						<input type="text" name="">
					</label>
					
					<buton>Subscribe</buton>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>GET IN TOUCH</h4>
				<p><a href="tel:+91 993300993300">+91 993300993300</a></p>
				<p><a href="tel:+91 993300993300">+91 993300993300</a></p>
				<h4>EMAIL US ON</h4>
				<p><a href="mailto:example@gmail.com">example@gmail.com</a></p>
				<h4>FOLLOW US</h4>
				<p>
					<a href=""><img src="{{ asset('frontassets/images/f_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/t_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/ins_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/w_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/y_icon.png') }}"></a>
				</p>
			</div>
		</div>
	</div>
</footer>
<!-- Footer End -->

<!-- Copy Right -->
<section id="copyright">
	<div class="container-fluid">
		<p>Copyright &copy; 2010-2020 Kalakshi Collections. All rights reserved</p>
	</div>
</section>
<!-- Copy Right End -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type="text/javascript" src="{{ asset('frontassets/js/main.js') }}"></script>
@yield('scriptjs')
</body>
</html>
@include('blog_header')


<div class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">News</div>
                </div>
				@foreach($category_blog_details as $cat_blog)
                <div class="row pb-4">
                    <div class="col-md-5">
                        <div class="fh5co_hover_news_img">
                            <div class="fh5co_news_img"><img src="{{ url('/') }}/public/uploads/blog_images/{{ $cat_blog->blog_image }}" alt=""/></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="col-md-7 animate-box">
                        <a href="{{ url('/blog_single') }}/{{ $cat_blog->blog_slug }}" class="fh5co_magna py-2"> {!! substr(strip_tags($cat_blog->blog_title),0, 50) !!}...</a> <a href="#" class="fh5co_mini_time py-3"> Admin -
                        {{ date('M d, Y',strtotime($cat_blog->blog_added_on)) }}</a>
                        <div class="fh5co_consectetur">
							{!! substr(strip_tags($cat_blog->blog_description),0, 250) !!}<a href="{{ url('/blog_single') }}/{{ $cat_blog->blog_slug }}">[...]</a>
                        </div>
                    </div>
                </div>
				@endforeach
            </div>
            <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                <!--<div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Tags</div>
                </div>
                <div class="clearfix"></div>
                <div class="fh5co_tags_all">
                    <a href="#" class="fh5co_tagg">Business</a>
                    <a href="#" class="fh5co_tagg">Technology</a>
                    <a href="#" class="fh5co_tagg">Sport</a>
                    <a href="#" class="fh5co_tagg">Art</a>
                    <a href="#" class="fh5co_tagg">Lifestyle</a>
                    <a href="#" class="fh5co_tagg">Three</a>
                    <a href="#" class="fh5co_tagg">Photography</a>
                    <a href="#" class="fh5co_tagg">Lifestyle</a>
                    <a href="#" class="fh5co_tagg">Art</a>
                    <a href="#" class="fh5co_tagg">Education</a>
                    <a href="#" class="fh5co_tagg">Social</a>
                    <a href="#" class="fh5co_tagg">Three</a>
                </div>-->
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom pt-3 py-2 mb-4">Most Popular</div>
                </div>
				@foreach($popular_posts as $pop_posts)
                <div class="row pb-3">
                    <div class="col-5 align-self-center">
					<a href="{{ url('/blog_single') }}/{{ $pop_posts->blog_slug }}" style="text-decoration: none;">
                        <img src="{{ url('/') }}/public/uploads/blog_images/{{ $pop_posts->blog_image }}" alt="img" class="fh5co_most_trading"/>
					</a>
                    </div>
                    <div class="col-7 paddding">
                        <div class="most_fh5co_treding_font">
						<a href="{{ url('/blog_single') }}/{{ $pop_posts->blog_slug }}" style="text-decoration: none;color: #222 !important;">
							{!! substr(strip_tags($pop_posts->blog_title),0, 50) !!}...</div>
					    </a>
                        <div class="most_fh5co_treding_font_123">{{ date('M d, Y',strtotime($pop_posts->blog_added_on)) }}</div>
                    </div>
                </div>
				@endforeach
            </div>
        </div>
        <div class="row mx-0">
				{{ $category_blog_details->links() }}
            <!--<div class="col-12 text-center pb-4 pt-4">
                <a href="#" class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp; Previous</a>
                <a href="#" class="btn_pagging">1</a>
                <a href="#" class="btn_pagging">2</a>
                <a href="#" class="btn_pagging">3</a>
                <a href="#" class="btn_pagging">...</a>
                <a href="#" class="btn_mange_pagging">Next <i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp; </a>
             </div>-->
        </div>
    </div>
</div>
<!--<div class="container-fluid pb-4 pt-5">
    <div class="container animate-box">
        <div>
            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Trending</div>
        </div>
        <div class="owl-carousel owl-theme" id="slider2">
            <div class="item px-2">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img"><img src="images/39-324x235.jpg" alt=""/></div>
                    <div>
                        <a href="#" class="d-block fh5co_small_post_heading"><span class="">The top 10 best computer speakers in the market</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i> Oct 16,2017</div>
                    </div>
                </div>
            </div>
            <div class="item px-2">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img"><img src="images/joe-gardner-75333.jpg" alt=""/></div>
                    <div>
                        <a href="#" class="d-block fh5co_small_post_heading"><span class="">The top 10 best computer speakers in the market</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i> Oct 16,2017</div>
                    </div>
                </div>
            </div>
            <div class="item px-2">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img"><img src="images/ryan-moreno-98837.jpg" alt=""/></div>
                    <div>
                        <a href="#" class="d-block fh5co_small_post_heading"><span class="">The top 10 best computer speakers in the market</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i> Oct 16,2017</div>
                    </div>
                </div>
            </div>
            <div class="item px-2">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img"><img src="images/seth-doyle-133175.jpg" alt=""/></div>
                    <div>
                        <a href="#" class="d-block fh5co_small_post_heading"><span class="">The top 10 best computer speakers in the market</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i> Oct 16,2017</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

@include('blog_footer')
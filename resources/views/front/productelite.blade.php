@extends('front.layouts.app')

    @section('content')

    <!-- Spl Sree Product -->
    <section id="splSreePro">
        <div class="container-fluid">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box_headline">
                    <h2>{{$productsdetails[0]->product_name}}</h2>
                </div>
            </div>
            @php
            if(isset($productStory) && !empty($productStory)){
            foreach($productStory as $key => $story){
                
                if($key%2 == 0){
            @endphp    
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="imgProduct" style="min-height: 650px;max-height: 780px;overflow: hidden;">
                                <img src="{{ asset('picture/productstory/'.$story->story_image ) }}">
                            </div>
                        </div>  
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="outerTextDiv">
                                <div class="innerTextDiv">@php echo $story->description;  @endphp</div>
                            </div>
                        </div>
                    </div>
                </div>
             @php   
                }else{
             @endphp    
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="outerTextDiv">
                                <div class="innerTextDiv">@php echo $story->description;  @endphp</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="imgProduct" style="min-height: 650px;max-height: 780px;overflow: hidden;">
                                <img src="{{ asset('picture/productstory/'.$story->story_image ) }}" >
                            </div>
                        </div>  
                    </div>
                </div>

            @php     
                }
                
            }}
            
            @endphp
        </div>
    </section>
    <!-- Spl Sree Product End-->

	<!-- addtoCart -->
	@php

         $product_images = [];
         $product_images = json_decode($productsdetails[0]->product_images);
         $product_images =  $product_images[0];
                    
    @endphp
	<section id="addtoCart">
        <input type="hidden" name="product_id" id="product_id" value="{{isset($productsdetails[0]->id) ? $productsdetails[0]->id : '' }}">
        <input type="hidden" name="product_price" id="product_price" value="{{isset($productsdetails[0]->product_price) ? $productsdetails[0]->product_price : '' }}">
        <input type="hidden" name="product_offer" id="product_offer" value="{{isset($productsdetails[0]->offer['percentage']) ? $productsdetails[0]->offer['percentage'] : '' }}">

		<div class="container">
			<div style="width:100%;float:left;padding:10px;background:#61d6ff;">
				<div style="width:100%;float:left;background:url({{ asset('frontassets/images/addtocartbg.jpg') }})center center no-repeat;background-size:cover;padding:10px;">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<img src="{{ asset('picture/product/'.$product_images ) }}" style="width:100%;">
					</div>
					<div class="col-md-8 col-sm-8 col-xs-12 cartDeatils">
						<h4>{{$productsdetails[0]->product_name}}</h4>
						<ul class="splTowInfo">
							<li>
								<b>Blouse Stitching</b>
								<label><input type="checkbox" name="">Measurements + 1200.00/-</label>
							</li>
							<li>
								<b>Petticoat Stitching</b>
								<label><input type="checkbox" name="">Measurements + 600.00/-</label>
							</li>
						</ul>
						<div class="addCartPrice">
							<ul>
								<li><b>₹{{$productsdetails[0]->product_price}}/-</b></li>
								<li><span>{{(isset($productsdetails[0]->offer['percentage']) && !empty($productsdetails[0]->offer['percentage']))?$productsdetails[0]->offer['percentage']:'0'}}% Off</span></li>
								<li id="add_to_bag_li"><a href="javascript:void(0);" id="add_to_bag" name="add_to_bag"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to Bag</a></li>
							</ul>
						</div>
						<div class="dispatchtext">
							<span class=""><img src="{{ asset('frontassets/images/delvery.png') }}"> Time To Dispatch : 10 to 12 days</span>
						</div>
						<div class="productInfo">
							<p><span>Product Code</span><b>{{$productsdetails[0]->product_code}}</b></p>
							<p><span>Work</span> <b>Printed</b></p>
							<p><span>Blouse Fabric</span> <b>Dupion-Silk</b></p>
							<p><span>Blouse Type</span> <b>Unstitched</b></p>
						</div>

						<p class="codNote">Stitching Requests Cannot Be Accepted on COD Orders.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- addtoCart End -->

	
    <!-- Similar Product -->
    @if(isset($similerproducts) && !empty($similerproducts))
    <section id="deydeals">
        <div class="container-fluid">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box_headline">
                    <h2>SIMILAR TYPE OF PRODUCT</h2>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    
                    @foreach($similerproducts as $product)

                    @php
                    
                    $product_images = [];
                    $product_images = json_decode($product->product_images);
                    $product_images =  $product_images[0];

                    $product_price = $product->product_price;

                    if(isset($product->offer['percentage']) && !empty($product->offer['percentage'])){
                        $product_price = $product_price - (($product_price * $product->offer['percentage'])/100);
                    }

                    @endphp
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="dayoffer">
                            <img src="{{ asset('picture/product/'.$product_images ) }}">
                            <div class="dayoffer_tham">
                                <span>{{ (isset($product->offer['percentage']) && !empty($product->offer['percentage']))?$product->offer['percentage'].'% Off' : ''}}</span>
                                <a href="">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- Similar Product End -->
	 
    @endsection   

    @section('scriptjs')

    <script type="text/javascript">
        var base_url = window.location.origin;
        $(document).on('click','#add_to_bag',function(e) {
            var product_id = $("#product_id").val();
            var product_price = $("#product_price").val();
            var product_offer = $("#product_offer").val();
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: base_url + "/addToCart",
                type: "post",
                datatype: "json",
                data: { 'product_id':product_id, 'product_price':product_price, 'product_offer':product_offer},
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log(response.success);
                    if(response.success == 1){
                        $("#add_to_bag_li").html('<a href="'+ base_url + '/carts" id="go_to_bag" name="go_to_bag"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Go to Bag</a>');
                    }else if(response.success == 2){
                        window.location.href = base_url + "/login";
                    }
                }
            });


        });

    </script>

    @endsection
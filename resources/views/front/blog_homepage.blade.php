@include('blog_header')


<div class="container-fluid paddding mb-5">
    <div class="row mx-0">
	@foreach($big_thumb as $big)
        <div class="col-md-6 col-12 paddding animate-box" data-animate-effect="fadeIn">
            <div class="fh5co_suceefh5co_height"><img src="{{ url('/') }}/public/uploads/blog_images/{{ $big->blog_image }}" alt="img"/>
                <div class="fh5co_suceefh5co_height_position_absolute"></div>
                <div class="fh5co_suceefh5co_height_position_absolute_font">
                    <div class=""><a href="#" class="color_fff"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ date('M d, Y',strtotime($big->blog_added_on)) }}
                    </a></div>
                    <div class=""><a href="{{ url('/blog_single') }}/{{ $big->blog_slug }}" class="fh5co_good_font">{!! substr(strip_tags($big->blog_title),0, 50) !!}...</a></div>
                </div>
            </div>
        </div>
		@endforeach
        <div class="col-md-6">
            <div class="row">
			@foreach($thumb_blogs as $thblog)
                <div class="col-md-6 col-6 paddding animate-box" data-animate-effect="fadeIn">
                    <div class="fh5co_suceefh5co_height_2"><img src="{{ url('/') }}/public/uploads/blog_images/{{ $thblog->blog_image }}" alt="img"/>
                        <div class="fh5co_suceefh5co_height_position_absolute"></div>
                        <div class="fh5co_suceefh5co_height_position_absolute_font_2">
                            <div class=""><a href="#" class="color_fff"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ date('M d, Y',strtotime($thblog->blog_added_on)) }} </a></div>
                            <div class=""><a href="{{ url('/blog_single') }}/{{ $thblog->blog_slug }}" class="fh5co_good_font_2">{!! substr(strip_tags($thblog->blog_title),0, 50) !!}...</a></div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pt-3">
    <div class="container animate-box" data-animate-effect="fadeIn">
        <div>
            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Trending Blogs</div>
        </div>
        <div class="owl-carousel owl-theme js" id="slider1">
		@foreach($trending_blogs as $trending)
            <div class="item px-2">
                <div class="fh5co_latest_trading_img_position_relative">
                    <div class="fh5co_latest_trading_img"><img src="{{ url('/') }}/public/uploads/blog_images/{{ $trending->blog_image }}" alt=""
                                                           class="fh5co_img_special_relative"/></div>
                    <div class="fh5co_latest_trading_img_position_absolute"></div>
                    <div class="fh5co_latest_trading_img_position_absolute_1">
                        <a href="{{ url('/blog_single') }}/{{ $trending->blog_slug }}" class="text-white">{!! substr(strip_tags($trending->blog_title),0, 50) !!}...</a>
                        <div class="fh5co_latest_trading_date_and_name_color"> {{ date('M d, Y',strtotime($trending->blog_added_on)) }}</div>
                    </div>
                </div>
            </div>
			@endforeach
        </div>
    </div>
</div>
<div class="container-fluid pb-4 pt-5">
    <div class="container animate-box">
        <div>
            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">News</div>
        </div>
        <div class="owl-carousel owl-theme" id="slider2">
		@foreach($news_blog_1 as $news1)
            <div class="item px-2">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img"><img src="{{ url('/') }}/public/uploads/blog_images/{{ $news1->blog_image }}" alt=""/></div>
                    <div>
                        <a href="{{ url('/blog_single') }}/{{ $news1->blog_slug }}" class="d-block fh5co_small_post_heading"><span class="">{!! substr(strip_tags($news1->blog_title),0, 50) !!}...</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i> {{ date('M d, Y',strtotime($news1->blog_added_on)) }}</div>
                    </div>
                </div>
            </div>
           @endforeach
        </div>
    </div>
</div>

<div class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">News</div>
                </div>
				@foreach($news_blog_2 as $news2)
                <div class="row pb-4">
                    <div class="col-md-5">
                        <div class="fh5co_hover_news_img">
                            <div class="fh5co_news_img"><img src="{{ url('/') }}/public/uploads/blog_images/{{ $news2->blog_image }}" alt=""/></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="col-md-7 animate-box">
                        <a href="{{ url('/blog_single') }}/{{ $news2->blog_slug }}" class="fh5co_magna py-2">{!! substr(strip_tags($news2->blog_title),0, 50) !!}...</a> 
						<p><a href="single.html" class="fh5co_mini_time py-3"> {{ date('M d, Y',strtotime($news2->blog_added_on)) }}</a></p>
                        <div class="fh5co_consectetur">{!! substr(strip_tags($news2->blog_description),0, 200) !!}...</div>
                    </div>
                </div>
               @endforeach
            </div>
            @include('blog_home_sidebar')
        </div>
        <!--<div class="row mx-0 animate-box" data-animate-effect="fadeInUp">
            <div class="col-12 text-center pb-4 pt-4">
                <a href="#" class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp; Previous</a>
                <a href="#" class="btn_pagging">1</a>
                <a href="#" class="btn_pagging">2</a>
                <a href="#" class="btn_pagging">3</a>
                <a href="#" class="btn_pagging">...</a>
                <a href="#" class="btn_mange_pagging">Next <i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp; </a>
             </div>
        </div>-->
    </div>
</div>

@include('blog_footer')
@extends('front.layouts.app')

    @section('content')
    	<!-- Topbar -->
	<section id="topbar">
		<div class="container-fluid">
			<a href="">Partner with us</a>
		</div>
		<div class="clearfix"></div>
	</section>
	<!-- Topbar End -->

	<!-- Menu Bar -->
	<section id="menubar_section">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="leftbar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
					<a class="navbar-brand" href="#">KALAKSHI</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="#">Home</a></li>
						<li class="dropdown dropdown-large"><a href="" class="dropdown-toggle" data-toggle="dropdown">Aadi Kalakshi</a>
							<!---------------------------------------------------------------------------------------------------------->
							<ul class="dropdown-menu dropdown-menu-large row">
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 1</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li class="dropdown-header">Saree No 2</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
									</ul>
								</li>
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 3</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li><img class="img-responsive" src="{{ asset('frontassets/images/sari_1.jpg') }}" /></li>
									</ul>
								</li>
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 4</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li class="dropdown-header">Saree No 5</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
									</ul>	
								</li>
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 6</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li><img class="img-responsive" src="{{ asset('frontassets/images/sari_1.jpg') }}" /></li>
									</ul>
								</li>
							</ul>
							<!---------------------------------------------------------------------------------------------------------->
						</li>
						<li><a href="">Swayam Kalakshii</a></li>
						<li><a href="">Umang Kalakshii</a></li>
						<li><a href="">Kalakshetra</a></li>
						<li><a href="">Blogs</a></li>
						<li><a href="">Leather</a></li>
						<li><a href="">Contacts</a></li>
					</ul>
				</div>
				
			</div>
			
		</nav>
	</section>
	<div class="clearfix"></div>
	<!-- Menu Bar End -->

	<!-- inner-page-content-start -->
	<div class="container-fluid inner-banner text-center">
		<h1>Cart</h1>
	</div>
	<section class="container-fluid my-5">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">

				<div class="cart-table table-responsive mb--40">
	                  <table class="table">
	                    <!-- Head Row -->
	                    <thead>
	                      <tr>
	                        <th class="pro-remove"></th>
	                        <th class="pro-thumbnail">Image</th>
	                        <th class="pro-title">Product</th>
	                        <th class="pro-price">Price</th>
	                        <th class="pro-quantity">Quantity</th>
	                        <th class="pro-subtotal">Total</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                      	<!-- Product Row -->
	                      	@if(isset($orders) && !empty($orders) && count($orders))
	                      	@foreach($orders[0]->productOrderItem as $key=>$val)
	                      	@php $product_image = json_decode($val->productDetails->product_images); @endphp
	                      	<tr>
		                        <td class="pro-remove"><a href="#"><i class="fa fa-trash"></i></a>
		                        </td>
		                        <td class="pro-thumbnail"><a href="#"><img src="{{ asset('picture/product/'.$product_image[0]) }}" alt="Product"></a></td>
		                        <td class="pro-title"><a href="#">{{ isset($val->productDetails->product_name)?$val->productDetails->product_name:''}}</a></td>
		                        <td class="pro-price"><span>₹{{ isset($val->productDetails->product_price)?$val->productDetails->product_price:'0'}}</span></td>
		                        <td class="pro-quantity">
		                          <div class="pro-qty">
		                            <div class="count-input-block">
		                              <input type="number" class="form-control text-center" value="{{isset($val->quantity)?$val->quantity:'0'}}" readonly>
		                            </div>
		                          </div>
		                        </td>
		                        <td class="pro-subtotal"><span>₹{{ (isset($val->productDetails->product_price) && isset($val->quantity) ) ? ($val->productDetails->product_price * $val->quantity) : 0 }}</span></td>
	                      	</tr>
	                      	@endforeach
	                      	@endif
	                      	<!-- Product Row -->
	                      
	                      <!-- Discount Row  -->
	                      <tr>
	                        <td colspan="6" class="actions text-right">
	                          <div class="cart-summary-button">
	                            <button class="btn main-button btn-outlined mr-3">Update Cart</button>
	                  <a href="" class="btn btn-reveal-right">Checkout</a>
	                  
	                </div>
	                         
	                        </td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </div>

			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="cart-price-details">
	              <!-- <div class="col-lg-6 col-12 d-flex"> -->
	                <h4><span>Cart Summary</span></h4>
	              <div class="cart-summary">
	                <div class="cart-summary-wrap">
	                  
	                  <p>Sub Total <span class="text-primary">${{ isset($orders[0]->total_amount) ? $orders[0]->total_amount : 0 }}</span></p>
	                  <p>Shipping Cost <span class="text-primary">${{ (isset($orders[0]->total_amount) && isset($orders[0]->tax_percentage) ) ? (($orders[0]->total_amount * $orders[0]->tax_percentage)/100) : 0 }}</span></p>
	                  <h2>Grand Total <span class="text-primary">${{ (isset($orders[0]->total_amount) && isset($orders[0]->tax_percentage) ) ? ($orders[0]->total_amount + (($orders[0]->total_amount * $orders[0]->tax_percentage)/100) ) : 0}}</span></h2>

	                  <div class="input-group d-flex mt-5">
	                              <input type="text" class="form-control" placeholder="Coupon code">
	                              <div class="input-group-append">
	                                <button class="btn aply-cpn-btn" type="button">
	                                  Apply coupon
	                                </button>
	                              </div>
	                            </div>
	                </div>
	                <!-- <div class="cart-summary-button">
	                  <a href="checkout.html" class="checkout-btn c-btn btn--primary">Checkout</a>
	                  <button class="update-btn c-btn btn-outlined">Update Cart</button>
	                </div> -->

	                <div class="coupon-block justify-content-between align-items-center">
	                            <!-- <div class="coupon-text">
	                              
	                              <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code">
	                            </div>
	                            <div class="coupon-btn">
	                              <input type="submit" class="btn btn-outlined" name="apply_coupon" value="Apply coupon">
	                            </div> -->

	                            
	                          </div>
	              </div>
	              
	            <!-- </div> -->
	            </div>
			</div>
		</div>
	</section>
    @endsection

    @section('scriptjs')
    <script>
		function openCity(evt, cityName) {
		  var i, tabcontent, tablinks;
		  tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }
		  tablinks = document.getElementsByClassName("tablinks");
		  for (i = 0; i < tablinks.length; i++) {
		    tablinks[i].className = tablinks[i].className.replace(" active", "");
		  }
		  document.getElementById(cityName).style.display = "block";
		  evt.currentTarget.className += " active";
		}

		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
	</script>

	@endsection
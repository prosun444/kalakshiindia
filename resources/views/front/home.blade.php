@extends('front.layouts.app')

@section('stylecss')
<style>
.owl-nav button {
    position: absolute;
    top: 50%;
    background-color: #000;
    color: #fff;
    margin: 0;
    transition: all 0.3s ease-in-out;
  }
  .owl-nav button.owl-prev {
    left: 0;
  }
  .owl-nav button.owl-next {
    right: 0;
  }
  
  .owl-dots {
    text-align: center;
    padding-top: 15px;
  }
  .owl-dots button.owl-dot {
    width: 15px;
    height: 15px;
    border-radius: 50%;
    display: inline-block;
    background: #ccc;
    margin: 0 3px;
  }
  .owl-dots button.owl-dot.active {
    background-color: #000;
  }
  .owl-dots button.owl-dot:focus {
    outline: none;
  }
  .owl-nav button {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      background: rgba(255, 255, 255, 0.38) !important;
  }
  span {
      font-size: 70px;    
      position: relative;
      top: -5px;
  }
  .owl-nav button:focus {
      outline: none;
  }
  .owl-carousel .owl-item img{
    height: 280px;
  }
</style>

@endsection 

    @section('content')

	<!-- Banner -->
	<section id="banner">
		<div class="container-fluid">
			<img src="{{ asset('frontassets/images/banner.jpg') }}" class="bannerImg">
		</div>
	</section>
	<!-- Banner End -->

	<!-- Deals -->
	<section id="deydeals">
		<div class="container-fluid">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box_headline">
					<h2>DEALS OF THE DAYS</h2>
				</div>
			</div>

            <div class="col-md-12 col-sm-12 col-xs-12 owl-slider">
                <div id="carousel" class="owl-carousel">
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/sari_1.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/d056c611df3e27ca83ce9d17aef025c5.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/kota104_Green-600x576.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/6b0a407f303142171e85ecded611da62.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/download.jpeg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/india-traditional-saree-14393822.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/sari_1.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                    <div class="item">
                        <div class="dayoffer">
							<img src="{{ asset('frontassets/images/india-traditional-saree-14393822.jpg') }}">
							<div class="dayoffer_tham">
								<span>30% Off</span>
								<a href="">Shop Now</a>
							</div>
						</div>
                    </div>
                </div>
            </div>
		</div>
	</section>
	<!-- Deals End -->

	<!-- Deals -->
	<section id="deydeals">
		<div class="container-fluid">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box_headline">
					<h2>KALAKSHI EXCLUSIVE PRODUCTS</h2>
				</div>
			</div>	

			<div class="col-md-12 col-sm-12 col-xs-12 owl-slider">
                <div id="carousel2" class="owl-carousel">
					<div class="item">
						<div class="klaProBox">
							<img src="{{ asset('frontassets/images/sari_1.jpg') }}">
							<div class="klaProBox_tham">
								<p>Fashion Wear</p>
								<ul>
									<li>12 Hat Saree</li>
									<li>Mead in Indian</li>
								</ul>
								<span>
									<a href="">Shop Now</a>
								</span>	
							</div>
						</div>
					</div>
                    <div class="item">
						<div class="klaProBox">
							<img src="{{ asset('frontassets/images/d056c611df3e27ca83ce9d17aef025c5.jpg') }}">
							<div class="klaProBox_tham">
								<p>Fashion Wear</p>
								<ul>
									<li>12 Hat Saree</li>
									<li>Mead in Indian</li>
								</ul>
								<span>
									<a href="">Shop Now</a>
								</span>	
							</div>
						</div>
					</div>
                    <div class="item">
						<div class="klaProBox">
							<img src="{{ asset('frontassets/images/kota104_Green-600x576.jpg') }}">
							<div class="klaProBox_tham">
								<p>Fashion Wear</p>
								<ul>
									<li>12 Hat Saree</li>
									<li>Mead in Indian</li>
								</ul>
								<span>
									<a href="">Shop Now</a>
								</span>	
							</div>
						</div>
					</div>
                    <div class="item">
						<div class="klaProBox">
							<img src="{{ asset('frontassets/images/6b0a407f303142171e85ecded611da62.jpg') }}">
							<div class="klaProBox_tham">
								<p>Fashion Wear</p>
								<ul>
									<li>12 Hat Saree</li>
									<li>Mead in Indian</li>
								</ul>
								<span>
									<a href="">Shop Now</a>
								</span>	
							</div>
						</div>
					</div>
                    <div class="item">
						<div class="klaProBox">
							<img src="{{ asset('frontassets/images/download.jpeg') }}">
							<div class="klaProBox_tham">
								<p>Fashion Wear</p>
								<ul>
									<li>12 Hat Saree</li>
									<li>Mead in Indian</li>
								</ul>
								<span>
									<a href="">Shop Now</a>
								</span>	
							</div>
						</div>
					</div>
                    <div class="item">
						<div class="klaProBox">
							<img src="{{ asset('frontassets/images/india-traditional-saree-14393822.jpg') }}">
							<div class="klaProBox_tham">
								<p>Fashion Wear</p>
								<ul>
									<li>12 Hat Saree</li>
									<li>Mead in Indian</li>
								</ul>
								<span>
									<a href="">Shop Now</a>
								</span>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Deals End -->

	<!-- Anti Bacterial -->
	<section id="antiBacterial">
		<div class="container-fluid">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="anti_main">
					<div class="anti_main_inn">
						<div class="col-md-7 col-md-offset-1 col-sm-7 col-md-offset-1 col-xs-12 anti_text">
							<h3>Anti Bacterial</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							<a href="">Read More..</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 anti_img">
							<img src="{{ asset('frontassets/images/anti.png') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Anti Bacterial End -->
    @endsection    

    @section('scriptjs')
    <script type="text/javascript">

    jQuery("#carousel").owlCarousel({
        autoplay: true,
        rewind: true, /* use rewind if you don't want loop */
        margin: 20,
        /*
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
            items: 1
            },

            600: {
            items: 3
            },

            1024: {
            items: 4
            },

            1366: {
            items: 4
            }
        }
    });
    jQuery("#carousel2").owlCarousel({
        autoplay: true,
        rewind: true, /* use rewind if you don't want loop */
        margin: 20,
        /*
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
            items: 1
            },

            600: {
            items: 3
            },

            1024: {
            items: 4
            },

            1366: {
            items: 4
            }
        }
    });
    </script>
    @endsection
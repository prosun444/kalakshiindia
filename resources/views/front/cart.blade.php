@extends('front.layouts.app')

@section('stylecss')
	<style>

		.quantity .input-text.qty {
	    	width: 54px;
		}
	</style>

@endsection

@section('content')

@php
use Illuminate\Support\Facades\Crypt;

@endphp

	<div class="clearfix"></div>
	<!-- Menu Bar End -->
	<section class="container-fluid my-5">
		<div class="card">
		    <div class="row" style="display: flex;">
		        <div class="col-md-8 cart">
		            <div class="title">
		                <div class="row">
		                    <div class="col-sm-6">
		                        <h4><b style="font-size: 22px;font-weight: 600;text-transform: uppercase;">Shopping Cart</b></h4>
		                    </div>
		                    <div class="col-sm-6 align-self-center text-right text-muted" style="font-size: 17px;color: gray;margin-top: 10px;">{{ (isset($orders) && !empty($orders) && count($orders)) ? count($orders['product_order_item']):'0'}} items</div>
		                </div>
		            </div>

		            @if(isset($orders) && !empty($orders) && count($orders))
		            @php $total_product_price = 0; @endphp
                  	@foreach($orders['product_order_item'] as $key=>$val)
                  	@if(isset($val['product_details']['offer']))
                  		@php 
                  		$product_price = ($val['product_details']['product_price'] - (($val['product_details']['product_price'] * $val['product_details']['offer']['percentage']) / 100)) * $val['quantity'];
                  		@endphp 
                  	@else
                   		@php $product_price = ($val['product_details']['product_price'] * $val['quantity']); @endphp  
                  	@endif
                  	@php $product_image = json_decode($val['product_details']['product_images']); 
                  	$total_product_price = ($total_product_price + $product_price);
                  	@endphp



		            <div class="row @if($key == 0 || $key%2 == 0) border-top @if(count($orders['product_order_item']) != $key+1) border-bottom @endif @endif" id="{{ isset($val['product_details']['id'])?($val['product_details']['id']):''}}">
		                <div class="row main align-items-center">
		                    <div class="col-sm-3">
		                    	<img class="img-fluid" src="" style="width: 105px;height: 105px;border: 1px solid;">
		                    </div>
		                    <div class="col-sm-3">
		                        <div class="row text-muted" style="font-size: 16px;font-weight: 600;"></div>
		                        <div class="row" style="font-size: 15px;"></div>
		                    </div>
		                    <div class="col-sm-3"> 
			                    <div class="quantity buttons_added" style="font-size: 18px;">
									<input type="button" value="-" class="minus" style="height: 25px;width: 25px;border-radius: 50%;margin-top: 8px;">
									<input data-product-id="" type="number" step="1" min="1" max="" name="quantity" value="" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
									<input type="button" value="+" class="plus" style="height: 25px;width: 25px;border-radius: 50%;    margin-top: 8px;">
								</div>
 							</div>
		                    <div class="col-sm-2" style="font-size: 20px;font-weight: 500;">
		                    	
		                    </div>
		                    <div class="col-sm-1" style="margin-top: 13px;">
		                    	<a href="javascript:void(0);" data-product-id="{{ isset($val['product_details']['id'])?Crypt::encryptString($val['product_details']['id']):''}}"  class="delete_cart_product" onclick="return confirm('Are you sure to delete this product from cart?')"><i class="fa fa-2x fa-times-circle-o" aria-hidden="true"></i></a></div>
		                </div>
		            </div>

		            @endforeach
	                @endif

		            <div class="back-to-shop">
		            	<a href="{{route('products')}}">&leftarrow;<span class="text-muted">Back to shop</span></a>
		            </div>
		        </div>

		        <div class="col-md-4 summary">
		            <div class="row">
		                <h5 style="text-align: center;"><b style="font-size: 20px;text-transform: uppercase;">Summary</b></h5>
		            </div>
		            <hr>
		            <div class="row">
		                <div class="col-sm-6" style="padding-left:0; font-size: 16px;font-weight: 500;"></div>
		                <div class="col-sm-6 text-right" style=" font-size: 16px;font-weight: 500;"></div>
		            </div>
		            <form>
		                <p>SHIPPING</p> 
		                <select id="shipping_charge_change">
		                	
		                    
		                </select>
		                <span style="color: green;"></span>
		                <br>
		                <br>

		                <p>GIVE CODE</p> <input id="code" placeholder="Enter your code">
		            </form>
		            <div class="row" style="border-top: 1px solid rgba(0,0,0,.1); padding: 2vh 0;">
		                <div class="col-sm-6" style="font-size: 18px;font-weight: 600;">TOTAL PRICE</div>
		                <div class="col-sm-6 text-right" style="font-size: 18px;font-weight: 600;"></div>
		            </div> 
		            <button class="btn">CHECKOUT</button>
		        </div>
		    </div>
		</div>
	</section>
@endsection

@section('scriptjs')
    <script>
		function openCity(evt, cityName) {
		  var i, tabcontent, tablinks;
		  tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }
		  tablinks = document.getElementsByClassName("tablinks");
		  for (i = 0; i < tablinks.length; i++) {
		    tablinks[i].className = tablinks[i].className.replace(" active", "");
		  }
		  document.getElementById(cityName).style.display = "block";
		  evt.currentTarget.className += " active";
		}

		function wcqib_refresh_quantity_increments() {
		    jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
		        var c = jQuery(b);
		        c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
		    })
		}
		String.prototype.getDecimals || (String.prototype.getDecimals = function() {
		    var a = this,
		        b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
		    return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
		}), jQuery(document).ready(function() {
		    wcqib_refresh_quantity_increments()
		}), jQuery(document).on("updated_wc_div", function() {
		    wcqib_refresh_quantity_increments()
		}), jQuery(document).on("click", ".plus, .minus", function() {
		    var a = jQuery(this).closest(".quantity").find(".qty"),
		        b = parseFloat(a.val()),
		        c = parseFloat(a.attr("max")),
		        d = parseFloat(a.attr("min")),
		        e = a.attr("step");
		    b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
		});

		$(document).ready(function(){
			var base_url = window.location.origin;
			carts();
			$(document).on('click','.minus,.plus,.qty' , function(){
				var product_id = $(this).parent().find('.qty').attr('data-product-id');
				var btn_type = $(this).attr('class');
				if(btn_type == 'plus' || btn_type == 'minus'){
					var quantity = $(this).parent().find('.qty').val();
				}else{
					var quantity = $(this).val();
				}
				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                url: base_url + "/quantity_increament",
	                type: "post",
	                datatype: "json",
	                data: { 'product_id':product_id,'quantity':quantity},
	                success: function (response) {
	                    var response = JSON.parse(response);
	                    if(response.success == 1){
	                        carts();
	                    }
	                }
	            });
			})

			$(document).on('click','.delete_cart_product' , function(){
				var product_id = $(this).attr('data-product-id');
				
				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                url: base_url + "/delete_product_from_cart",
	                type: "post",
	                datatype: "json",
	                data: { 'product_id':product_id},
	                success: function (response) {
	                	var response = JSON.parse(response);
	                    if(response.success == 1){
	                        $("#"+response.product_id).remove();
	                        carts();
	                    }
	                }
	            });
			})

			$(document).on('change','#shipping_charge_change' , function(){
				var shipping_charge_type = $("#shipping_charge_change option:selected").val();
				var charge = $("#shipping_charge_change option:selected").attr('charge');
				
				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                url: base_url + "/add_shipping_charge",
	                type: "post",
	                datatype: "json",
	                data: { 'shipping_charge':charge,'shipping_type':shipping_charge_type},
	                success: function (response) {
	                    var response = JSON.parse(response);
	                    if(response.success == 1){
	                        carts();
	                    }
	                }
	            });
			})
		});

		function carts(){
			var base_url = window.location.origin;
			var total_product_price = 0;
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: base_url + "/cart_order_list",
                type: "post",
                datatype: "json",
                data: { 'cartlist':1},
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log(response);
                    $(".cart").find('.title').find('.text-muted').text(response.product_order_item.length + ' items');
                    
                    $.each(response.product_order_item, function (key, val) {
                    	if(key == 0 || key%2 == 0){
                    		$("#"+val.product_details.id).addClass('border-top');
                    		if(response.product_order_item.length != key+1){
                    			$("#"+val.product_details.id).addClass('border-bottom');
                    		}
                    	}
                    	
                    	if(val.product_details.product_images !=""){
                    		var product_images = JSON.parse(val.product_details.product_images);
				        	$(".cart").find('.row:nth-child('+(key+2)+')').find('img').attr('src',base_url+'/picture/product/'+product_images[0]);
				        }else{
				        	$(".cart").find('.row:nth-child('+(key+2)+')').find('img').attr('src',base_url+'/picture/product/no_image.png');
				        }

				        $(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(2)').find('div:nth-child(1)').html(val.product_details.product_name);

				        if (typeof(val.product_details.sub_category) != "undefined" && val.product_details.sub_category !== null) {
				        	$(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(2)').find('div:nth-child(2)').html(val.product_details.sub_category.product_category_name);
				        }else{
				        	$(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(2)').find('div:nth-child(2)').html('');
				        }

				        $(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(3)').find(".quantity").find('.qty').attr('data-product-id',val.product_details.id);
				        $(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(3)').find(".quantity").find('.qty').val(val.quantity);


				        if(val.product_details.offer != null){
                  			var product_price = (val.product_details.product_price - ((val.product_details.product_price * val.product_details.offer.percentage) / 100)) * val.quantity;
                  		
	                  	}else{
	                   		var product_price = (val.product_details.product_price * val.quantity);  
	                  	}
	                  	total_product_price = parseFloat(total_product_price + product_price);

	                  	$(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(4)').html('&euro;'+parseFloat(product_price)+'<span class="close">&#10005;</span>');

	                  	//var product_id = val.product_details.id;
	                  	//$(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(5)').find('a').attr('data-product-id',"<?php //echo Crypt::encryptString('".product_id."');?>");


				        //console.log($(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(5)').find('a').attr('data-product-id',val.product_details.id));
				    });
                    
                    $(".summary").find('div:nth-child(3)').find('div:nth-child(1)').html('ITEMS '+response.product_order_item.length);
                    
                    $(".summary").find('div:nth-child(3)').find('div:nth-child(2)').html('&euro; '+total_product_price);

                    var shipping_option = '';
                    var max_delivery_days = 0;
                    var min_delivery_days = 0;
                    var delivery_charge = 0;
                    $.each(response.shipping_details, function (key1, value) {
                    	shipping_option += '<option class="text-muted" value="'+value.delivery_type+'" charge="'+value.delivery_charge+'" ';

                    	if(value.delivery_charge == response.shipping_charge){
                    		shipping_option += 'selected="selected"';
                    		max_delivery_days = value.max_delivery_days;
                    		min_delivery_days = value.min_delivery_days;
                    		delivery_charge = value.delivery_charge;
                    	}
                    	shipping_option += '>'+value.delivery_type+'- &euro;'+value.delivery_charge+'</option>';
					});
                    $("#shipping_charge_change").html(shipping_option);
                    
                    $("#shipping_charge_change").parent().find('span').html('Delivery by '+min_delivery_days+'-'+max_delivery_days+' days');
                    
                    $(".summary").find('div:nth-child(5)').find('div:nth-child(2)').html('&euro; '+(total_product_price+delivery_charge));
                }
            });
		}
		
	</script>

@endsection
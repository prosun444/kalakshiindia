        
@extends('front.layouts.app') 

@section('content')


        <div class="container register-form" style="margin-top: 10%;">
            <div class="form">
                <div class="note">
                <p style="font-size: 20px;">PARTNER WITH US</p>
                </div>
                <form style="padding: 0;" method="post" action="{{ route('registration.store') }}" enctype="multipart/form-data">
                @csrf
                    
                    @if(session('success'))
                    <div class="success-msg" id="success">
                        <i class="fa fa-check"></i>
                        {{session('success')}}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="error-msg" id="error" >
                        <i class="fa fa-times-circle"></i>
                        {{session('error')}}
                    </div>
                    @endif
                <div class="form-content">
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" value="{{old('name')}}" name="name" />
                                <p class="text-muted">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone Number *" value="{{old('number')}}" name="number" />
                                <p class="text-muted">{{ $errors->first('number') }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email *" value="{{old('email')}}" name="email"/>
                                <p class="text-muted">{{ $errors->first('email') }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Your Password *" value="{{old('password')}}" name="password"/>
                                <p class="text-muted">{{ $errors->first('password') }}</p>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Confirm Password *" value="{{old('confpassword')}}" name="confpassword" />
                                <p class="text-muted">{{ $errors->first('confpassword') }}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control" name="organizationtype" id="organizationtype">
                                    <option value="">You are</option>
                                    <option value="Entrepreneur">Entrepreneur</option>
                                    <option value="Proprietor">Proprietor</option>
                                    <option value="Manufacturer">Manufacturer</option>
                                    <option value="Wholesaler">Wholesaler</option>
                                    <option value="Trader">Trader</option>
                                    <option value="Individual">Individual</option>
                                </select>
                            </div>
                            <div class="form-group">
                            <div class="form-group">
                                <select class="form-control" name="participation" id="participation" value="{{old('participation')}}">
                                    <option {{ old('participation') == '' ? "selected" : "" }} value="">Participation</option>
                                    <option {{ old('participation') == '2' ? "selected" : "" }} value="2">As an affiliate only</option>
                                    <option {{ old('participation') == '3' ? "selected" : "" }} value="3">As a supplier only</option>
                                    <option {{ old('participation') == '4' ? "selected" : "" }} value="4">As both supplier & affiliate</option>
                                    <option {{ old('participation') == '5' ? "selected" : "" }} value="5">As a buyer</option>
                                    
                                </select>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea placeholder="Address" class="form-control" rows="4" id="comment" name="address">{{old('address')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <section id='bankinfo' style="display: none;">
                        <div class="note2inner">
                            <p style="font-size: 14px;">BANK DETAILS</p>
                        </div>  
                        <div class="row">    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Bank Name" value="{{old('bankname')}}" name="bankname"/>
                                    <p class="text-muted">{{ $errors->first('bankname') }}</p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="A/C Number" value="{{old('acnumber')}}" name="acnumber"/>
                                    <p class="text-muted">{{ $errors->first('acnumber') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="IFSC Code" value="{{old('ifsccode')}}" name="ifsccode"/>
                                    <p class="text-muted">{{ $errors->first('ifsccode') }}</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id='extrainfo' style="display: none;">
                        <div class="note2inner">
                            <p style="font-size: 14px;">PERSONAL DETAILS</p>
                        </div>
                        <div class="row">    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Aadhar No" value="{{old('aadhar')}}" name="aadhar"/>
                                    <p class="text-muted">{{ $errors->first('aadhar') }}</p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="PAN" value="{{old('panno')}}" name="panno"/>
                                    <p class="text-muted">{{ $errors->first('panno') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Individual Code" value="{{old('individualcode')}}" name="individualcode"/>
                                </div>
                            </div>
                        </div>
                    </section>   

                    <button style="border-radius: 0.5rem;margin-left: 17px;" type="submit" class="btnSubmit">Submit</button>
                </div>
                </form>
            </div>
        </div>
@endsection 
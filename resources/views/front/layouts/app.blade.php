<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>|| KALAKSHI ||</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @yield('stylecss')
</head>
<body>

<!--Body Start -->
<section id="mainbody">
		<!-- Topbar -->
	<section id="topbar">
		<div class="container-fluid">
			@if(Auth::guard('front')->check())
				<form id="logoutform" name="logoutform" class="logout-form" method="POST" action="{{ route('logout') }}" style="padding: 0;">
           		@csrf
			  		<a id="logoutbutton" href="javascript:;" onclick="logout()">Log Out</button>
			  	</form> 
			@else
				<a href="{{ route('login') }}">Login</a>
			@endif
			<a href="{{ route('user.registration') }}">Partner with us</a>
		</div>
	</section>
	<!-- Topbar End -->

	<!-- Menu Bar -->
	<section id="menubar_section">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="leftbar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
					<a class="navbar-brand" href="#">KALAKSHI</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="{{ route('home') }}">Home</a></li>

						@if(isset($pcategory) && !empty($pcategory))
						@foreach($pcategory as $key => $val)

							<li class="dropdown dropdown-large"><a href="{{ route('products').'/?cat='.$val['product_category_id'] }}" class="dropdown-toggle" data-toggle="dropdown">{{$val['product_category_name']}}</a>
								@if(isset($val['sub_category']) && !empty($val['sub_category']))
								
								<ul class="dropdown-menu dropdown-menu-large row" style="margin-top: -10px;">
									@foreach($val['sub_category'] as $key2 => $val2)
									<li><a class="droptext" href="{{ route('products').'/?cat='.$val2['product_category_id'] }}">{{$val2['product_category_name']}}</a></li>	
									@endforeach
								</ul>
								@endif
							</li>
						@endforeach
						@endif
						<li><a href="{{ route('blogs') }}">Blogs</a></li>
						<li><a href="">Leather</a></li>
						<li><a href="">Contacts</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</section>
	<!-- Menu Bar End -->
    @yield('content')
</section>

<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>About Kalakshi</h4>
				<ul>
					<li><a href="">About Us</a></li>
					<li><a href="">Contact Us</a></li>
					<li><a href="">Blog</a></li>
					<li><a href="">Testimonial</a></li>
					<li><a href="">Press</a></li>
					<li><a href="">Lookbook</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>My Account</h4>
				<ul>
					<li><a href="">Login</a></li>
					<li><a href="">Shopping Bag</a></li>
					<li><a href="">Wish List</a></li>
					<li><a href="">Order History</a></li>
					<li><a href="">Order Tracking</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>WE ACCEPT</h4>
				<span><img src="{{ asset('frontassets/images/payment.png') }}"></span>
				<h4>NEWSLETTER IN YOUR INBOX</h4>
				<div class="newsBox">
					<label>
						<input type="text" name="">
					</label>
					
					<buton>Subscribe</buton>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h4>GET IN TOUCH</h4>
				<p><a href="tel:+91 993300993300">+91 993300993300</a></p>
				<p><a href="tel:+91 993300993300">+91 993300993300</a></p>
				<h4>EMAIL US ON</h4>
				<p><a href="mailto:example@gmail.com">example@gmail.com</a></p>
				<h4>FOLLOW US</h4>
				<p>
					<a href=""><img src="{{ asset('frontassets/images/f_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/t_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/ins_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/w_icon.png') }}"></a>
					<a href=""><img src="{{ asset('frontassets/images/y_icon.png') }}"></a>
				</p>
			</div>
		</div>
	</div>
</footer>
<!-- Footer End -->

<!-- Copy Right -->
<section id="copyright">
	<div class="container-fluid">
		<p>Copyright &copy; 2010-2020 Kalakshi Collections. All rights reserved</p>
	</div>
</section>
<!-- Copy Right End -->

<script type="text/javascript" src="{{ asset('frontassets/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontassets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type="text/javascript" src="{{ asset('frontassets/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js" type="text/javascript" charset="utf-8" async defer></script>
@yield('scriptjs')
@yield('profilescriptjs')
</body>
</html>
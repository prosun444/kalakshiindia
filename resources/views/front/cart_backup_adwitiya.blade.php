@extends('front.layouts.app')

@section('stylecss')
	<style>

		.quantity .input-text.qty {
	    	width: 54px;
		}
	</style>

@endsection

@section('content')
    	<!-- Topbar -->
	<section id="topbar">
		<div class="container-fluid">
			<a href="">Partner with us</a>
		</div>
		<div class="clearfix"></div>
	</section>
	<!-- Topbar End -->

	<!-- Menu Bar -->
	<section id="menubar_section">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="leftbar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
					<a class="navbar-brand" href="#">KALAKSHI</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="#">Home</a></li>
						<li class="dropdown dropdown-large"><a href="" class="dropdown-toggle" data-toggle="dropdown">Aadi Kalakshi</a>
							<!---------------------------------------------------------------------------------------------------------->
							<ul class="dropdown-menu dropdown-menu-large row">
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 1</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li class="dropdown-header">Saree No 2</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
									</ul>
								</li>
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 3</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li><img class="img-responsive" src="{{ asset('frontassets/images/sari_1.jpg') }}" /></li>
									</ul>
								</li>
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 4</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li class="dropdown-header">Saree No 5</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
									</ul>	
								</li>
								<li class="col-sm-3">
									<ul>
										<li class="dropdown-header">Saree No 6</li>
										<li><a href="#">Saree 1</a></li>
										<li><a href="#">Saree 2</a></li>
										<li><a href="#">Saree 3</a></li>
										<li><a href="#">Saree 4</a></li>
										<li class="divider"></li>
										<li><img class="img-responsive" src="{{ asset('frontassets/images/sari_1.jpg') }}" /></li>
									</ul>
								</li>
							</ul>
							<!---------------------------------------------------------------------------------------------------------->
						</li>
						<li><a href="">Swayam Kalakshii</a></li>
						<li><a href="">Umang Kalakshii</a></li>
						<li><a href="">Kalakshetra</a></li>
						<li><a href="">Blogs</a></li>
						<li><a href="">Leather</a></li>
						<li><a href="">Contacts</a></li>
					</ul>
				</div>
				
			</div>
			
		</nav>
	</section>
	<div class="clearfix"></div>
	<!-- Menu Bar End -->
	<section class="container-fluid my-5">
		<div class="card">
		    <div class="row" style="display: flex;">
		        <div class="col-md-8 cart">
		            <div class="title">
		                <div class="row">
		                    <div class="col-sm-6">
		                        <h4><b style="font-size: 22px;font-weight: 600;text-transform: uppercase;">Shopping Cart</b></h4>
		                    </div>
		                    <div class="col-sm-6 align-self-center text-right text-muted" style="font-size: 17px;color: gray;margin-top: 10px;">{{ (isset($orders) && !empty($orders) && count($orders)) ? count($orders['product_order_item']):'0'}} items</div>
		                </div>
		            </div>

		            @if(isset($orders) && !empty($orders) && count($orders))
		            @php $total_product_price = 0; @endphp
                  	@foreach($orders['product_order_item'] as $key=>$val)
                  	@if(isset($val['product_details']['offer']))
                  		@php 
                  		$product_price = ($val['product_details']['product_price'] - (($val['product_details']['product_price'] * $val['product_details']['offer']['percentage']) / 100)) * $val['quantity'];
                  		@endphp 
                  	@else
                   		@php $product_price = ($val['product_details']['product_price'] * $val['quantity']); @endphp  
                  	@endif
                  	@php $product_image = json_decode($val['product_details']['product_images']); 
                  	$total_product_price = ($total_product_price + $product_price);
                  	@endphp



		            <div class="row @if($key == 0 || $key%2 == 0) border-top @if(count($orders['product_order_item']) != $key+1) border-bottom @endif @endif">
		                <div class="row main align-items-center">
		                    <div class="col-sm-3">
		                    	<img class="img-fluid" src="{{ isset($product_image[0])? asset('picture/product/'.$product_image[0]):'' }}" style="width: 99px;height: 106px;">
		                    </div>
		                    <div class="col-sm-3">
		                        <div class="row text-muted" style="font-size: 16px;font-weight: 600;">{{ isset($val['product_details']['product_name'])?$val['product_details']['product_name']:''}}</div>
		                        <div class="row" style="font-size: 15px;">{{$val['product_details']['sub_category']['product_category_name']?$val['product_details']['sub_category']['product_category_name']:''}}</div>
		                    </div>
		                    <div class="col-sm-3"> 
			                    <div class="quantity buttons_added" style="font-size: 18px;">
									<input type="button" value="-" class="minus" style="width: 26px;">
									<input data-product-id="{{ isset($val['product_details']['id'])?$val['product_details']['id']:''}}" type="number" step="1" min="1" max="" name="quantity" value="{{ isset($val['quantity'])?$val['quantity']:'0'}}" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
									<input type="button" value="+" class="plus" style="width: 26px;">
								</div>
 							</div>
		                    <div class="col-sm-2" style="font-size: 20px;font-weight: 500;">
		                    	&euro; {{ isset($product_price)?number_format((float)$product_price, 2, '.', ''):'0'}} <span class="close">&#10005;</span>
		                    </div>
		                    <div class="col-sm-1" style="margin-top: 13px;">
		                    	<a href="javascript:void(0);" data-product-id="{{ isset($val['product_details']['id'])?$val['product_details']['id']:''}}"  class="delete_cart_product" onclick="return confirm('Are you sure to delete this product from cart?')"><i class="fa fa-2x fa-times-circle-o" aria-hidden="true"></i></a></div>
		                </div>
		            </div>

		            @endforeach
	                @endif


		            <!-- <div class="row">
		                <div class="row main align-items-center">
		                    <div class="col-sm-3"><img class="img-fluid" src="https://i.imgur.com/1GrakTl.jpg" style="width: 99px;height: 106px;"></div>
		                    <div class="col-sm-3">
		                        <div class="row text-muted" style="font-size: 16px;font-weight: 600;">Shirt</div>
		                        <div class="row" style="font-size: 15px;">Cotton T-shirt</div>
		                    </div>
		                    <div class="col-sm-3"> 
			                    <div class="quantity buttons_added" style="font-size: 18px;">
									<input type="button" value="-" class="minus" style="width: 26px;"><input type="number" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="" inputmode=""><input type="button" value="+" class="plus" style="width: 26px;">
								</div>
 							</div>
		                    <div class="col-sm-2" style="font-size: 20px;font-weight: 500;">&euro; 44.00 <span class="close">&#10005;</span></div>
		                    <div class="col-sm-1" style="margin-top: 13px;"><a href=""><i class="fa fa-2x fa-times-circle-o" aria-hidden="true"></i></a></div>
		                </div>
		            </div>
		            <div class="row border-top border-bottom">
		                <div class="row main align-items-center">
		                    <div class="col-sm-3"><img class="img-fluid" src="https://i.imgur.com/1GrakTl.jpg" style="width: 99px;height: 106px;"></div>
		                    <div class="col-sm-3">
		                        <div class="row text-muted" style="font-size: 16px;font-weight: 600;">Shirt</div>
		                        <div class="row" style="font-size: 15px;">Cotton T-shirt</div>
		                    </div>
		                    <div class="col-sm-3"> 
			                    <div class="quantity buttons_added" style="font-size: 18px;">
									<input type="button" value="-" class="minus" style="width: 26px;"><input type="number" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="" inputmode=""><input type="button" value="+" class="plus" style="width: 26px;">
								</div>
 							</div>
		                    <div class="col-sm-2" style="font-size: 20px;font-weight: 500;">&euro; 44.00 <span class="close">&#10005;</span></div>
		                    <div class="col-sm-1" style="margin-top: 13px;"><a href=""><i class="fa fa-2x fa-times-circle-o" aria-hidden="true"></i></a></div>
		                </div>
		            </div>

		            <div class="row">
		                <div class="row main align-items-center">
		                    <div class="col-sm-3"><img class="img-fluid" src="https://i.imgur.com/1GrakTl.jpg" style="width: 99px;height: 106px;"></div>
		                    <div class="col-sm-3">
		                        <div class="row text-muted" style="font-size: 16px;font-weight: 600;">Shirt</div>
		                        <div class="row" style="font-size: 15px;">Cotton T-shirt</div>
		                    </div>
		                    <div class="col-sm-3"> 
			                    <div class="quantity buttons_added" style="font-size: 18px;">
									<input type="button" value="-" class="minus" style="width: 26px;"><input type="number" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="" inputmode=""><input type="button" value="+" class="plus" style="width: 26px;">
								</div>
 							</div>
		                    <div class="col-sm-2" style="font-size: 20px;font-weight: 500;">&euro; 44.00 <span class="close">&#10005;</span></div>
		                    <div class="col-sm-1" style="margin-top: 13px;"><a href=""><i class="fa fa-2x fa-times-circle-o" aria-hidden="true"></i></a></div>
		                </div>
		            </div> -->

		            <div class="back-to-shop">
		            	<a href="{{route('products')}}">&leftarrow;</a><span class="text-muted">Back to shop</span>
		            </div>
		        </div>

		        <div class="col-md-4 summary">
		            <div>
		                <h5 style="text-align: center;"><b style="font-size: 20px;text-transform: uppercase;">Summary</b></h5>
		            </div>
		            <hr>
		            <div class="row">
		                <div class="col-sm-6" style="padding-left:0; font-size: 16px;font-weight: 500;">ITEMS {{ (isset($orders) && !empty($orders) && count($orders)) ? count($orders['product_order_item']):'0'}}</div>
		                <div class="col-sm-6 text-right" style=" font-size: 16px;font-weight: 500;">&euro; {{isset($total_product_price) ? number_format((float)$total_product_price, 2, '.', '') : '0'}}</div>
		            </div>
		            <form>
		                <p>SHIPPING</p> 
		                <select id="shipping_charge_change">
		                	@if(isset($orders['shipping_details']) && !empty($orders['shipping_details']) && count($orders['shipping_details'])>0)
		                	@foreach($orders['shipping_details'] as $shipping_key=>$shipping_val)
		                	<option class="text-muted" value="{{$shipping_val['delivery_type']}}" charge="{{$shipping_val['delivery_charge']}}" @if($orders["shipping_type"]==$shipping_val['delivery_type']) selected="selected" @endif >{{$shipping_val['delivery_type']}}- &euro;{{$shipping_val['delivery_charge']}}</option>
		                	@endforeach
		                	@endif
		                    
		                </select>
		                <p>GIVE CODE</p> <input id="code" placeholder="Enter your code">
		            </form>
		            <div class="row" style="border-top: 1px solid rgba(0,0,0,.1); padding: 2vh 0;">
		                <div class="col-sm-6" style="font-size: 18px;font-weight: 600;">TOTAL PRICE</div>
		                <div class="col-sm-6 text-right" style="font-size: 18px;font-weight: 600;">&euro; {{isset($total_product_price) ? number_format((float)$total_product_price, 2, '.', '') : '0'}}</div>
		            </div> <button class="btn">CHECKOUT</button>
		        </div>
		    </div>
		</div>
	</section>
@endsection

@section('scriptjs')
    <script>
		function openCity(evt, cityName) {
		  var i, tabcontent, tablinks;
		  tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }
		  tablinks = document.getElementsByClassName("tablinks");
		  for (i = 0; i < tablinks.length; i++) {
		    tablinks[i].className = tablinks[i].className.replace(" active", "");
		  }
		  document.getElementById(cityName).style.display = "block";
		  evt.currentTarget.className += " active";
		}

		function wcqib_refresh_quantity_increments() {
		    jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
		        var c = jQuery(b);
		        c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
		    })
		}
		String.prototype.getDecimals || (String.prototype.getDecimals = function() {
		    var a = this,
		        b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
		    return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
		}), jQuery(document).ready(function() {
		    wcqib_refresh_quantity_increments()
		}), jQuery(document).on("updated_wc_div", function() {
		    wcqib_refresh_quantity_increments()
		}), jQuery(document).on("click", ".plus, .minus", function() {
		    var a = jQuery(this).closest(".quantity").find(".qty"),
		        b = parseFloat(a.val()),
		        c = parseFloat(a.attr("max")),
		        d = parseFloat(a.attr("min")),
		        e = a.attr("step");
		    b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
		});

		$(document).ready(function(){
			var base_url = window.location.origin;

			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: base_url + "/cart_order_list",
                type: "post",
                datatype: "json",
                data: { 'cartlist':1},
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log(response.product_order_item);
                    $(".cart").find('.title').find('.text-muted').text(response.product_order_item.length + ' items');
                    
                    $.each(response.product_order_item, function (key, val) {
                    	var product_images = JSON.parse(val.product_details.product_images);
				        $(".cart").find('.row:nth-child('+(key+2)+')').find('img').attr('src',base_url+'/picture/product/'+product_images[0]);

				        $(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(2)').find('div:nth-child(1)').html('val.product_details.product_name')

				        console.log($(".cart").find('.row:nth-child('+(key+2)+')').find('.main').find('div:nth-child(2)').find('div:nth-child(1)').html());
				        //console.log());
				        
				        //$(".cart").find('.row:nth-child('+(key+2)+')').find('img').attr('src',base_url+'/picture/product/'+product_images[0]);

				        //$(".cart").find('.main:nth-child(2)').find('img').attr('src',base_url+'/picture/product/'+product_images[0]);

				        product_images = '';
				        //console.log($(".cart").find('.main:nth-child(1)').find('img').attr('src'));

				    });


                    
                }
            });



			$(document).on('click','.minus,.plus,.qty' , function(){
				var product_id = $(this).parent().find('.qty').attr('data-product-id');
				var btn_type = $(this).attr('class');
				if(btn_type == 'plus' || btn_type == 'minus'){
					var quantity = $(this).parent().find('.qty').val();
				}else{
					var quantity = $(this).val();
				}
				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                url: base_url + "/quantity_increament",
	                type: "post",
	                datatype: "json",
	                data: { 'product_id':product_id,'quantity':quantity},
	                success: function (response) {
	                    var response = JSON.parse(response);
	                    console.log(response.success);
	                    if(response.success == 1){
	                        location.reload();
	                    }
	                }
	            });
			})

			$(document).on('click','.delete_cart_product' , function(){
				var product_id = $(this).attr('data-product-id');
				
				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                url: base_url + "/delete_product_from_cart",
	                type: "post",
	                datatype: "json",
	                data: { 'product_id':product_id},
	                success: function (response) {
	                    var response = JSON.parse(response);
	                    console.log(response.success);
	                    if(response.success == 1){
	                        location.reload();
	                    }
	                }
	            });
			})

			$(document).on('change','#shipping_charge_change' , function(){
				var shipping_charge_type = $("#shipping_charge_change option:selected").val();
				var charge = $("#shipping_charge_change option:selected").attr('charge');
				
				$.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	            $.ajax({
	                url: base_url + "/add_shipping_charge",
	                type: "post",
	                datatype: "json",
	                data: { 'shipping_charge':charge,'shipping_type':shipping_charge_type},
	                success: function (response) {
	                    var response = JSON.parse(response);
	                    console.log(response.success);
	                    if(response.success == 1){
	                        location.reload();
	                    }
	                }
	            });
			})
		})
	</script>

@endsection
@extends('front.layouts.app')

 @section('stylecss') 
         <style>   
            .klaProBox{
                height: 240px;
            }
            .klaProBox_tham {
                bottom: -50px;
            }
            .circle {
                border:2px solid white;    
                height:20px;
                border-radius:50%;
                -moz-border-radius:50%;
                -webkit-border-radius:50%;
                width:20px;
                margin-bottom: -5px;
            }
        </style>
 @endsection 

    @section('content')
    
     <!-- inner-page-content-start -->
      
      <section class="container-fluid my-5" style="width: 75%;">
        <div class="container-fluid text-center" style="margin-top: 70px;text-transform: uppercase;margin-top: 120px;margin-bottom: 50px;">
            <h1 style="font-size: 50px;font-weight: 500;color: white;">Products</h1>
        </div>
         <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
               <div class="left-filter">
                  <div class="filter-applied-part p-3">
                     <div class="d-flex align-items-center justify-content-between mb-4">
                        <h5 class="mb-0 filter-part-heading" style="margin: 0;padding: 0;padding-left: 105px;padding-top: 10px;">Filters</h5>
                     </div>
                     <ul class="list-unstyled d-flex align-items-center flex-wrap fil-applied-list">
                        <!-- <li>
                           <a href="#" title="">36 <i class="fa fa-times"></i></a>
                        </li>
                        <li>
                           <a href="#" title="">Green <i class="fa fa-times"></i></a>
                        </li>
                        <li>
                           <a href="#" title="">₹130 - ₹250 <i class="fa fa-times"></i></a>
                        </li> -->
                     </ul>
                  </div>
                  <hr class="m-0">
                  <div class="filter-locality-part  p-3">
                     <div class="d-flex align-items-center justify-content-between  mb-4">
                        <h5 class="mb-0 filter-part-heading">
                           Department
                        </h5>
                        <p class="filter-part-clear-text">Clear</p>
                     </div>
                     <form>

                        @php
                        $scategory = [];

                        if(isset($_GET["cat"]) && !empty($_GET["cat"]))
                        {
                            $scategory = explode(',' , $_GET["cat"]);
                        }else{
                            $scategory = [];
                        }

                        if(isset($productcategory) && !empty($productcategory)){
                        foreach($productcategory as $key =>$category){

                        @endphp

                        <div class="cus-checkbox">
                            <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                  <div style="display: inline; margin: 17px;">
                                      <input type="checkbox" name="category[]" class="pcategory" id="pid-{{$key}}" value="{{$category['product_category_id']}}" @if(isset($scategory) && in_array($category['product_category_id'] , $scategory)) {{"checked"}} @endif>  
                                      <label for="pid-{{$key}}" style="margin-bottom: 0;margin-left: 10px;">{{$category['product_category_name']}}</label>
                                  </div>   
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="false" aria-controls="collapse{{$key}}" style="display: inline-block;padding: 0px;float: right;margin-right: 11px;"></a>
                                </h4>
                              </div>
                              <div id="collapse{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    @php
                                    if(isset($category['subcategory']) && !empty($category['subcategory'])){
                                    foreach($category['subcategory'] as $key2 =>$subcategory){
                                    @endphp
                                    <input type="checkbox" name="category[]" class="scategory" id="spid{{$key}}-{{$key2}}" value="{{$subcategory['product_category_id']}}" @if(in_array($subcategory['product_category_id'] , $scategory)) {{"checked"}} @endif>
                                    <label for="spid{{$key}}-{{$key2}}">{{$subcategory['product_category_name']}}</label>
                                    @php
                                    }}
                                    @endphp
                                    
                                </div>
                              </div>
                            </div>
                        </div>

                        @php
                        }
                        }
                        @endphp
                       

                     </form>
                  </div>
                  <hr class="m-0">
                  <div class="filter-locality-part  p-3">
                     <div class="d-flex align-items-center justify-content-between  mb-4">
                        <h5 class="mb-0 filter-part-heading">
                           Color<!-- <span>(3)</span> -->
                        </h5>
                        <p class="filter-part-clear-text">Clear</p>
                     </div>
                     <div class="custom-radios">
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-1" name="color" value="green" @if(isset($_GET["color"]) && $_GET["color"] =="green") {{"checked"}} @endif >
                           <label for="color-1">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-2" name="color" value="blue" @if(isset($_GET["color"]) && $_GET["color"] =="blue") {{"checked"}} @endif >
                           <label for="color-2">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-3" name="color" value="yellow" @if(isset($_GET["color"]) && $_GET["color"] =="yellow") {{"checked"}} @endif>
                           <label for="color-3">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-4" name="color" value="red" @if(isset($_GET["color"]) && $_GET["color"] =="red") {{"checked"}} @endif>
                           <label for="color-4">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-5" name="color" value="black" @if(isset($_GET["color"]) && $_GET["color"] =="black") {{"checked"}} @endif>
                           <label for="color-5">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-6" name="color" value="white" @if(isset($_GET["color"]) && $_GET["color"] =="white") {{"checked"}} @endif>
                           <label for="color-6">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                        <div style="margin-top: 2px;">
                           <input type="radio" id="color-7" name="color" value="orange" @if(isset($_GET["color"]) && $_GET["color"] =="orange") {{"checked"}} @endif>
                           <label for="color-7">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                         <div style="margin-top: 2px;">
                           <input type="radio" id="color-8" name="color" value="violet" @if(isset($_GET["color"]) && $_GET["color"] =="violet") {{"checked"}} @endif>
                           <label for="color-8">
                           <span>
                           <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                           </span>
                           </label>
                        </div>
                     </div>
                  </div>
                  <hr class="m-0">
                  <div class="filter-locality-part  p-3">
                     <div class="d-flex align-items-center justify-content-between  mb-4">
                        <h5 class="mb-0 filter-part-heading">Price</h5>
                        <p class="filter-part-clear-text">Clear</p>
                     </div>
                     <div class="price-range-slider">
                        <p class="range-value">
                           <input type="text" id="amount" readonly>
                        </p>
                        <div id="slider-range" class="range-bar"></div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <hr class="m-0">
                  <div class="filter-locality-part  p-3">
                     <div class="d-flex align-items-center justify-content-between  mb-4">
                        <h5 class="mb-0 filter-part-heading">
                           Review
                        </h5>
                        <p class="filter-part-clear-text">Clear</p>
                     </div>
                     <form>
                        <div class="cus-checkbox">
                           <input type="checkbox" id="id-8">
                           <label for="id-8">4 Star & Up</label>
                        </div>
                        <div class="cus-checkbox">
                           <input type="checkbox" id="id-9">
                           <label for="id-9">3 Star & Up</label>
                        </div>
                        <div class="cus-checkbox">
                           <input type="checkbox" id="id-10">
                           <label for="id-10">2 Star & Up</label>
                        </div>
                        <div class="cus-checkbox">
                           <input type="checkbox" id="id-11">
                           <label for="id-11">1 Star & Up</label>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
               <div class="d-flex align-items-center justify-content-between">
                  <h3 class="m-0 product-list-hd">{{ $totalproducts}} products </h3>
                  <select id="normal-select-1" placeholder-text="Sort by">
                     <option value="1" class="select-dropdown__list-item">Best rating</option>
                     <option value="ASC" class="select-dropdown__list-item">Lowest price first</option>
                     <option value="DESC" class="select-dropdown__list-item">Highest price first</option>
                  </select>
               </div>
               <hr>
               <div class="row">

                @foreach($products as $product)

                @php

                    $product_images = [];
                    $product_images = json_decode($product->product_images);
                    $product_images =  $product_images[0];

                    $product_price = $product->product_price;

                    if(isset($product->offer->percentage) && !empty($product->offer->percentage)){
                        $product_price = $product_price - (($product_price * $product->offer->percentage)/100);
                    }
                @endphp

                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="klaProBox">
                        <img src="{{ asset('picture/product/'.$product_images ) }}">
                        @if(isset($product->offer->percentage) && !empty($product->offer->percentage))
                        <div class="offer_tham">
                            <span>{{$product->offer->percentage}}% Off</span>
                        </div>
                        @endif
                        <div class="klaProBox_tham">
                           <p>{{$product->product_name}}</p>
                           <ul>
                              <li>@if(isset($product->offer->percentage) && !empty($product->offer->percentage))<strike>₹{{$product->product_price}}</strike>@endif ₹{{$product_price}}</li>
                              <li><div class="circle" style="background-color:{{$product->product_colour}};"></div></li>
                           </ul>
                           <span>
                            <a href="{{route('productsdetails', ['slug' => $product->product_slug])}}" style="background: #4ad395;">Buy Now <img src="{{ asset('frontassets/images/money.png') }}" style="height: 25px;width: 25px;"></a>
                            <a href="" style="background: #4ad395;">Add to Cart <img src="{{ asset('frontassets/images/shopping-cart.png') }}" style="height: 25px;width: 25px;"></a>
                           </span> 
                        </div>
                     </div>
                  </div>
                  
                @endforeach

               </div>
               @if( $totalpage > 1 )
               <nav aria-label="Page navigation example">
                  <ul class="pagination">
                     <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a>
                     </li>
                     
                     @for($i=1 ; $i<=$totalpage ; $i++)
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                     @endfor
                     
                     <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a>
                     </li>
                  </ul>
               </nav>
               @endif
            </div>
         </div>
      </section>
      <!-- inner-page-content-end -->
     @endsection 

     @section('scriptjs')
     <script type="text/javascript">

         var min = <?= $min ?>;
         var max = <?= $max ?>; 
         var lowrange = <?= $lowrange ?>;
         var highrange = <?= $highrange ?>;
         var baseurl = 'http://127.0.0.1:8000/';

         //-----JS for Price Range slider-----
         
        $(function() {
            $( "#slider-range" ).slider({
                 range: true,
                 min: min,
                 max: max,
                 values: [ lowrange , highrange ],
                 slide: function( event, ui ) {
                 $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
                 }
            });
            $( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +
              " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );


            $("#slider-range").on("slidestop", function(event, ui) {
                
                productFilter();

            });
        });


        $(document).ready(function() {
            $("input[name='color']").change(function(){
                productFilter();
            });

            $('.pcategory').click(function(){
              $(this).parent().parent().parent().parent().find('.scategory').prop('checked', $(this).prop('checked'));
              productFilter();
            });

            $('.scategory').click(function(){
              $(this).parent().parent().parent().parent().find('.pcategory').prop('checked', $(this).prop('checked'));
                productFilter();
            });

            $('#normal-select-1').change(function(){
              alert();
              var orderBy = $(this + ' option:selected').val();
              alert(orderBy);
              //productFilter();
            });

        });

         

         function productFilter() {

            var url = '';
            var color = '';
            var startPos = 0;
            var endPos = 3000;
            var cat = '';
            var findcat = '';
            var findcolor = '';
            var findOrderBy = '';

            var ckbox = $('input[name="category[]"]');

            if (ckbox.is(':checked')) {
              $("input[name='category[]']:checked").each ( function() {
                  cat += $(this).val() + ",";
              });

              cat = cat.slice(0,-1);
            }

            if(cat != ''){
              findcat = 'cat='+cat;
            }

            color = $('input[name="color"]:checked').val();
            if(color!='' && color != undefined){
              findcolor = 'color='+color;
            }

            if($('#normal-select-1 option:selected').val() != ''){
              orderBy = $('#normal-select-1 option:selected').val();
            }
            if(orderBy != ''){
              findOrderBy = 'ob='+orderBy;
            }

            start = $('#slider-range').slider("values")[0]; 
            end = $('#slider-range').slider("values")[1]; 
            url = 'sp='+start+'&ep='+end;


            if(findcolor!= ''){
              url += '&'+findcolor;
            }
            if(findcat!= ''){
              url += '&'+findcat;
            }
            if(findOrderBy!= ''){
              url += '&'+findOrderBy;
            }

            window.location.href = baseurl+'products/?'+url;
               
         }

      </script>
      <script>
         function openCity(evt, cityName) {
           var i, tabcontent, tablinks;
           tabcontent = document.getElementsByClassName("tabcontent");
           for (i = 0; i < tabcontent.length; i++) {
             tabcontent[i].style.display = "none";
           }
           tablinks = document.getElementsByClassName("tablinks");
           for (i = 0; i < tablinks.length; i++) {
             tablinks[i].className = tablinks[i].className.replace(" active", "");
           }
           document.getElementById(cityName).style.display = "block";
           evt.currentTarget.className += " active";
         }
         
         // Get the element with id="defaultOpen" and click on it
         document.getElementById("defaultOpen").click();
      </script>
      <script type="text/javascript">
         document.addEventListener('DOMContentLoaded', createSelect, false);
         function createSelect() {
             var select = document.getElementsByTagName('select'),
               liElement,
               ulElement,
               optionValue,
               iElement,
               optionText,
               selectDropdown,
               elementParentSpan;
         
               for (var select_i = 0, len = select.length; select_i < len; select_i++) {
                 //console.log('selects init');
         
               select[select_i].style.display = 'none';
               wrapElement(document.getElementById(select[select_i].id), document.createElement('div'), select_i, select[select_i].getAttribute('placeholder-text'));
         
               for (var i = 0; i < select[select_i].options.length; i++) {
                 liElement = document.createElement("li");
                 optionValue = select[select_i].options[i].value;
                 optionText = document.createTextNode(select[select_i].options[i].text);
                 liElement.className = 'select-dropdown__list-item';
                 liElement.setAttribute('data-value', optionValue);
                 liElement.appendChild(optionText);
                 ulElement.appendChild(liElement);
         
                 liElement.addEventListener('click', function () {
                   displyUl(this);
                 }, false);
               }
             }
             function wrapElement(el, wrapper, i, placeholder) {
               el.parentNode.insertBefore(wrapper, el);
               wrapper.appendChild(el);
         
               document.addEventListener('click', function (e) {
                 let clickInside = wrapper.contains(e.target);
                 if (!clickInside) {
                   let menu = wrapper.getElementsByClassName('select-dropdown__list');
                   menu[0].classList.remove('active');
                 }
               });
         
               var buttonElement = document.createElement("button"),
                 spanElement = document.createElement("span"),
                 spanText = document.createTextNode(placeholder);
                 iElement = document.createElement("i");
                 ulElement = document.createElement("ul");
         
               wrapper.className = 'select-dropdown select-dropdown--' + i;
               buttonElement.className = 'select-dropdown__button select-dropdown__button--' + i;
               buttonElement.setAttribute('data-value', '');
               buttonElement.setAttribute('type', 'button');
               spanElement.className = 'select-dropdown select-dropdown--' + i;
               iElement.className = 'fa fa-chevron-down';
               ulElement.className = 'select-dropdown__list select-dropdown__list--' + i;
               ulElement.id = 'select-dropdown__list-' + i;
         
               wrapper.appendChild(buttonElement);
               spanElement.appendChild(spanText);
               buttonElement.appendChild(spanElement);
               buttonElement.appendChild(iElement);
               wrapper.appendChild(ulElement);
             }
         
             function displyUl(element) {
         
               if (element.tagName == 'BUTTON') {
                 selectDropdown = element.parentNode.getElementsByTagName('ul');
                 //var labelWrapper = document.getElementsByClassName('js-label-wrapper');
                 for (var i = 0, len = selectDropdown.length; i < len; i++) {
                   selectDropdown[i].classList.toggle("active");
                   //var parentNode = $(selectDropdown[i]).closest('.js-label-wrapper');
                   //parentNode[0].classList.toggle("active");
                 }
               } else if (element.tagName == 'LI') {
                 var selectId = element.parentNode.parentNode.getElementsByTagName('select')[0];
                 selectElement(selectId.id, element.getAttribute('data-value'));
                 elementParentSpan = element.parentNode.parentNode.getElementsByTagName('span');
                 element.parentNode.classList.toggle("active");
                 elementParentSpan[0].textContent = element.textContent;
                 elementParentSpan[0].parentNode.setAttribute('data-value', element.getAttribute('data-value'));
               }
         
             }
             function selectElement(id, valueToSelect) {
               var element = document.getElementById(id);
               element.value = valueToSelect;
               element.setAttribute('selected', 'selected');
             }
             var buttonSelect = document.getElementsByClassName('select-dropdown__button');
             for (var i = 0, len = buttonSelect.length; i < len; i++) {
               buttonSelect[i].addEventListener('click', function (e) {
                 e.preventDefault();
                 displyUl(this);
               }, false);
             }
         }
      </script>  

      @endsection 
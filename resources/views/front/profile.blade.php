@extends('front.layouts.app')
    
   @section('content')

	<div class="clearfix"></div>
	 <!-- inner-page-content-start -->
      <section class="container-fluid my-5" style="width: 60%; margin-top: 75px;">
         <div class="my-account">
            <div class="tab">
               <div class="container-fluid text-center">
         			<img src="https://i2.cdn.turner.com/cnnnext/dam/assets/140926165711-john-sutter-profile-image-large-169.jpg" alt="" class="image--cover" />
      		   </div>
               <button class="tablinks" onclick="openCity(event, 'profile')" id="defaultOpen">Profile</button>
               <button class="tablinks" onclick="openCity(event, 'order')">Order History</button>
               <button class="tablinks" onclick="openCity(event, 'change-password')">Change Password</button>
               <button class="tablinks" onclick="openCity(event, 'addresses')">Addresses</button>
               <button class="tablinks" id="logoutbutton" onclick="logout()">Log out</button>
            </div>
            <div id="order" class="tabcontent">
               <div class="table-responsive">
                  <table class="table ">
                     <!-- Head Row -->
                     <thead>
                        <tr>
                           <th style="text-align: center;" colspan="" rowspan="" headers="" scope="">Id</th>
                           <th style="text-align: center;" colspan="" rowspan="" headers="" scope="">Image</th>
                           <th style="text-align: center;" colspan="" rowspan="" headers="" scope="">Name</th>
                           <th style="text-align: center;" colspan="" rowspan="" headers="" scope="">Size</th>
                           <th style="text-align: center;" colspan="" rowspan="" headers="" scope="">Price</th>
                           <th style="text-align: center;" colspan="" rowspan="" headers="" scope="">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <!-- Product Row -->
                        <tr>
                           <td colspan="" rowspan="" headers="">#12345</td>
                           <td colspan="" rowspan="" headers=""><img src="images/part_2.jpg" alt="" class="img-responsive"></td>
                           <td colspan="" rowspan="" headers="">Lorem Ipsum</td>
                           <td colspan="" rowspan="" headers="">1800mm. X 2500mm.</td>
                           <td colspan="" rowspan="" headers="">₹200</td>
                           <td class="action-btns"><a href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                        <!-- Product Row -->
                        <tr>
                           <td colspan="" rowspan="" headers="">#12345</td>
                           <td colspan="" rowspan="" headers=""><img src="images/part_2.jpg" alt="" class="img-responsive"></td>
                           <td colspan="" rowspan="" headers="">Lorem Ipsum</td>
                           <td colspan="" rowspan="" headers="">1800mm. X 2500mm.</td>
                           <td colspan="" rowspan="" headers="">₹200</td>
                           <td class="action-btns"><a href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                        <!-- Discount Row  -->
                        <tr>
                           <td colspan="" rowspan="" headers="">#12345</td>
                           <td colspan="" rowspan="" headers=""><img src="images/part_2.jpg" alt="" class="img-responsive"></td>
                           <td colspan="" rowspan="" headers="">Lorem Ipsum</td>
                           <td colspan="" rowspan="" headers="">1800mm. X 2500mm.</td>
                           <td colspan="" rowspan="" headers="">₹200</td>
                           <td class="action-btns"><a href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
            <div id="profile" class="tabcontent">
               <form class="form" method="post" action="{{ route('user.userprofilesubmit') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="userId" id="userId" value="{{(isset($userDetails->id) && !empty($userDetails->id)?$userDetails->id:'')}}">

                  <div class="row mb-20">
                     <div class="col-sm-6">
                        @php $Username = explode(' ',$userDetails->name) @endphp
                        <label>First Name *</label>
                        <input type="text" class="form-control" name="first_name" readonly="" value="{{ (isset($Username[0]) && !empty($Username[0])?$Username[0]:'') }}">
                        <p class="text-muted">{{ $errors->first('first_name') }}</p>
                     </div>
                     <div class="col-sm-6">
                        <label>Last Name *</label>
                        <input type="text" class="form-control" name="last_name" readonly="" value="{{ (isset($Username[1]) && !empty($Username[1])?$Username[1]:'') }}">
                        <p class="text-muted">{{ $errors->first('last_name') }}</p>
                     </div>
                  </div>
                  <div class="row mb-20">
                     <div class="col-sm-6">
                        <label>Phone Number *</label>
                        <input type="text" class="form-control" name="number" id="number" required="" value="{{ (isset($userDetails->number) && !empty($userDetails->number)?$userDetails->number:'') }}">
                        <p class="text-muted">{{ $errors->first('number') }}</p>
                     </div>
                     <div class="col-sm-6">
                        <label>Email address *</label>
                        <input type="email" class="form-control" name="email" readonly="" value="{{ (isset($userDetails->email) && !empty($userDetails->email)?$userDetails->email:'') }}">
                        <p class="text-muted">{{ $errors->first('email') }}</p>
                     </div>
                  </div>
                  <div class="mb-20">
                     <label>Address </label>
                     <textarea class="form-control" name="address" id="address" rows="4">{{ (isset($userDetails->address) && !empty($userDetails->address)?$userDetails->address:'') }}</textarea>
                     <p class="text-muted">{{ $errors->first('address') }}</p>
                  </div>
                  <div class="row mb-20">
                     <div class="col-sm-6">
                        <label>Organization Type</label>
                        <select class="form-control" name="organizationtype" id="organizationtype">
                           <option value="">You are</option>
                           <option value="Entrepreneur" {{ $userDetails->organizationtype == 'Entrepreneur' ? "selected" : "" }}>Entrepreneur</option>
                           <option value="Proprietor" {{ $userDetails->organizationtype == 'Proprietor' ? "selected" : "" }}>Proprietor</option>
                           <option value="Manufacturer" {{ $userDetails->organizationtype == 'Manufacturer' ? "selected" : "" }}>Manufacturer</option>
                           <option value="Wholesaler" {{ $userDetails->organizationtype == 'Wholesaler' ? "selected" : "" }}>Wholesaler</option>
                           <option value="Trader" {{ $userDetails->organizationtype == 'Trader' ? "selected" : "" }}>Trader</option>
                           <option value="Individual" {{ $userDetails->organizationtype == 'Individual' ? "selected" : "" }}>Individual</option>
                        </select>
                        <p class="text-muted">{{ $errors->first('organizationtype') }}</p>

                     </div>
                     <div class="col-sm-6">
                        <label>Participation</label>
                        <select class="form-control" name="participation" id="participation" >
                           <option {{ $userDetails->participation == '' ? "selected" : "" }} value="">Participation</option>
                           <option {{ $userDetails->participation == '2' ? "selected" : "" }} value="2">As an affiliate only</option>
                           <option {{ $userDetails->participation == '3' ? "selected" : "" }} value="3">As a supplier only</option>
                           <option {{ $userDetails->participation == '4' ? "selected" : "" }} value="4">As both supplier & affiliate</option>
                           <option {{ $userDetails->participation == '5' ? "selected" : "" }} value="5">As a buyer</option>
                        </select>
                        <p class="text-muted">{{ $errors->first('participation') }}</p>

                     </div>
                  </div>
                  
                  @if(isset($userDetails->participation) &&  ($userDetails->participation =='2' || $userDetails->participation =='3' || $userDetails->participation =='4'))
                     @php $style = 'style=display:block;' @endphp
                  @else 
                     @php $style = 'style=display:none;' @endphp
                  @endif
                  <hr class="bankinfo" {{ $style }}>
                  <!-- <div class="">
                     <p style="font-size: 18px; color: #ff6600;">BANK DETAILS</p>
                  </div> -->
                  
                  <div class="row mb-20 bankinfo" {{ $style }}>
                     <div class="col-sm-6">
                        <label>Bank Name</label>
                        <input type="text" class="form-control" name="bankname" id="bankname" value="{{ (isset($userDetails->bankname) && !empty($userDetails->bankname)?$userDetails->bankname:'') }}" required="">
                        <p class="text-muted">{{ $errors->first('bankname') }}</p>
                     </div>
                     <div class="col-sm-6">
                        <label>IFSC Code</label>
                        <input type="text" class="form-control" name="ifsccode" id="ifsccode" value="{{ (isset($userDetails->ifsccode) && !empty($userDetails->ifsccode)?$userDetails->ifsccode:'') }}" required="">
                        <p class="text-muted">{{ $errors->first('ifsccode') }}</p>
                     </div>
                  </div>
                  <div class="row mb-20 bankinfo" {{ $style }}>
                     <div class="col-sm-6">
                        <label>A/C Number</label>
                        <input type="text" class="form-control" name="acnumber" id="acnumber" value="{{ (isset($userDetails->acnumber) && !empty($userDetails->acnumber)?$userDetails->acnumber:'') }}" required="">
                        <p class="text-muted">{{ $errors->first('acnumber') }}</p>
                     </div>
                  </div>
                  <hr class="extrainfo" {{ $style }}>
                  <!-- <div class="">
                     <p style="font-size: 18px; color: #ff6600;">PERSONAL DETAILS</p>
                  </div> -->

                  <div class="row mb-20 extrainfo" {{ $style }}>
                     <div class="col-sm-6">
                        <label>Aadhar No</label>
                        <input type="text" class="form-control" name="aadhar" id="aadhar" value="{{ (isset($userDetails->aadhar) && !empty($userDetails->aadhar)?$userDetails->aadhar:'') }}" required="">
                        <p class="text-muted">{{ $errors->first('aadhar') }}</p>
                     </div>
                     <div class="col-sm-6">
                        <label>PAN No</label>
                        <input type="text" class="form-control" name="panno" id="panno" value="{{ (isset($userDetails->panno) && !empty($userDetails->panno)?$userDetails->panno:'') }}" required="">
                        <p class="text-muted">{{ $errors->first('panno') }}</p>
                     </div>
                  </div>
                  <div class="row mb-20 extrainfo" {{ $style }}>
                     <div class="col-sm-6">
                        <label>Individual Code</label>
                        <input type="text" class="form-control" name="individualcode" id="individualcode" value="{{ (isset($userDetails->individualcode) && !empty($userDetails->individualcode)?$userDetails->individualcode:'') }}" required="">
                        <p class="text-muted">{{ $errors->first('individualcode') }}</p>
                     </div>
                  </div>
                  <button type="submit" class="btn btn-reveal-right">SAVE CHANGES <i class="d-icon-arrow-right"></i></button>
               </form>
            </div>
            <div id="change-password" class="tabcontent">
               <form class="form" method="post" action="{{ route('user.changePasswordSubmit') }}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="userId" id="userId" value="{{(isset($userDetails->id) && !empty($userDetails->id)?$userDetails->id:'')}}">
                  <div class="mb-20">
                     <label>Current password (leave blank to leave unchanged)</label>
                     <input type="password" class="form-control" name="old_password" id="old_password">
                     <p class="text-muted">{{ $errors->first('old_password') }}</p>
                  </div>
                  <div class="mb-20">
                     <label>New password (leave blank to leave unchanged)</label>
                     <input type="password" class="form-control" name="password" id="password">
                     <p class="text-muted">{{ $errors->first('password') }}</p>
                  </div>
                  <div class="mb-20">
                     <label>Confirm new password</label>
                     <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                     <p class="text-muted">{{ $errors->first('confirm_password') }}</p>
                  </div>
                  <button type="submit" class="btn btn-reveal-right">SAVE CHANGES <i class="d-icon-arrow-right"></i></button>
               </form>
            </div>
            <div id="addresses" class="tabcontent">
               <div class="row">
                  <div class="col-lg-6 mb-4">
                     <div class="card card-address">
                        <div class="card-body">
                           <h5 class="card-title">Billing Address</h5>
                           <p>User Name<br>
                              User Company<br>
                              John str<br>
                              New York, NY 10001<br>
                              1-234-987-6543<br>
                              yourmail@mail.com<br>
                           </p>
                           <a href="#" class="btn btn-link btn-secondary btn-underline">Edit <i class="fa fa-edit"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 mb-4">
                     <div class="card card-address">
                        <div class="card-body">
                           <h5 class="card-title">Shipping Address</h5>
                           <p>You have not set up this type of address yet.</p>
                           <a href="#" class="btn btn-link btn-secondary btn-underline">Edit <i class="fa fa-edit"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
         <div class="clearfix"></div>
      </section>
      <!-- inner-page-content-end -->
      
   @endsection    

   @section('profilescriptjs')
      <script type="text/javascript">
         // Get the element with id="defaultOpen" and click on it
         document.getElementById("defaultOpen").click();


         var words = document.getElementsByClassName("word");
         var wordArray = [];
         var currentWord = 0;

         words[currentWord].style.opacity = 1;
         for (var i = 0; i < words.length; i++) {
           splitLetters(words[i]);
         }

         function changeWord() {
           var cw = wordArray[currentWord];
           var nw =
             currentWord == words.length - 1 ? wordArray[0] : wordArray[currentWord + 1];
           for (var i = 0; i < cw.length; i++) {
             animateLetterOut(cw, i);
           }

           for (var i = 0; i < nw.length; i++) {
             nw[i].className = "letter behind";
             nw[0].parentElement.style.opacity = 1;
             animateLetterIn(nw, i);
           }

           currentWord = currentWord == wordArray.length - 1 ? 0 : currentWord + 1;
         }

         function setLeftValue() {
         var _this = inputLeft,
         min = parseInt(_this.min),
         max = parseInt(_this.max);

         _this.value = Math.min(parseInt(_this.value), parseInt(inputRight.value) - 1);

         var percent = ((_this.value - min) / (max - min)) * 100;

         thumbLeft.style.left = percent + "%";
         range.style.left = percent + "%";
         amountLeft.innerText = parseInt(percent * 100);
         }
         setLeftValue();

         function setRightValue() {
         var _this = inputRight,
         min = parseInt(_this.min),
         max = parseInt(_this.max);

         _this.value = Math.max(parseInt(_this.value), parseInt(inputLeft.value) + 1);

         var percent = ((_this.value - min) / (max - min)) * 100;

         amountRight.innerText = parseInt(percent * 100);
         thumbRight.style.right = (100 - percent) + "%";
         range.style.right = (100 - percent) + "%";
         }
         setRightValue();

         inputLeft.addEventListener("input", setLeftValue);
         inputRight.addEventListener("input", setRightValue);

         inputLeft.addEventListener("mouseover", function () {
         thumbLeft.classList.add("hover");
         });
         inputLeft.addEventListener("mouseout", function () {
         thumbLeft.classList.remove("hover");
         });
         inputLeft.addEventListener("mousedown", function () {
         thumbLeft.classList.add("active");
         });
         inputLeft.addEventListener("mouseup", function () {
         thumbLeft.classList.remove("active");
         });

         inputRight.addEventListener("mouseover", function () {
         thumbRight.classList.add("hover");
         });
         inputRight.addEventListener("mouseout", function () {
         thumbRight.classList.remove("hover");
         });
         inputRight.addEventListener("mousedown", function () {
         thumbRight.classList.add("active");
         });
         inputRight.addEventListener("mouseup", function () {
         thumbRight.classList.remove("active");
         });


      </script>
   @endsection
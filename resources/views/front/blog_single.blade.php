@include('blog_single_header')

<div id="fh5co-title-box" style="background-image: url('{{ url('/') }}/public/uploads/blog_images/{{ $blog_details->blog_image }}'); background-position: 50% 90.5px;" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="page-title">
        <span>{{ date('M d, Y',strtotime($blog_details->blog_added_on)) }}</span>
        <h2>{{ $blog_details->blog_title }}</h2>
    </div>
</div>
<div id="fh5co-single-content" class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <p>
                    <?php echo $blog_details->blog_description; ?>
                </p>
            </div>
            <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                <!--<div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Tags</div>
                </div>
                <div class="clearfix"></div>
                <div class="fh5co_tags_all">
                    <a href="#" class="fh5co_tagg">Business</a>
                    <a href="#" class="fh5co_tagg">Technology</a>
                    <a href="#" class="fh5co_tagg">Sport</a>
                    <a href="#" class="fh5co_tagg">Art</a>
                    <a href="#" class="fh5co_tagg">Lifestyle</a>
                    <a href="#" class="fh5co_tagg">Three</a>
                    <a href="#" class="fh5co_tagg">Photography</a>
                    <a href="#" class="fh5co_tagg">Lifestyle</a>
                    <a href="#" class="fh5co_tagg">Art</a>
                    <a href="#" class="fh5co_tagg">Education</a>
                    <a href="#" class="fh5co_tagg">Social</a>
                    <a href="#" class="fh5co_tagg">Three</a>
                </div>-->
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom pt-3 py-2 mb-4">Most Popular</div>
                </div>
				@foreach($popular_posts as $pop_posts)
                <div class="row pb-3">
                    <div class="col-5 align-self-center">
					<a href="{{ url('/blog_single') }}/{{ $pop_posts->blog_slug }}" style="text-decoration: none;">
                        <img src="{{ url('/') }}/public/uploads/blog_images/{{ $pop_posts->blog_image }}" alt="img" class="fh5co_most_trading"/>
					</a>
                    </div>
                    <div class="col-7 paddding">
                        <div class="most_fh5co_treding_font">
						<a href="{{ url('/blog_single') }}/{{ $pop_posts->blog_slug }}" style="text-decoration: none;color: #222 !important;">
							{!! substr(strip_tags($pop_posts->blog_title),0, 50) !!}...</div>
					    </a>
                        <div class="most_fh5co_treding_font_123">{{ date('M d, Y',strtotime($pop_posts->blog_added_on)) }}</div>
                    </div>
                </div>
				@endforeach
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pb-4 pt-5">
    <div class="container animate-box">
        <div>
            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Releted Posts</div>
        </div>
        <div class="owl-carousel owl-theme" id="slider2">
		@foreach($releted_posts as $rel_post)
            <div class="item px-2">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img">
						<a href="{{ url('/blog_single') }}/{{ $rel_post->blog_slug }}" style="text-decoration: none;">
							<img src="{{ url('/') }}/public/uploads/blog_images/{{ $rel_post->blog_image }}" alt="Blog Image"/>
						</a>
					</div>
                    <div>
                        <a href="{{ url('/blog_single') }}/{{ $rel_post->blog_slug }}" class="d-block fh5co_small_post_heading"><span class="">{!! substr(strip_tags($rel_post->blog_title),0, 50) !!}...</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i>{{ date('M d, Y',strtotime($rel_post->blog_added_on)) }}</div>
                    </div>
                </div>
            </div>
			@endforeach
        </div>
    </div>
</div>

@include('blog_single_footer')
@extends('admin.layouts.login')
@section('content')
<div class="middle">
   <div id="login">
      <form action="{{ route('adminuser.auth') }}" method="post">
      	{{ csrf_field() }}
         <fieldset class="clearfix">
            <p ><span class="fa fa-user"></span><input type="text" class="text"  Placeholder="E-mail address" name="user_name" id="user_name"  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E-mail address';}"></p>
            @error('email')
               <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
               </span>
            @enderror
            <!-- JS because of IE support; better: placeholder="Username" -->
            <p><span class="fa fa-lock"></span><input type="password" Placeholder="Password" name="password" id="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"></p>
            @error('password')
               <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
               </span>
            @enderror
            <!-- JS because of IE support; better: placeholder="Password" -->
            <div>
               <span style="width:48%; text-align:left;  display: inline-block;"><a class="small-text" href="#">Forgot
               password?</a></span>
               <span style="width:50%; text-align:right;  display: inline-block;"><input onclick="myFunction()" type="submit" value="Sign In"></span>
            </div>
         </fieldset>
         <div class="clearfix"></div>
      </form>
      <div class="clearfix"></div>
   </div>
   <!-- end login -->
   <div class="logo">
      <img src="{{ asset('images/admin_logo.png') }}" alt=""/>
      <div class="clearfix"></div>
   </div>
</div>
@endsection
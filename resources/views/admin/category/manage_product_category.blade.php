@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
         	<div class="col-lg-12 col-md-12 col-sm-12 text-left" style="padding: 0;"><a href="{{ route('add_product_category') }}" class="btn btn-warning">ADD PRODUCT CATEGORY</a></div>
            <div class="card">
               <div class="card-body">
                  <div class="table-responsive">
                      <table id="productcategorytable" class="table table-striped table-bordered" style="width:100%">
                        <thead class=" text-primary">
                          <tr style="height: 58px;text-align: center;"> 
                             
					          <th>Product Category Name</th>
							  <th>Product Parent Category Name</th>
							  <th>Product Category Status</th>
					          <th>Action</th>
					         
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($product_category_details as $product_category)
					         <tr class="active" >
						          <td >{{ $product_category->product_category_name }}</td>
						          <td >{{ $product_category->product_parent_category }}</td>
						          <td style="text-align: center;" >
								  @if($product_category->product_category_status == 1)
									<a class="btn-success btn btn-status"> Active </a>
								  @endif
								  @if($product_category->product_category_status == 2)
									<a class="btn-danger btn btn-status">Inactive</a>
								  @endif
								  </td> 
								  <td style="text-align: center;">
										<?php if($product_category->product_category_status == 1) { ?>
										<a href="change_product_category_status/{{ $product_category->product_category_id }}/{{ $product_category->product_category_status }}" onclick="return confirm('Are you sure to inactive this product_category?')"><i class="fa fa-2x fa-check" aria-hidden="true"></i></a>&nbsp;&nbsp;
										<?php } if($product_category->product_category_status == 2) { ?>
										<a href="change_product_category_status/{{ $product_category->product_category_id }}/{{ $product_category->product_category_status }}" onclick="return confirm('Are you sure to active this product_category?')"><i class="fa fa-2x fa-times" aria-hidden="true"></i></a>&nbsp;&nbsp;
										<?php } ?>
										<a href="edit_product_category/{{ $product_category->product_category_id }}"><i class="fa fa-2x fa-pencil-square-o" aria-hidden="true"></i></a>
										<a href="delete_product_category/{{ $product_category->product_category_id }}" onclick="return confirm('Are you sure to delete this product_category?')"><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></a>
								  </td>
							 </tr> 
					      @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scriptjs')
<script>
	$(document).ready(function() {
    	$('#productcategorytable').DataTable();
	} );
</script>
@endsection
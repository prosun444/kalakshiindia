@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Category</h4>
               </div>
               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ url('admin_manage7081/add_product_category_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <label class="label-input100" for="first-name">Product Category Name *</label>
                        <div class="wrap-input100 validate-input">
                           <input id="product_category_name" class="input100 {{ $errors->has('product_category_name') ? 'error' : '' }}" type="text" name="product_category_name" id="blog_title" placeholder="Enter Product Category Name" value="{{ old('product_category_name') }}">
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="email">Product Category Slug *</label>
                        <div class="wrap-input100 validate-input">
                           <input class="input100 {{ $errors->has('product_category_slug') ? 'error' : '' }}" type="text" id="product_category_slug" name="product_category_slug" placeholder="Enter Product Category Slug" value="{{ old('product_category_slug') }}">
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Product Parent Category</label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('product_parent_category') ? 'error' : '' }}" name="product_parent_category" id="product_parent_category">
                              <option value="">Select Parent Category</option>
                              @foreach($product_parent_category_details as $parent_cat)
                              <option value="{{ $parent_cat->product_category_id }}">{{ $parent_cat->product_category_name }}</option>
                              @endforeach
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_product_category') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn">
                           <span>
                           Save Product Category
                           <i class="zmdi zmdi-arrow-right m-l-8"></i>
                           </span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('js')
<script>
   $(document).ready(function () {
   
   	$("#blog_category").select2({
          placeholder: "Select Category",
          allowClear: true
    });
   
   	CKEDITOR.replace('blog_description');
   //$("#upload_div").hide();
       $('#blog_title').bind('keyup keypress blur', function () {
           var myStr = $(this).val();
           myStr = myStr.toLowerCase();
           myStr = myStr.replace(/ /g, "-");
           myStr = myStr.replace(/[^a-zA-Z0-9\.]+/g, "-");
           myStr = myStr.replace(/\.+/g, "-");
           $('#blog_slug').val(myStr);
       });
   });
</script>
@endsection
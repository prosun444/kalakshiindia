@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
         	<div class="col-lg-12 col-md-12 col-sm-12 text-left" style="margin-bottom: 14px; padding: 0;"><a href="{{ url('admin_manage7081/add_blog') }}" class="btn btn-warning">ADD BLOG</a></div>	
            <div class="card">
               <div class="card-body">
                  <div class="table-responsive">
                      <table id="blogtable" class="table table-striped table-bordered" style="width:100%">
                        <thead class=" text-primary">
                          <tr style="height: 58px;text-align: center;"> 
                             
					            <th >Blog ID</th>
					            <th >Blog Title</th>
					            <th >Blog Added On</th>
					            <th >Action</th>
					         
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($blog_details as $blog)
					         <tr style="text-align: center;">
					            	<td >{{ $blog->blog_id }}</td>
					            	<td >{{ $blog->blog_title }}</td>
					            	<td >{{ $blog->blog_added_on }}</td>
					            	<td >
						               <?php if($blog->blog_status == 1) { ?>
						               <a href="change_blog_status/{{ $blog->blog_id }}/{{ $blog->blog_status }}" onclick="return confirm('Are you sure to inactive this blog?')"><i class="fa fa-2x fa-check" aria-hidden="true"></i></a>&nbsp;&nbsp;
						               <?php } if($blog->blog_status == 2) { ?>
						               <a href="change_blog_status/{{ $blog->blog_id }}/{{ $blog->blog_status }}" onclick="return confirm('Are you sure to active this blog?')"><i class="fa fa-2x fa-times" aria-hidden="true"></i></a>&nbsp;&nbsp;
						               <?php } ?>
						               <a href="edit_blog/{{ $blog->blog_id }}"><i class="fa fa-2x fa-pencil-square-o" aria-hidden="true"></i></a>
						               <a href="delete_blog/{{ $blog->blog_id }}" onclick="return confirm('Are you sure to delete this blog?')"><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></a>
					               </td>
					         </tr>
					      @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scriptjs')
<script>
	$(document).ready(function() {
    	$('#blogtable').DataTable();
	} );
</script>
@endsection
@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Update Blog</h4>
               </div>
               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ url('admin_manage7081/edit_blog_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control1" name="blog_id" id="blog_id" value="{{ $blog_detail->blog_id }}">
                        <label class="label-input100" for="first-name">Blog Title *</label>
                        <div class="wrap-input100 validate-input">
                           <input id="first-name" class="input100 {{ $errors->has('blog_title') ? 'error' : '' }}" type="text" name="blog_title" id="blog_title" placeholder="Enter Blog Title" value="{{ $blog_detail->blog_title }}">
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="email">Blog Slug *</label>
                        <div class="wrap-input100 validate-input">
                           <input class="input100 {{ $errors->has('blog_slug') ? 'error' : '' }}" type="text" id="blog_slug" name="blog_slug" placeholder="Enter Blog Slug" value="{{ $blog_detail->blog_slug }}">
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Blog Category</label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('blog_category') ? 'error' : '' }}" name="blog_category" id="blog_category">
                              <option value="">Select Category</option>
                              @foreach($category_details as $cat)
                              <option value="{{ $cat->blog_category_id }}" {{ ( $blog_detail->blog_category == $cat->blog_category_id ) ? 'selected' : '' }} >{{ $cat->blog_category_name }}</option>
                              @endforeach
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="message">Blog Description *</label>
                        <div class="wrap-input100 validate-input">
                           <textarea class="input100 {{ $errors->has('blog_description') ? 'error' : '' }}" name="blog_description" id="blog_description" placeholder="Please enter your comments...">{{ $blog_detail->blog_description }}</textarea>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="message">Blog Image *</label>
                        <div class="">
                           <img src="{{ url("/") }}/uploads/blog_images/{{ $blog_detail->blog_image }}" height="200px" width="200px">
                           <input type="file" name="blog_img" id="blog_img">
                           <span class="focus-input100"></span>
                        </div>
                        @if($errors->has('blog_img'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('blog_img') }}</span>
                        @endif
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_blog') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn">
                           <span>
                           Update Blog
                           <i class="zmdi zmdi-arrow-right m-l-8"></i>
                           </span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('scriptjs')
<script>
   $(document).ready(function () {
 
   tinymce.init({
           selector:'textarea#blog_description',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
    });
   	
   //$("#upload_div").hide();
       $('#blog_title').bind('keyup keypress blur', function () {
           var myStr = $(this).val();
           myStr = myStr.toLowerCase();
           myStr = myStr.replace(/ /g, "-");
           myStr = myStr.replace(/[^a-zA-Z0-9\.]+/g, "-");
           myStr = myStr.replace(/\.+/g, "-");
           $('#blog_slug').val(myStr);
       });
   });
</script>
@endsection
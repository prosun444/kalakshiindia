@extends('admin.layouts.main')

@section('csscontent')
<style type="text/css">
  
  .cp-container {
  position: relative;
  .colorpicker-bs-popover {
    transform: none!important;
    top: 100%!important;
    width: 100%;
    max-width: none;
    .colorpicker {
      width: 100%;
      .colorpicker-saturation,
      .colorpicker-hue,
      .colorpicker-alpha,
      .colorpicker-preview,
      .colorpicker-bar {
        width: 100%;
        box-shadow: none;
      }
      .colorpicker-saturation {
        height: 150px;
        .colorpicker-guide {
          height: 10px;
          width: 10px;
          border-radius: 10px;
          margin: -5px 0 0 -5px;
        }
      }
      .colorpicker-hue,
      .colorpicker-alpha,
      .colorpicker-preview,
      .colorpicker-bar {
        margin-top: 10px;
      }
      
      .colorpicker-hue,
      .colorpicker-alpha,
      .colorpicker-preview {
        height: 30px;
      }
      .colorpicker-alpha,
      .colorpicker-preview {
        background-size: 20px 20px;
        background-position: 0 0, 10px 10px;
      }
      .colorpicker-preview {
        font-size: 1rem;
        line-height: 1.75;
      }
    }
  }
}

// For testing only
.cp-container {
  max-width: 350px;
  margin: 2rem auto;
}
</style>
@endsection 

@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Update Product</h4>
               </div>
               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ route('edit_product_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{(isset($product->id) && !empty($product->id))?$product->id:''}}">
                        <label class="label-input100" for="first-name">Product Name <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100 validate-input">
                           <input id="product_name" class="input100 {{ $errors->has('product_name') ? 'error' : '' }}" type="text" name="product_name" id="product_name" placeholder="Enter Product Name" value="{{ (isset($product->product_name) && !empty($product->product_name))?$product->product_name:''}}">
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="email">Product Code <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100 validate-input">
                           <input class="input100 {{ $errors->has('product_code') ? 'error' : '' }}" type="text" id="product_code" name="product_code" placeholder="Enter Blog Slug" value="{{ (isset($product->product_code) && !empty($product->product_code))?$product->product_code:''}}" readonly>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Product Category <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('product_category') ? 'error' : '' }}" name="product_category" id="product_category">
                              <option value="">Select</option>
                              @if(isset($product->category) && !empty($product->category))
                              @foreach($product->category as $key=>$val)
                              <option value="{{$val->product_category_id}}" @if($product->product_category == $val->product_category_id) selected @endif > {{$val->product_category_name}} </option>
                              @endforeach
                              @endif
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Product Subcategory <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <select class="browser-default " name="product_subcategory" id="product_subcategory">
                              <option value="">Select</option>
                              @if(isset($product->allsubcategory) && !empty($product->allsubcategory))
                              @foreach($product->allsubcategory as $key=>$val)
                              <option value="{{$val->product_category_id}}" @if($product->product_subcategory == $val->product_category_id) selected @endif > {{$val->product_category_name}} </option>
                              @endforeach
                              @endif
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Product Type <span style="color: red !important;">*</span></label>
                        <div class="" style="margin-bottom: 25px;">
                           <label class="radio-inline" style="display: inline;">
                           <input type="radio" name="product_type" value="1" @if(isset($product->product_type) && !empty($product->product_type) && $product->product_type == 1) checked @endif > Normal Product</label>
                           <label class="radio-inline" style="display: inline;">
                           <input type="radio" name="product_type" value="2" @if(isset($product->product_type) && !empty($product->product_type) && $product->product_type == 2) checked @endif > Elite Product</label>
                           <span class="focus-input100"></span>
                        </div>
                        <label for="product_price" class="label-input100">Product Price<span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <input type="text" class="input100 {{ $errors->has('product_price') ? 'error' : '' }}" name="product_price" id="product_price" placeholder="Enter Product Price" value="{{ (isset($product->product_price) && !empty($product->product_price))?$product->product_price:''}}">
                           <span class="focus-input100"></span>
                        </div>
                        <label for="product_colour" class="label-input100">Product Colour<span style="color: red !important;">*</span></label>

                        <div class="wrap-input100">
                          <SELECT class="form-control1" name="product_colour" id="product_colour">
                            <option value="">Select</option>
                            @if(isset($product->colour) && !empty($product->colour))
                            @foreach($product->colour as $key=>$colour)
                            <option style="background-color: {{$colour->colour_code}};" value="{{$colour->colour_slug}}" @if($product->product_colour == $colour->colour_slug) selected @endif>{{$colour->colour}}</option>
                            @endforeach
                            @endif
                          </SELECT>
                          @if($errors->has('product_colour'))
                            <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('product_colour') }}</span>
                          @endif
                        </div>

                        <!-- <div class="cp-container wrap-input100" id="cp3-container">
                            <div class="input-group" title="Using input value">
                              <input id="cp3" type="text" name="product_colour" class="input100 {{ $errors->has('product_colour') ? 'error' : '' }}" value="{{$product->product_colour}}" autocomplete="off"/>
                            </div>
                        </div> -->

                        <!-- <div class="colorpick" style=" margin-bottom: 15px;">
                            <input type="color" name="color" style="width: 50px;height: 35px;" value="{{ (isset($product->product_colour) && !empty($product->product_colour))?$product->product_colour:''}}" />
                            <span id="span_actual_color">{{ (isset($product->product_colour_name) && !empty($product->product_colour_name))?$product->product_colour_name:''}}</span>

                            <input id="color-name" type="hidden" name="color-name" value="{{ (isset($product->product_colour_name) && !empty($product->product_colour_name))?$product->product_colour_name:''}}" />
                            
                            <div class="sample" id="default-sample">
                                <div style="display: none;" class="actual"></div><div style="display: none;" class="nearest"></div>
                            </div>
                        </div> -->


                        <label for="description" class="label-input100">Description<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="description {{ $errors->has('description') ? 'error' : '' }}" name="description" id="description">{{ (isset($product->description) && !empty($product->description))?$product->description:''}}</textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('description'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('description') }}</span>
                           @endif
                        </div>
                        <label style="padding-top: 30px;" for="usages" class="label-input100">Usages<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="usages {{ $errors->has('usages') ? 'error' : '' }}" name="usages" id="usages">{{ (isset($product->usages) && !empty($product->usages))?$product->usages:''}}</textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('usages'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('usages') }}</span>
                           @endif
                        </div>
                        <label style="padding-top: 30px;" for="special_tips" class="label-input100">Kalakshi Special tips<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="special_tips {{ $errors->has('special_tips') ? 'error' : '' }}" name="special_tips" id="special_tips">{{ (isset($product->special_tips) && !empty($product->special_tips))?$product->special_tips:''}}</textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('special_tips'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('special_tips') }}</span>
                           @endif
                        </div>
                        <label style="padding-top: 30px;" for="product_images"  class="label-input100" for="message">Product Images <span style="color: red !important;">*</span></label>
                        <div class="">
                           <input type="file" class="form-control1" name="product_images[]" id="product_images" placeholder="Enter Product Price" multiple>
                           @if($errors->has('product_images'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('product_images') }}</span>
                           @endif
                           <span class="focus-input100"></span>
                        </div>
                        <div class="">
                          <div class="col-sm-2"> </div>
                          <div class="col-sm-8">
                            @php $images = json_decode($product->product_images) @endphp
                            @if(isset($images) && !empty($images))
                            @foreach($images as $key=>$val)
                              <img src="{{url('picture/product/',$val)}}" width="100" height="100">
                            @endforeach
                            @endif
                          </div>
                        </div>  
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_blog') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn">
                           <span> Update Product<i class="zmdi zmdi-arrow-right m-l-8"></i></span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('scriptjs')
<script>
   $(document).ready(function () {
       tinymce.init({
           selector:'textarea.description',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });

       tinymce.init({
           selector:'textarea.usages',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });

       tinymce.init({
           selector:'textarea.special_tips',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });
   });

   // For testing purposes only
    /*console.clear();

    init_colorpicker_fn( '#cp2', 'rgb' );

    init_colorpicker_fn( '#cp3', 'hex' );

    function init_colorpicker_fn( id_str, format_str = 'hex' ) {
      
      if ( !id_str.startsWith( '#' ) ) {
        id_str = '#' + id_str;
      }
      
      var $picker_el = jQuery( id_str );

      $picker_el.colorpicker( {
        format: format_str,
        horizontal: true,
        popover: {
          container: id_str + '-container'
        },
        template: '<div class="colorpicker">' +
          '<div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>' +
          '<div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>' +
          '<div class="colorpicker-alpha">' +
          ' <div class="colorpicker-alpha-color"></div>' +
          ' <i class="colorpicker-guide"></i>' +
          '</div>' +
          '<div class="colorpicker-bar">' +
          ' <div class="input-group">' +
          '   <input class="form-control input-block color-io" />' +
          ' </div>' +
          '</div>' +
          '</div>'
      } ).on( 'colorpickerCreate colorpickerUpdate', function( e ) {
        $picker_el.parent().find( '.colorpicker-input-addon>i' ).css( 'background-color', e.value );
      } ).on( 'colorpickerCreate', function( e ) {
        resize_color_picker_fn( $picker_el );
      } ).on( 'colorpickerShow', function( e ) {
        var cpInput_el = e.colorpicker.popupHandler.popoverTip.find( '.color-io' );

        cpInput_el.val( e.color.string() );

        cpInput_el.on( 'change keyup', function() {
          e.colorpicker.setValue( cpInput_el.val() );
        } );
      } ).on( 'colorpickerHide', function( e ) {
        var cpInput_el = e.colorpicker.popupHandler.popoverTip.find( '.color-io' );
        cpInput_el.off( 'change keyup' );
      } ).on( 'colorpickerChange', function( e ) {
        var cpInput_el = e.colorpicker.popupHandler.popoverTip.find( '.color-io' );

        if ( e.value === cpInput_el.val() || !e.color || !e.color.isValid() ) {
          return;
        }

        cpInput_el.val( e.color.string() );
      } );

      $picker_el.parent().find( '.colorpicker-input-addon>i' ).on( 'click', function( e ) {
        $picker_el.colorpicker( 'colorpicker' ).show();
      } );

      jQuery( window ).resize( function( e ) {
        resize_color_picker_fn( $picker_el );
      } );
    }

    function resize_color_picker_fn( $picker_el ) {
      var rem_int = parseInt( getComputedStyle( document.documentElement ).fontSize ),
        width_int = $picker_el.parent().width() - ( ( rem_int * .75 ) * 2 ) - 2,
        colorPicker_obj = $picker_el.colorpicker( 'colorpicker' ),
        slider_obj = colorPicker_obj.options.slidersHorz;

      slider_obj.alpha.maxLeft = width_int;
      slider_obj.alpha.maxTop = 0;

      slider_obj.hue.maxLeft = width_int;
      slider_obj.hue.maxTop = 0;

      slider_obj.saturation.maxLeft = width_int;
      slider_obj.saturation.maxTop = 150;

      colorPicker_obj.update();
    }*/

</script>

<script>
        var colorPicker = document.querySelector('input[name="color"]');

        function prepareSection(label, colors) {
            var palette = document.getElementById(label + '-palette'),
                sample = document.getElementById(label + '-sample'),
                actual = sample.querySelector('.actual'),
                nearest = sample.querySelector('.nearest'),
                getColor = nearestColor.from(colors);

            colorPicker.addEventListener('change', function() {
                var value = colorPicker.value;

                var color = HEXToHSL(value);
                $("#span_actual_color").html(color);
                $("#color-name").val(color);

            });

            colors.forEach(function(color) {
                var span = document.createElement('SPAN');
                span.style.backgroundColor = color.source || color;
                palette.appendChild(span);
            });
        }

        prepareSection('default', nearestColor.DEFAULT_COLORS);

        function HEXToHSL(hex) {

          hex = hex.replace(/ /g,'').replace(/#/g,'').replace(/;/g,'').replace(/\]/g,'')
          if (hex.length == 3){
              hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2]
          } else if (hex.length == 4){
              hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2]+hex[3]+hex[3]
          }
            var r = parseInt(hex.substring(0,2),16),
                g = parseInt(hex.substring(2,4),16),
                b = parseInt(hex.substring(4,6),16)
          

          // Make r, g, and b fractions of 1
            r /= 255;
            g /= 255;
            b /= 255;

          // Find greatest and smallest channel values
          let cmin = Math.min(r,g,b),
              cmax = Math.max(r,g,b),
              delta = cmax - cmin,
              h = 0,
              s = 0,
              l = 0;
          
          // Calculate hue
          // No difference
          if (delta == 0)
            h = 0;
          // Red is max
          else if (cmax == r)
            h = ((g - b) / delta) % 6;
          // Green is max
          else if (cmax == g)
            h = ((b - r) / delta) + 2;
          // Blue is max
          else if (cmax == b)
            h = ((r - g) / delta) + 4;

          h0 = h;
          h = Math.round(h * 60);
          
          // Make negative hues positive behind 360°
          if (h < 0)
              h += 360;
          
          // Calculate lightness
          l = (cmax + cmin) / 2;

          // Calculate saturation
          s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
            
          // Multiply l and s by 100
          s = +(s * 100).toFixed(4);
          l = +(l * 100).toFixed(4);
          
          if (h >= 0 && h <20 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Red';
              }
          }else if (h >= 20 && h <40 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Orenge';
              }
          }else if (h >= 40 && h <80 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Yellow';
              }
          }else if (h >= 80 && h <140 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Green';
              }
          }else if (h >= 140 && h <190 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Sky Blue';
              }
          }else if (h >= 190 && h <255 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Blue';
              }
          }else if (h >= 255 && h <280 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Violet';
              }

          }else if (h >= 280 && h <315 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Perple';
              }
          }else if (h >= 280 && h <335 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Pink';
              }
          }else if (h >= 335 && h <359 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Red';
              }
          }


          return color;

        }

    </script>
  
@endsection


@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
            <div class="col-lg-12 col-md-12 col-sm-12 text-left" style="margin-bottom: 14px; padding: 0;"><a href="{{ route('add_product') }}" class="btn btn-warning">ADD PRODUCT</a> <a href="{{ route('add_deals_or_exclusive_product',['deals_of_the_day']) }}" class="btn btn-warning">DEALS OF THE DAY</a> <a href="{{ route('add_deals_or_exclusive_product',['kalakshi_exclusive_product']) }}" class="btn btn-warning">KALAKSHI EXCLUSIVE PRODUCTS</a></div>

            <div class="card">
               <div class="card-body">
                
                  @if(session('succ_msg'))
                  <div class="alert alert-info round  alert-icon-left alert-dismissible mb-2" role="alert">
                      <span class="alert-icon">
                          <i class="ft-thumbs-up"></i>
                      </span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> {{session('succ_msg')}}
                  </div>
                  @endif
                  @if(session('err_msg'))
                  <div class="alert round bg-danger alert-icon-left alert-dismissible mb-2" role="alert">
                      <span class="alert-icon">
                          <i class="ft-thumbs-down"></i>
                      </span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                      <strong>Warning!</strong> {{session('err_msg')}}
                  </div>
                  @endif

                  <div class="table-responsive">
                      <table id="producttable" class="table table-striped table-bordered" style="width:100%">
                        <thead class=" text-primary">
                          <tr style="height: 58px;text-align: center;"> 
                              <th>Sl No.</th>
                              <th>Product Code</th>
                              <th>Product Name</th>
                              <th>Product Price</th>
                              <th>Product Type</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($product_details) && !empty($product_details))
                           @foreach($product_details as $key=>$product)
                            <tr>
                              <td >{{ $key+1 }}</td>
                              <td >{{ (isset($product->product_code) && !empty($product->product_code))?$product->product_code:'' }}</td>
                              <td >{{ (isset($product->product_name) && !empty($product->product_name))?$product->product_name:'' }}</td>
                              <td >{{ (isset($product->product_price) && !empty($product->product_price))?$product->product_price:'' }}</td>
                              <td >@if(isset($product->product_type) && !empty($product->product_type)) @if($product->product_type == 1) Normal Product @elseif($product->product_type == 2) Elite Product @endif @endif </td>
                              
                              <td style="text-align: center;">
                                <?php if($product->product_status == 1) { ?>
                                <a href="change_product_status/{{ $product->id }}/{{ $product->product_status }}" onclick="return confirm('Are you sure to inactive this product?')"><i class="material-icons">check_circle</i>
                                <?php } if($product->product_status == 0 || $product->product_status == 2) { ?>
                                <a href="change_product_status/{{ $product->id }}/{{ $product->product_status }}" onclick="return confirm('Are you sure to active this product?')"><i class="material-icons">cancel</i></a>
                                <?php } ?>
                                <a href="edit_product/{{ $product->id }}"><i class="material-icons">edit_note</i></a>
                                <a href="manage_product_story/?product={{ $product->id }}"><i class="material-icons">history_edu</i></a>
                                <a href="manage_product_offer/{{ $product->id }}"><i class="material-icons">local_offer</i></a>
                                <a href="delete_product/{{ $product->id }}" onclick="return confirm('Are you sure to delete this product?')"><i class="material-icons">delete</i></a>
                              </td>
                            </tr>   
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scriptjs')
<script>
    $(document).ready(function() {
         $(document).ready(function() {
            $('#producttable').DataTable();
          } );        
    });
</script>
@endsection

@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Product Story</h4>
               </div>
               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ route('edit_product_story_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{(isset($story->id) && !empty($story->id))?$story->id:''}}">

                        <label class="label-input100" for="phone">Elite Product <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('product_id') ? 'error' : '' }}" name="product_id" id="product_id">
                              <option value="">Select Elite Product</option>
                              @if(isset($story->product_details) && !empty($story->product_details))
                              @foreach($story->product_details as $key=>$val)
                              <option value="{{$val->id}}" @if($story->product_id == $val->id) selected @endif>{{$val->product_name}}</option>
                              @endforeach
                              @endif
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        
                        <label for="description" class="label-input100">Product Story<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="description {{ $errors->has('description') ? 'error' : '' }}" name="description" id="description">{{ (isset($story->description) && !empty($story->description))?$story->description:''}}</textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('description'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('description') }}</span>
                           @endif
                        </div>
                        
                        <label style="padding-top: 30px;" for="story_image"  class="label-input100" for="message">Story Image <span style="color: red !important;">*</span></label>
                        <div class="">
                           <input type="file" class="form-control1" name="story_image" id="story_image" placeholder="Enter Product Price" multiple>
                           @if($errors->has('story_image'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('story_image') }}</span>
                           @endif
                           <span class="focus-input100"></span>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-2"> </div>
                          <div class="col-sm-8" style="text-align: left !important;">
                            @php $images = $story->story_image @endphp
                            @if(isset($images) && !empty($images))
                            
                              <img src="{{url('picture/productstory/',$images)}}" width="100" height="100" >
                            
                            @endif
                          </div>
                          <div class="col-sm-2"> </div>
                        </div>


                        
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_product_story') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn">
                           <span> Save Product Story<i class="zmdi zmdi-arrow-right m-l-8"></i></span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('scriptjs')
<script>
   $(document).ready(function () {
       tinymce.init({
           selector:'textarea.description',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });
   });
</script>
@endsection
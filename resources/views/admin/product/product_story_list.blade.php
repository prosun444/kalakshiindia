

@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
          <div class="col-lg-12 col-md-12 col-sm-12 text-left" style="margin-bottom: 14px; padding: 0;"><a href="{{ route('product_story') }}" class="btn btn-warning">ADD PRODUCT STORY</a></div>
            <div class="card">
               <div class="card-body">
                
                  @if(session('succ_msg'))
                  <div class="alert alert-info round  alert-icon-left alert-dismissible mb-2" role="alert">
                      <span class="alert-icon">
                          <i class="ft-thumbs-up"></i>
                      </span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> {{session('succ_msg')}}
                  </div>
                  @endif
                  @if(session('err_msg'))
                  <div class="alert round bg-danger alert-icon-left alert-dismissible mb-2" role="alert">
                      <span class="alert-icon">
                          <i class="ft-thumbs-down"></i>
                      </span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                      <strong>Warning!</strong> {{session('err_msg')}}
                  </div>
                  @endif

                  <div class="table-responsive">
                      <table id="producttable" class="table table-striped table-bordered" style="width:100%">
                        <thead class=" text-primary">
                          <tr style="height: 58px;text-align: center;"> 
                              <th>Sl No.</th>
                              <th>Product Code</th>
                              <th>Product Name</th>
                              <th>Product Story Image</th>
                              <th>Story Description</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($product_story) && !empty($product_story))
                           @foreach($product_story as $key=>$product)
                            <tr>
                              <td >{{ $key+1 }}</td>
                              <td >{{ (isset($product->Product->product_code) && !empty($product->Product->product_code))?$product->Product->product_code:'' }}</td>
                              <td >{{ (isset($product->Product->product_name) && !empty($product->Product->product_name))?$product->Product->product_name:'' }}</td>
                              <td ><img src="{{url('picture/productstory/',$product->story_image)}}" width="100" height="100"></td>
                              <td >@if(isset($product->description) && !empty($product->description)) @php print_r(substr($product->description,0,20).'...'); @endphp @endif </td>
                              
                              <td style="text-align: center;">
                                <?php if($product->status == 1) { ?>
                                <a href="change_product_story_status/{{ $product->id }}/{{ $product->status }}" onclick="return confirm('Are you sure to inactive this product story?')"><i class="fa fa-2x fa-check" aria-hidden="true"></i>&nbsp;&nbsp;
                                <?php } if($product->status == 0 || $product->status == 2) { ?>
                                <a href="change_product_story_status/{{ $product->id }}/{{ $product->status }}" onclick="return confirm('Are you sure to active this product story?')"><i class="fa fa-2x fa-times" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                <?php } ?>
                                <a href="edit_product_story/{{ $product->id }}"><i class="fa fa-2x fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="delete_product_story/{{ $product->id }}" onclick="return confirm('Are you sure to delete this product story?')"><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></a>
                              </td>
                            </tr>   
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scriptjs')
<script>
    $(document).ready(function() {
         $(document).ready(function() {
            $('#producttable').DataTable();
          } );        
    });
</script>
@endsection

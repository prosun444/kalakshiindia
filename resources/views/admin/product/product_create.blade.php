@extends('admin.layouts.main')

@section('csscontent')
<style type="text/css">
  
  .cp-container {
  position: relative;
  .colorpicker-bs-popover {
    transform: none!important;
    top: 100%!important;
    width: 100%;
    max-width: none;
    .colorpicker {
      width: 100%;
      .colorpicker-saturation,
      .colorpicker-hue,
      .colorpicker-alpha,
      .colorpicker-preview,
      .colorpicker-bar {
        width: 100%;
        box-shadow: none;
      }
      .colorpicker-saturation {
        height: 150px;
        .colorpicker-guide {
          height: 10px;
          width: 10px;
          border-radius: 10px;
          margin: -5px 0 0 -5px;
        }
      }
      .colorpicker-hue,
      .colorpicker-alpha,
      .colorpicker-preview,
      .colorpicker-bar {
        margin-top: 10px;
      }
      
      .colorpicker-hue,
      .colorpicker-alpha,
      .colorpicker-preview {
        height: 30px;
      }
      .colorpicker-alpha,
      .colorpicker-preview {
        background-size: 20px 20px;
        background-position: 0 0, 10px 10px;
      }
      .colorpicker-preview {
        font-size: 1rem;
        line-height: 1.75;
      }
    }
  }
  }

  .nearest {
            position: relative;
            display: inline-block;
            width: 50%;
            height: 50%;
        }

  // For testing only
  .cp-container {
    max-width: 350px;
    margin: 2rem auto;
  }
</style>

<style type="text/css">
        /*body > div {
            width: 960px;
            margin: auto;
        }

        h1 {
            font-family: sans-serif;
            border-bottom: 1px solid black;
        }

        p {
            font-size: 150%;
        }

        h3 {
            margin-bottom: 0;
        }*/

        /*.palette span {
            display: inline-block;
            height: 30px;
            width: 30px;
            margin-right: 10px;
        }*/

        /*.sample {
            height: 300px;
        }*/

        .actual,
        .nearest {
            position: relative;
            display: inline-block;
            width: 0%;
            height: 0%;
        }

        .actual:before {
            position: absolute;
            display: block;
            content: "Actual";
            top: calc(50% - 10px);
            left: 0;
            right: 0;
            line-height: 20px;
            text-align: center;
        }

        .nearest:before {
            position: absolute;
            display: block;
            content: "Nearest";
            top: calc(50% - 10px);
            left: 0;
            right: 0;
            line-height: 20px;
            text-align: center;
        }
        .colour-colorDisplay{
          width: 15px;
          height: 15px;
          border-radius: 50%;
          display: inline-block;
          margin-right: 8px;
          margin-left: 27px;
        }
        .colour-label{
          cursor: pointer;
          position: relative;
          height: 100%;
          line-height: 16px;
          vertical-align: top;
        }
    </style>
@endsection 

@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Product</h4>
               </div>

                <!-- <form>
                    <h3>Select a color</h3>
                    <input type="color" name="color" />
                </form>

                <div class="palette" id="default-palette">
                    <h3>Default colors</h3>
                </div>

                <div class="sample" id="default-sample">
                    <div class="actual"></div><div class="nearest"></div>
                </div> -->











               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ route('add_product_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <label class="label-input100" for="first-name">Product Name <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100 validate-input">
                           <input id="product_name" class="input100 {{ $errors->has('product_name') ? 'error' : '' }}" type="text" name="product_name" id="product_name" placeholder="Enter Product Name" value="{{ old('product_name') }}">
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="email">Product Code <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100 validate-input">
                           <input class="input100 {{ $errors->has('product_code') ? 'error' : '' }}" type="text" id="product_code" name="product_code" placeholder="Enter Blog Slug" readonly value="KC{{ $product_category->randval }}" readonly>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Product Category <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('product_category') ? 'error' : '' }}" name="product_category" id="product_category">
                              <option value="">Select Category</option>
                              @if(isset($product_category) && !empty($product_category))
                              @foreach($product_category as $key=>$val)
                              <option value="{{$val->product_category_id}}">{{$val->product_category_name}}</option>
                              @endforeach
                              @endif
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100 product_subcat_div" for="phone" style="display: none;">Product Subcategory <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100 product_subcat_div" style="display: none;">
                          <select class="browser-default " name="product_subcategory" id="product_subcategory">
                              <option value="">Product Subcategory</option>
                          </select>
                          <span class="focus-input100"></span>
                        </div>
                        <label class="label-input100" for="phone">Product Type <span style="color: red !important;">*</span></label>
                        <div class="" style="margin-bottom: 25px;">
                           <label class="radio-inline" style="display: inline;">
                           <input type="radio" name="product_type" value="1" checked> Normal Product</label>
                           <label class="radio-inline" style="display: inline;">
                           <input type="radio" name="product_type" value="2"> Elite Product</label>
                           <span class="focus-input100"></span>
                        </div>
                        <label for="product_price" class="label-input100">Product Price<span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <input type="text" class="input100 {{ $errors->has('product_price') ? 'error' : '' }}" name="product_price" id="product_price" placeholder="Enter Product Price" value="{{ old('product_price') }}">
                           <span class="focus-input100"></span>
                        </div>
                        <!-- <div class="sample" id="default-sample" style="display: none;">
                            <div class="actual"></div><div class="nearest"></div>
                        </div> -->
                        <label for="product_colour" class="label-input100">Product Colour<span style="color: red !important;">*</span></label>

                        <div class="wrap-input100">
                          <SELECT class="form-control1" name="product_colour" id="product_colour">
                            <option value="">Select</option>
                            @if(isset($product_category->ProductColour) && !empty($product_category->ProductColour))
                            @foreach($product_category->ProductColour as $key=>$colour)
                            <option style="@if($key+1 == count($product_category->ProductColour))background: {{$colour->colour_code}}; @else background-color: {{$colour->colour_code}}; @endif" value="{{$colour->colour_slug}}">{{$colour->colour}}</option>
                            @endforeach
                            @endif
                          </SELECT>
                          @if($errors->has('product_colour'))
                            <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('product_colour') }}</span>
                          @endif
                        </div>
                          <!-- <div class="cp-container wrap-input100" id="cp3-container">
                            <div class="input-group" title="Using input value">
                              <input id="cp3" type="text" name="product_colour" class="input100 {{ $errors->has('product_colour') ? 'error' : '' }}" value="#ff9900" autocomplete="off"/>
                            </div>
                          </div>class="colour-label colour-colorDisplay" -->

                        <!-- <div class="colorpick" style=" margin-bottom: 15px;">
                            <input type="color" name="color" style="width: 50px;height: 35px;" />
                            <span id="span_actual_color"></span>

                            <input id="color-name" type="hidden" name="color-name" />
                            
                            <div class="sample" id="default-sample">
                                <div style="display: none;" class="actual"></div><div style="display: none;" class="nearest"></div>
                            </div>
                        </div> -->
                        <label for="description" class="label-input100">Description<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="description {{ $errors->has('description') ? 'error' : '' }}" name="description" id="description"></textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('description'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('description') }}</span>
                           @endif
                        </div>
                        <label style="padding-top: 30px;" for="usages" class="label-input100">Usages<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="usages {{ $errors->has('usages') ? 'error' : '' }}" name="usages" id="usages"></textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('usages'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('usages') }}</span>
                           @endif
                        </div>
                        <label style="padding-top: 30px;" for="special_tips" class="label-input100">Kalakshi Special tips<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="special_tips {{ $errors->has('special_tips') ? 'error' : '' }}" name="special_tips" id="special_tips"></textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('special_tips'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('special_tips') }}</span>
                           @endif
                        </div>
                        <label style="padding-top: 30px;" for="product_images"  class="label-input100" for="message">Product Images <span style="color: red !important;">*</span></label>
                        <div class="">
                           <input type="file" class="form-control1" name="product_images[]" id="product_images" placeholder="Enter Product Price" multiple>
                           @if($errors->has('product_images'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('product_images') }}</span>
                           @endif
                           <span class="focus-input100"></span>
                        </div>
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_products') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn" type="submit">
                           <span> Save Product<i class="zmdi zmdi-arrow-right m-l-8"></i></span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('scriptjs')
<script>
   $(document).ready(function () {
       tinymce.init({
           selector:'textarea.description',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });

       tinymce.init({
           selector:'textarea.usages',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11alertycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });

       tinymce.init({
           selector:'textarea.special_tips',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });
   });

   // For testing purposes only
    /*console.clear();

    init_colorpicker_fn( '#cp2', 'rgb' );

    init_colorpicker_fn( '#cp3', 'hex' );

    function init_colorpicker_fn( id_str, format_str = 'hex' ) {
      
      if ( !id_str.startsWith( '#' ) ) {
        id_str = '#' + id_str;
      }

      var $picker_el = jQuery( id_str );

      $picker_el.colorpicker( {
        format: format_str,
        horizontal: true,
        popover: {
          container: id_str + '-container'
        },
        template: '<div class="colorpicker">' +
          '<div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>' +
          '<div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>' +
          '<div class="colorpicker-alpha">' +
          ' <div class="colorpicker-alpha-color"></div>' +
          ' <i class="colorpicker-guide"></i>' +
          '</div>' +
          '<div class="colorpicker-bar">' +
          ' <div class="input-group">' +
          '   <input class="form-control input-block color-io" />' +
          ' </div>' +
          '</div>' +
          '</div>'
      } ).on( 'colorpickerCreate colorpickerUpdate', function( e ) {
        $picker_el.parent().find( '.colorpicker-input-addon>i' ).css( 'background-color', e.value );
      } ).on( 'colorpickerCreate', function( e ) {
        resize_color_picker_fn( $picker_el );
      } ).on( 'colorpickerShow', function( e ) {
        var cpInput_el = e.colorpicker.popupHandler.popoverTip.find( '.color-io' );

        cpInput_el.val( e.color.string() );

        cpInput_el.on( 'change keyup', function() {
          e.colorpicker.setValue( cpInput_el.val() );
        } );
      } ).on( 'colorpickerHide', function( e ) {
        var cpInput_el = e.colorpicker.popupHandler.popoverTip.find( '.color-io' );
        cpInput_el.off( 'change keyup' );
      } ).on( 'colorpickerChange', function( e ) {

        //
        var cpInput_el = e.colorpicker.popupHandler.popoverTip.find( '.color-io' );

        if ( e.value === cpInput_el.val() || !e.color || !e.color.isValid() ) {
          return;
        }

        cpInput_el.val( e.color.string() );
      } );

      $picker_el.parent().find( '.colorpicker-input-addon>i' ).on( 'click', function( e ) {
        $picker_el.colorpicker( 'colorpicker' ).show();
      } );

      jQuery( window ).resize( function( e ) {
        resize_color_picker_fn( $picker_el );
      } );
    }

    var colorPicker = document.querySelector('input[name="product_colour"]');

    function prepareSection(label, colors) {
            var palette = document.getElementById(label + '-palette'),
                sample = document.getElementById(label + '-sample'),
                actual = sample.querySelector('.actual'),
                nearest = sample.querySelector('.nearest'),
                getColor = nearestColor.from(colors);

            colorPicker.addEventListener('change', function() {
                var value = colorPicker.value;

                actual.style.backgroundColor = value;
                nearest.style.backgroundColor = getColor(value);

                alert( nearest.style.backgroundColor)
            });

    }

    prepareSection('default', nearestColor.DEFAULT_COLORS);

    function resize_color_picker_fn( $picker_el ) {
      var rem_int = parseInt( getComputedStyle( document.documentElement ).fontSize ),
        width_int = $picker_el.parent().width() - ( ( rem_int * .75 ) * 2 ) - 2,
        colorPicker_obj = $picker_el.colorpicker( 'colorpicker' ),
        slider_obj = colorPicker_obj.options.slidersHorz;

      slider_obj.alpha.maxLeft = width_int;
      slider_obj.alpha.maxTop = 0;

      slider_obj.hue.maxLeft = width_int;
      slider_obj.hue.maxTop = 0;

      slider_obj.saturation.maxLeft = width_int;
      slider_obj.saturation.maxTop = 150;

      colorPicker_obj.update();
    }*/
</script>

<script>
        var colorPicker = document.querySelector('input[name="color"]');

        function prepareSection(label, colors) {
            var palette = document.getElementById(label + '-palette'),
                sample = document.getElementById(label + '-sample'),
                actual = sample.querySelector('.actual'),
                nearest = sample.querySelector('.nearest'),
                getColor = nearestColor.from(colors);

            colorPicker.addEventListener('change', function() {
                var value = colorPicker.value;

                var color = HEXToHSL(value);
                $("#span_actual_color").html(color);
                $("#color-name").val(color);

            });

            colors.forEach(function(color) {
                var span = document.createElement('SPAN');
                span.style.backgroundColor = color.source || color;
                palette.appendChild(span);
            });
        }

        prepareSection('default', nearestColor.DEFAULT_COLORS);

        function HEXToHSL(hex) {

          hex = hex.replace(/ /g,'').replace(/#/g,'').replace(/;/g,'').replace(/\]/g,'')
          if (hex.length == 3){
              hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2]
          } else if (hex.length == 4){
              hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2]+hex[3]+hex[3]
          }
            var r = parseInt(hex.substring(0,2),16),
                g = parseInt(hex.substring(2,4),16),
                b = parseInt(hex.substring(4,6),16)
          

          // Make r, g, and b fractions of 1
            r /= 255;
            g /= 255;
            b /= 255;

          // Find greatest and smallest channel values
          let cmin = Math.min(r,g,b),
              cmax = Math.max(r,g,b),
              delta = cmax - cmin,
              h = 0,
              s = 0,
              l = 0;
          
          // Calculate hue
          // No difference
          if (delta == 0)
            h = 0;
          // Red is max
          else if (cmax == r)
            h = ((g - b) / delta) % 6;
          // Green is max
          else if (cmax == g)
            h = ((b - r) / delta) + 2;
          // Blue is max
          else if (cmax == b)
            h = ((r - g) / delta) + 4;

          h0 = h;
          h = Math.round(h * 60);
          
          // Make negative hues positive behind 360°
          if (h < 0)
              h += 360;
          
          // Calculate lightness
          l = (cmax + cmin) / 2;

          // Calculate saturation
          s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
            
          // Multiply l and s by 100
          s = +(s * 100).toFixed(4);
          l = +(l * 100).toFixed(4);
          
          if (h >= 0 && h <20 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Red';
              }
          }else if (h >= 20 && h <40 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Orenge';
              }
          }else if (h >= 40 && h <80 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Yellow';
              }
          }else if (h >= 80 && h <140 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Green';
              }
          }else if (h >= 140 && h <190 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Sky Blue';
              }
          }else if (h >= 190 && h <255 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Blue';
              }
          }else if (h >= 255 && h <280 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Violet';
              }

          }else if (h >= 280 && h <315 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Perple';
              }
          }else if (h >= 280 && h <335 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Pink';
              }
          }else if (h >= 335 && h <359 ) {
              if (s >= 0 && s <= 100 && l >= 0 && l <= 29 ) {
                color = 'Black';
              }else if(s >= 0 && s <= 100 && l >= 30 && l <= 39 ){
                color = 'Light Black';
              }else if(s >= 0 && s <= 4 && l >= 96 && l <= 100 ){
                color = 'White';
              }else if(s >= 0 && s <= 4 && l >= 85 && l <= 95 ){
                color = 'Off White';
              }else if(s >= 0 && s <= 4 && l >= 40 && l <= 95 ){
                color = 'Gray';
              }else{
                color = 'Red';
              }
          }


          return color;

        }

    </script>
@endsection
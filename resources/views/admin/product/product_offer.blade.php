@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Product Offer</h4>
               </div>
               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ route('product_offer_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="productid" id="productid" value="{{ $productid }}">
                        <div></div>
                        <label class="label-input100" for="phone">Offer Percentage </label>
                        <div class="wrap-input100">
                           <input type="text" class="input100 {{ $errors->has('percentage') ? 'error' : '' }}" name="percentage" id="percentage" placeholder="Enter Offer Percentage" value="{{ (isset($offer->percentage))? $offer->percentage : '' }}">
                           <span class="focus-input100"></span>
                        </div>

                        <label class="label-input100" for="phone">Offer Status </label>
                        <div class="wrap-input100">
                           <SELECT class="browser-default" name="offer_status" id="offer_status" >
                              <option value="1" {{ (isset($offer->deleted_at) && !empty($offer->deleted_at))? '' : 'selected="selected"' }}>Active</option>
                              <option value="2" {{ (isset($offer->deleted_at) && !empty($offer->deleted_at))? 'selected="selected"' : '' }}>Inactive</option>
                           </SELECT>
                           <span class="focus-input100"></span>
                        </div>
                        
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn">
                           <span> Save Product Offer<i class="zmdi zmdi-arrow-right m-l-8"></i></span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('scriptjs')
<script>
   $(document).ready(function () {
       
   });
</script>
@endsection
<!DOCTYPE HTML>
<html>
<head>
<title>Kalakshi Creations Admin | Manage Products</title>

@include('admin/admin_header')

        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
  	 <h3>Manage Products</h3>
	 <div class="col-lg-12 col-md-12 col-sm-12 text-left">{!! $product_details->links() !!}</div>
	 <div class="col-lg-12 col-md-12 col-sm-12 text-right"><a href="{{ url('admin_manage7081/add_blog') }}" class="btn btn-warning">ADD PRODUCT</a></div>
	 <br/>
	 <div class="col-lg-12 col-md-12 col-sm-12 text-center">
	 @if (Session::has('status'))
						<br/>
						<div class="alert alert-info alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{!! session('status') !!}
						</div>
						@endif
						<br/>
	</div>					
	
  	<div class="bs-example4" data-example-id="contextual-table" style="margin-top: 50px !important;">	
    <table class="table">
      <thead>
        <tr>
          <th>Product Name</th>
		  <th>Product Type</th>
		  <th>Product Category</th>
		  <th>Product Price</th>
          <th>Action</th>
        </tr>
      </thead>
	  @foreach($product_details as $product)
      <tbody>
        <tr class="active" style="color: #000000 !important;">
          <td style="color: #000000 !important;">{{ $product->blog_id }}</td>
		  <td style="color: #000000 !important;">{{ $product->blog_id }}</td>
          <td style="color: #000000 !important;">{{ $product->blog_title }}</td>
		  <td style="color: #000000 !important;">{{ $product->blog_added_on }}</td>
		  <td style="color: #000000 !important;">
				<?php if($product->product_status == 1) { ?>
				<a href="change_product_status/{{ $product->product_id }}/{{ $product->product_status }}" onclick="return confirm('Are you sure to inactive this product?')"><img src="{{ asset('public/images/tick.png') }}"></a>&nbsp;&nbsp;
				<?php } if($product->product_status == 2) { ?>
				<a href="change_product_status/{{ $product->product_id }}/{{ $product->product_status }}" onclick="return confirm('Are you sure to active this product?')"><img src="{{ asset('public/images/2301.png') }}"></a>&nbsp;&nbsp;
				<?php } ?>
				<a href="edit_product/{{ $product->product_id }}"><img src="{{ asset('public/images/2303.png') }}"></a>
				<a href="delete_product/{{ $product->product_id }}" onclick="return confirm('Are you sure to delete this product?')"><img src="{{ asset('public/images/2302.png') }}"></a>
		  </td>
        </tr>        
      </tbody>
	  @endforeach
    </table>
   </div>
	
  </div>

 @include('admin/admin_footer')

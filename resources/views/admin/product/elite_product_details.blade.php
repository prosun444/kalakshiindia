@extends('admin.layouts.main')

@section('content')

    <h3>Manage Elite Product Details</h3>
    <div class="col-lg-12 col-sm-12 col-md-12 text-right">

        <div class="tab-content">
          <form class="form-horizontal" action="{{ url('admin_manage7081/elite_product_process') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="tab-pane active" id="horizontal-form">
              <div class="form-group">
                <label for="product_id" class="col-sm-2 control-label">Elite Product<span style="color: red !important;">*</span></label>
                <div class="col-sm-8">
                  <SELECT class="form-control1" name="product_id" id="product_id">
                    <option>Select</option>
                    @if(isset($product_details) && !empty($product_details))
                    @foreach($product_details as $key=>$val)
                    <option value="{{$val->id}}">{{$val->product_name}}</option>
                    @endforeach
                    @endif
                  </SELECT>
                  @if($errors->has('product_id'))
                    <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('product_id') }}</span>
                  @endif
                </div>
                <div class="col-sm-2"> </div>
              </div>

              <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Product Description<span style="color: red !important;">*</span></label>
                  <div class="col-sm-8">
                    <textarea class="form-control" name="description" id="description"></textarea>
                    @if($errors->has('description'))
                      <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('description') }}</span>
                    @endif
                  </div>
                  <script type="text/javascript">
                      CKEDITOR.replace('description');
                  </script>
                  <div class="col-sm-2">
                    
                  </div>
                </div>              
                <div class="form-group">
                  <label for="service_slug" class="col-sm-2 control-label">Product Image<span style="color: red !important;">*</span></label>
                  <div class="col-sm-8">
                    <input type="file" name="elite_product_image[]" id="elite_product_image">
                    @if($errors->has('elite_product_image'))
                      <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('elite_product_image') }}</span>
                    @endif
                  </div>
                  <div class="col-sm-2">
                    
                  </div>
                </div>
              
            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-left">
                  <button class="btn-success btn">Save Product</button>
                </div>
              </div>
            </div>
          </form>
        </div>

    </div>

@endsection              
@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
            <div class="card">
               <div class="card-body">
                  <!-- <div class="col-lg-12 col-sm-12 col-md-12 text-right"> -->
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ route('add_product_as_exclusive') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="page_type" id="page_type" value="{{$page_type}}">
                        <input type="hidden" name="page_id" id="page_id" value="{{$page_id}}">
                        <label class="label-input100" for="first-name">Add {{$page_type}} <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('product_name') ? 'error' : '' }}" name="product_name" id="product_name">
                              <option value="">Select Category</option>
                              @if(isset($products) && !empty($products))
                              @foreach($products as $key=>$val)
                              <option value="{{$val['id']}}">{{$val['product_name']}}</option>
                              @endforeach
                              @endif
                           </select>
                           <span class="focus-input100"></span>
                        </div>

                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_products') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn" type="submit">
                           <span> Add Product<i class="zmdi zmdi-arrow-right m-l-8"></i></span>
                           </button>
                        </div>


                      </form>
                    </div>
                  <!-- </div> -->
               </div>
             </div>

            <div class="card">
               <div class="card-body">
                
                  @if(session('succ_msg'))
                  <div class="alert alert-info round  alert-icon-left alert-dismissible mb-2" role="alert">
                      <span class="alert-icon">
                          <i class="ft-thumbs-up"></i>
                      </span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> {{session('succ_msg')}}
                  </div>
                  @endif
                  @if(session('err_msg'))
                  <div class="alert round bg-danger alert-icon-left alert-dismissible mb-2" role="alert">
                      <span class="alert-icon">
                          <i class="ft-thumbs-down"></i>
                      </span>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                      <strong>Warning!</strong> {{session('err_msg')}}
                  </div>
                  @endif

                  <div class="table-responsive">
                      <table id="producttable" class="table table-striped table-bordered" style="width:100%">
                        <thead class=" text-primary">
                          <tr style="height: 58px;text-align: center;"> 
                              <th>Sl No.</th>
                              <th>Product Code</th>
                              <th>Product Name</th>
                              <th>Product Price</th>
                              <th>Product Type</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                          
                           @if(isset($product_details) && !empty($product_details))
                           @foreach($product_details as $key=>$product)
                            <tr>
                              <td >{{ $key+1 }}</td>
                              <td >{{ (isset($product->product_code) && !empty($product->product_code))?$product->product_code:'' }}</td>
                              <td >{{ (isset($product->product_name) && !empty($product->product_name))?$product->product_name:'' }}</td>
                              <td >{{ (isset($product->product_price) && !empty($product->product_price))?$product->product_price:'' }}</td>
                              <td >@if(isset($product->product_type) && !empty($product->product_type)) @if($product->product_type == 1) Normal Product @elseif($product->product_type == 2) Elite Product @endif @endif </td>
                              
                              <td style="text-align: center;">
                                <a href="delete_product_from_exclusive/{{ $product->id }}/{{ $page_id }}" onclick="return confirm('Are you sure to removed this product from this list?')"><i class="material-icons">delete</i></a>
                              </td>
                            </tr>   
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scriptjs')
<script>
    $(document).ready(function() {
         $(document).ready(function() {
            $('#producttable').DataTable();
          } );        
    });
</script>
@endsection

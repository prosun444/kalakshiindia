@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-10">
            <div class="card">
               <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Product Story</h4>
               </div>
               <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                  <div class="wrap-contact100" style="text-align: left;">
                     <form  action="{{ route('product_story_process') }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <label class="label-input100" for="phone">Elite Product <span style="color: red !important;">*</span></label>
                        <div class="wrap-input100">
                           <select class="browser-default {{ $errors->has('product_id') ? 'error' : '' }}" name="product_id" id="product_id">
                              <option value="">Select Elite Product</option>
                              @if(isset($product_details) && !empty($product_details))
                              @foreach($product_details as $key=>$val)
                              <option value="{{$val->id}}">{{$val->product_name}}</option>
                              @endforeach
                              @endif
                           </select>
                           <span class="focus-input100"></span>
                        </div>
                        
                        <label for="description" class="label-input100">Product Story<span style="color: red !important;">*</span></label>
                        <div class="">
                           <textarea class="description {{ $errors->has('description') ? 'error' : '' }}" name="description" id="description"></textarea>
                           <span class="focus-input100"></span>
                           @if($errors->has('description'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('description') }}</span>
                           @endif
                        </div>
                        
                        <label style="padding-top: 30px;" for="story_image"  class="label-input100" for="message">Story Image <span style="color: red !important;">*</span></label>
                        <div class="">
                           <input type="file" class="form-control1" name="story_image" id="story_image" placeholder="Enter Product Price" multiple>
                           @if($errors->has('story_image'))
                           <span class="help-block" style="color: red !important;text-align: left !important;">{{ $errors->first('story_image') }}</span>
                           @endif
                           <span class="focus-input100"></span>
                        </div>
                        <div class="container-contact100-form-btn" style="justify-content: left;margin: ;margin-top: 25px;">
                           <a href="{{ url('admin_manage7081/manage_product_story') }}" class="btn btn-primary">Back</a>
                           <button class="btn-success btn">
                           <span> Save Product Story<i class="zmdi zmdi-arrow-right m-l-8"></i></span>
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection             
@section('scriptjs')
<script>
   $(document).ready(function () {
       tinymce.init({
           selector:'textarea.description',
           width: 900,
           height: 200,
           plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
           toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
       });
   });
</script>
@endsection
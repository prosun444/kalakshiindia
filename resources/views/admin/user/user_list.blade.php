

@extends('admin.layouts.main')
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="table-responsive">
                      <table id="usertable" class="table table-striped table-bordered" style="width:100%">
                        <thead class=" text-primary">
                          <tr style="height: 58px;text-align: center;"> 
                             <th>Sl No.</th>
                             <th>User Name</th>
                             <th>User Email</th>
                             <th>User Mobile</th>
                             <th>User Organisation Type</th>
                             <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($user_details) && !empty($user_details))
                           @foreach($user_details as $key=>$user)
                           <tr >
                              <td >{{ $key+1 }}</td>
                              <td >{{ (isset($user->name) && !empty($user->name))?$user->name:'' }}</td>
                              <td >{{ (isset($user->email) && !empty($user->email))?$user->email:'' }}</td>
                              <td >{{ (isset($user->number) && !empty($user->number))?$user->number:'' }}</td>
                              <td >{{(isset($user->organizationtype) && !empty($user->organizationtype))?$user->organizationtype:''}}  </td>
                              <td style="text-align: center;">
                                 <?php if($user->status == 1) { ?>
                                 <a href="{{ route('change_user_status',['id'=>$user->id , 'status' => $user->status]) }}" onclick="return confirm('Are you sure to inactive this user?')"><i class="fa fa-2x fa-thumbs-o-up" aria-hidden="true"></i></a>
                                 <?php } if($user->status == 0 ) { ?>
                                 <a href="{{ route('change_user_status',['id'=>$user->id , 'status' => $user->status]) }}" onclick="return confirm('Are you sure to active this user?')"><i class="fa fa-2x fa-thumbs-o-down" aria-hidden="true"></i></a>
                                 <?php } ?>
                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('scriptjs')
<script>
    $(document).ready(function() {
        $('#usertable').DataTable();     
    });
</script>
@endsection

<!DOCTYPE HTML>
<html>
   <head>
      <title>Kalakshi Creations Admin | Add Product Category</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
         Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
      <div class="main">
         <div class="container">
            <center>
               @yield('content')
            </center>
         </div>
      </div>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
      <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
      @yield('scriptjs')
      <script>
         $(document).ready(function () {
           if ($(".chosen").length > 0) {
               $(".chosen").chosen();
           }
           
           var base_url = window.location.origin;
           $('#product_category_name').bind('keyup keypress blur', function () {
               var myStr = $(this).val();
               myStr = myStr.toLowerCase();
               myStr = myStr.replace(/ /g, "-");
               myStr = myStr.replace(/[^a-zA-Z0-9\.]+/g, "-");
               myStr = myStr.replace(/\.+/g, "-");
               $('#product_category_slug').val(myStr);
           });
         
           $("#product_parent_category" ).bind('keyup keypress blur', function () {
               var myStr = $(this).val();
               myStr = myStr.toLowerCase();
               myStr = myStr.replace(/ /g, "-");
               myStr = myStr.replace(/[^a-zA-Z0-9\.]+/g, "-");
               myStr = myStr.replace(/\.+/g, "-");
               $('#product_parent_category').val(myStr);
           });
         });             
      </script>
      </body>
</html>
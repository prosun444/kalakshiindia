<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      
      <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
          <img src="{{ asset('images/adminlogo.png') }}" alt="" style="width: 210px;"/>
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item  {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}  ">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item {{ (request()->routeIs('manage_user')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('manage_user') }}">
              <i class="material-icons">person</i>
              <p>User Profile</p>
            </a>
          </li>
          <li class="nav-item {{ (request()->routeIs('admin.blogmanage') || request()->routeIs('addblogmanage') || request()->routeIs('editblogmanage')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.blogmanage') }}">
              <i class="material-icons">content_paste</i>
              <p>Blog Management</p>
            </a>
          </li>
          <li class="nav-item {{ (request()->routeIs('manage_product_category') || request()->routeIs('add_product_category') || request()->routeIs('edit_product_category')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('manage_product_category') }}">
              <i class="material-icons">library_books</i>
              <p>Category</p>
            </a>
          </li>
          <li class="nav-item {{ (request()->routeIs('manage_products') || request()->routeIs('add_product') || request()->routeIs('edit_product') || request()->routeIs('add_deals_or_exclusive_product') ) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('manage_products') }}">
              <i class="material-icons">bubble_chart</i>
              <p>Product Management</p>
            </a>
          </li>

          <li class="nav-item {{ (request()->routeIs('manage_product_story') || request()->routeIs('product_story') || request()->routeIs('edit_product_story')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('manage_product_story') }}">
              <i class="material-icons">history_edu</i>
              <p>Product Story</p>
            </a>
          </li>
        
        </ul>
      </div>
</div>
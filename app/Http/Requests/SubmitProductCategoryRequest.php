<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitProductCategoryRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'product_category_name' => 'required',
            'product_category_slug' => 'required',
            'product_parent_category' => 'required'
        ];
    }
	
	 public function messages() {
        return [
            "product_category_name.required" => 'Please Enter Product Category Name',
            "product_category_slug.required" => 'Please Enter Product Category Slug',
			"product_parent_category.required" => 'Please Enter Product Parent Category'
        ];
    }

}

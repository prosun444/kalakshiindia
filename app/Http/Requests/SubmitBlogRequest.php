<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitBlogRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'blog_title' => 'required',
            'blog_slug' => 'required',
			'blog_category' => 'required',
            'blog_description' => 'required',
			'blog_img' => 'required|file|mimes:jpg,jpeg,gif,png'
        ];
    }
	
	 public function messages() {
        return [
            "blog_title.required" => 'Please Enter Blog Title',
            "blog_slug.required" => 'Please Enter Blog Slug',
			"blog_category.required" => 'Please Choose Blog Category',
			"blog_description.required" => 'Please Enter Blog Description',
            "blog_img.required" => 'Please Choose jpg,jpeg,gif,png format only'
        ];
    }

}

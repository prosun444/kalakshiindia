<?php

namespace App\Http\Middleware;

use Closure;

class Logincheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('admin_id')) {
            return $next($request);
        } else {
            return redirect('/admin_manage7081')->with('loginError', 'You need to logged in first.');
        }
    }
}

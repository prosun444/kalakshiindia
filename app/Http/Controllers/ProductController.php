<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product as Product;
use App\Models\admin\ProductStory;
use App\Models\admin\ProductCategoryModel as ProductCategory;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *http://127.0.0.1:8000/products
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   

        //Category
        $category = array();
        if (isset($_GET['cat']) && !empty($_GET['cat'])) {
            $category = explode(",",$_GET['cat']);
        }

        //Page
        $page = 1;
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $page = intval($_GET['page']);
        }

        $start = $end = 0;

        $start = 15 * ($page -1);
        $end = 15 * ($page);    


        //Price Range

        //max(`id`)

        $min = Product::min('product_price');
        $max = Product::max('product_price');


        if (isset($_GET['sp']) && !empty($_GET['sp']) ) {
            # lowcost...
           $lowrange = $_GET['sp'];
        }else {
           $lowrange = $min;
        } 

        if (isset($_GET['ep']) && !empty($_GET['ep']) ) {
            # highcost...
           $highrange = $_GET['ep'];
        }else {
           $highrange = $max;
        }      

        //color
        if (isset($_GET['color']) && !empty($_GET['color']) ) {
            $color = $_GET['color'];
        }

        $productslist = [];
        $productcategory = [];
        $productrange = [];
        $totalproduct = $totalpage = 0;
        
        $productslist = Product::skip($start)->take($end)->with('offer');

        //Price 
        if (isset($_GET['sp']) && !empty($_GET['sp']) && isset($_GET['ep']) && !empty($_GET['ep'])) {
            $productslist = $productslist->whereBetween('product_price', [$_GET['sp'], $_GET['ep']]);
        }

        //Category
        if (isset($_GET['cat']) && !empty($_GET['cat'])) {
            $productslist = $productslist->whereIn('product_subcategory',$category);
        }

        //Color
        if (isset($color) && !empty($color)) {
            $productslist = $productslist->where('product_colour_name',$color);
        }

        //orderBy
        if(isset($_GET['ob']) && !empty($_GET['ob']) && $_GET['ob'] != 1){
            $productslist = $productslist->orderBy('product_price',$_GET['ob']);
        }

        $productslist = $productslist->get();

        //product count
        $totalproduct = $productslist->count();
        $totalpage = ceil($totalproduct/15);
                        
        $productcategory = ProductCategory::where(['product_parent_category' => 0])->get()->toArray();
        $productrange = DB::table('product_details')->selectRaw('max(product_price) as highrange , min(product_price) as lowrange')->get()->toArray();
        
        //dd($productrange);
        

        if (isset($productcategory) && !empty($productcategory) ) {
            foreach ($productcategory as $key => $category) {
                $productsubcategory = [];
                $productsubcategory = ProductCategory::where(['product_parent_category' => $category['product_category_id']])->get()->toArray();
                $productcategory[$key]['subcategory'] = $productsubcategory;

            }
        }


        $categoryall = $this->categorymodel();


        return view('front.productlist' , ['products' => $productslist , 'totalproducts' => $totalproduct , 'totalpage' => $totalpage , 'productcategory' => $productcategory , 'lowrange' => $lowrange , 'highrange' => $highrange ,'min' => $min , 'max' => $max , 'pcategory' => $categoryall]);
    }

    public function categorymodel ()
    {   
        $category = array();
        $mastercategory = ProductCategory::where("product_parent_category", 0)->with("subCategory")->get()->toArray();
        return $mastercategory;
    }

    public function productsdetails($slug)
    {   
        $productsdetails = [];
        $productsdetails = Product::where(['product_slug' => $slug])->with('offer')->get();
        $categoryall = $this->categorymodel();
        $similerproducts = $this->similerproducts($slug);

        //print_r($productsdetails[0]->offer['percentage']);die;
        if(isset($productsdetails[0]->product_type) && $productsdetails[0]->product_type == 1){

            return view('front.product' , ['productsdetails' => $productsdetails , 'pcategory' => $categoryall , 'similerproducts' => $similerproducts ]);

        }elseif (isset($productsdetails[0]->product_type) && $productsdetails[0]->product_type == 2) {

            $productsstory = ProductStory::where(['product_id' => $productsdetails[0]->id])->get();

            return view('front.productelite' , ['productsdetails' => $productsdetails , 'productStory' => $productsstory , 'pcategory' => $categoryall , 'similerproducts' => $similerproducts ]);
            
        }

    }

    public function similerproducts($slug)
    {   
        $similerproducts = [];
        $category = [];
        $productsdetails = Product::where(['product_slug' => $slug])->with('offer')->first();

        $category[] = $productsdetails->product_subcategory;

        if(isset($productsdetails->product_subcategory) && !empty($productsdetails->product_subcategory)){ 
            
            $similerproducts = Product::skip(0)->take(4)->with('offer')->whereIn('product_subcategory', $category)->orderBy('id', 'DESC')->get();

        }

        return $similerproducts;

    }

    
}

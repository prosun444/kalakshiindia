<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Productorder;
use App\Productorderitem;
use App\Product;
use App\ShippingCharge;
use Auth;
use Illuminate\Support\Facades\Crypt;
use App\Models\admin\ProductCategoryModel as ProductCategory;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *http://127.0.0.1:8000/carts
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $userDetails = Auth::guard('front')->user();
        $userId = $userDetails['id'];
        $orderlist = array();
        $orderlist = Productorder::with('productOrderItem')->select('id','order_id','total_amount','shipping_charge','shipping_type')->where(['user_id'=>$userId,'status'=>'pending'])->first()->toArray();
        $orderlist['shipping_details'] = ShippingCharge::get()->toArray();

        $categorylist = $this->categorymodel();

		return view('front.cart' , ['orders' => $orderlist , 'pcategory' => $categorylist]);
    }

    public function categorymodel ()
    {   
        $category = array();
        $mastercategory = ProductCategory::where("product_parent_category", 0)->with("subCategory")->get()->toArray();
        return $mastercategory;
    }
    

    public function carts(Request $request){
        //print_r($request['cartlist']);die;
        if($request['cartlist'] == 1){
            $userDetails = Auth::guard('front')->user();
            $userId = $userDetails['id'];
            $orderlist = array();
            $orderlist = Productorder::with('productOrderItem')->select('id','order_id','total_amount','shipping_charge','shipping_type')->where(['user_id'=>$userId,'status'=>'pending'])->first()->toArray();
            $orderlist['shipping_details'] = ShippingCharge::get()->toArray();
        }

        echo json_encode($orderlist);
        exit(0);

    }

    public function addToCart(Request $request)
    {   
    	$userDetails = Auth::guard('front')->user();
        if(isset($userDetails) && !empty($userDetails)){
            $order_details = Productorder::select('id','order_id','total_amount')->where(['user_id'=>$userDetails['id'],'status'=>'pending'])->get();
            //print_r($order_details);die;
            $insert_data = array();
            if(isset($order_details) && !empty($order_details) && count($order_details)){
            	//update
            	$order_id = $order_details[0]->order_id;
            	$order = Productorder::find($order_details[0]->id);
            	$order->total_amount = ($order_details[0]->total_amount + $request->product_price);
            	$order->updated_at = Carbon::now()->toDateTimeString();
            	$response = $order->save();
            	
            }else{
            	//insert
            	$insert_data['order_id'] = $order_id = $this->generateUniqueNumber();
    	        $insert_data['user_id'] = $userDetails['id'];
    	        $insert_data['tax_percentage'] = 0;
    	        $insert_data['total_amount'] = $request->product_price;
    	        $insert_data['status'] = 'pending';
                $insert_data['shipping_charge'] = '0';
                $insert_data['shipping_type'] = '';
    	        $insert_data['created_at'] = Carbon::now()->toDateTimeString();
    	        $response = Productorder::insertGetId($insert_data);
            	
            }
    		//print_r($response); die("00000");

            if(!empty($response)){
                $oder_item_id = Productorderitem::select('id','quantity')->where(['product_id'=>$request->product_id,'order_id'=>$order_id])->get();

                if(isset($oder_item_id) && !empty($oder_item_id) && count($oder_item_id)){
                	$item = Productorderitem::find($oder_item_id[0]->id);
                	$item->quantity = ($oder_item_id[0]->quantity + 1);
                	$item->updated_at = Carbon::now()->toDateTimeString();
                	$result = $item->save();
                }else{
                	$insert_data['order_id'] = $order_id;
    		        $insert_data['product_id'] = $request->product_id;
    		        $insert_data['amount'] = $request->product_price;
    		        $insert_data['discount'] = $request->product_offer?$request->product_offer:0;
    		        $insert_data['quantity'] = 1;
    		        $insert_data['status'] = 1;
    		        $insert_data['created_at'] = Carbon::now()->toDateTimeString();
    		        $result = Productorderitem::insertGetId($insert_data);
                }
                
                if(!empty($result)){
                	$data['success'] = 1;
                }else{
    	            $data['success'] = 0;
    	        }
            }else{
                $data['success'] = 0;
            }
        }else{
            $data['success'] = 2;
        }
        echo json_encode($data);
        exit(0);
    }

    public function quantityIncreament(Request $request){
        //print_r($request->all());die;
        $userDetails = Auth::guard('front')->user();
        $userId = $userDetails['id'];
        $productId = $request['product_id'];
        $order = Productorder::select('order_id')->where(['user_id'=>$userId,'status'=>'pending'])->first();
        $orderId = $order->order_id;

        $oder_item_id = Productorderitem::select('id')->where(['product_id'=>$productId,'order_id'=>$orderId])->get();

        if(isset($oder_item_id) && !empty($oder_item_id) && count($oder_item_id)){
            $item = Productorderitem::find($oder_item_id[0]->id);
            $item->quantity = $request['quantity'];
            $item->updated_at = Carbon::now()->toDateTimeString();
            $result = $item->save();
            
            if(!empty($result)){
                $data['success'] = 1;
            }else{
                $data['success'] = 0;
            }
        }else{
            $data['success'] = 0;
        }
        echo json_encode($data);
        exit(0);

    }

    public function deleteProductFromCart(Request $request){
        $userDetails = Auth::guard('front')->user();
        $userId = $userDetails['id'];
        $product_id = $request['product_id'];
        $productId = Crypt::decryptString($product_id);
        //print_r($productId);die;
        $order = Productorder::select('order_id')->where(['user_id'=>$userId,'status'=>'pending'])->first();
        $orderId = $order->order_id;

        $oder_item_id = Productorderitem::select('id','quantity')->where(['product_id'=>$productId,'order_id'=>$orderId])->get();

        if(isset($oder_item_id) && !empty($oder_item_id) && count($oder_item_id)){
            $item = Productorderitem::find($oder_item_id[0]->id);
            $result = $item->delete();
            
            if(!empty($result)){
                $data['success'] = 1;
                $data['product_id'] = $productId;
            }else{
                $data['success'] = 0;
            }
        }else{
            $data['success'] = 0;
        }
        echo json_encode($data);
        exit(0);

    }

    public function addShippingCharge(Request $request){
        //print_r($request->all());die;
        $userDetails = Auth::guard('front')->user();
        $userId = $userDetails['id'];
        $shippingCharge = $request['shipping_charge'];
        $shippingType = $request['shipping_type'];
        $order = Productorder::select('id')->where(['user_id'=>$userId,'status'=>'pending'])->get();
        if(isset($order) && !empty($order) && count($order)){
            $order = Productorder::find($order[0]->id);
            $order->shipping_charge = ($shippingCharge?$shippingCharge:'0');
            $order->shipping_type = ($shippingType?$shippingType:'');
            $order->updated_at = Carbon::now()->toDateTimeString();
            $result = $order->save();
            
            if(!empty($result)){
                $data['success'] = 1;
            }else{
                $data['success'] = 0;
            }
        }else{
            $data['success'] = 0;
        }
        echo json_encode($data);
        exit(0);

    }

    function generateUniqueNumber() {
	    $number = 'OD'.mt_rand(100000000000000000, 999999999999999999); 
	    if ($this->uniqueNumberExists($number)) {
	        return generateUniqueNumber();
	    }

	    return $number;
	}

	function uniqueNumberExists($number) {
	    return Productorder::where(['order_id' => $number])->exists();
	}


}

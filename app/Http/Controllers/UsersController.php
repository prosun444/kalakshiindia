<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\User;
use Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendingEmail;
use App\Models\admin\ProductCategoryModel as ProductCategory;


class UsersController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'number' => 'required',
            'email' => 'required | unique:users',
            'password' => 'required | min:6 | required_with:confpassword | same:confpassword',
            'confpassword' => 'required | min:6'
        ],[
            'name.required' => 'The Name is Required.',
            'number.required' => 'The Number is Required.',
            'confpassword.required' => 'The Confirm Password is Required.',
            'password.same' => 'The Confirm Password must match.',
            'confpassword.min' => 'The Confirm password must be at least 6 characters.'
        ]);

        if(isset($request->participation) &&  ($request->participation =='2' || $request->participation =='3' || $request->participation =='4')){
            $this->validate($request, [
                'bankname'  => 'required',
                'ifsccode'  => 'required',
                'acnumber'  => 'required',
                'aadhar'    => 'required',
                'panno'     => 'required'

            ],[
                'bankname.required' => 'The Bank Name is Required.',
                'ifsccode.required' => 'The IFSC is Required.',
                'acnumber.required' => 'The Ac Number is Required.',
                'aadhar.required'   => 'The Aadhar is Required.',
                'panno.required'    => 'The PAN must is Required.'
            ]);
        }

        $insert_data = array();

        $insert_data['name'] = $request->name;
        $insert_data['number'] = $request->number;
        $insert_data['email'] = $request->email;
        $insert_data['password'] = Hash::make($request->password);
        $insert_data['address'] = $request->address;
        $insert_data['organizationtype'] = $request->organizationtype;
        $insert_data['participation'] = $request->participation;
        $insert_data['created_at'] = Carbon::now()->toDateTimeString();

        if(isset($request->participation) &&  ($request->participation =='2' || $request->participation =='3' || $request->participation =='4')){
            $insert_data['bankname'] = $request->bankname;
            $insert_data['acnumber'] = $request->acnumber;
            $insert_data['ifsccode'] = $request->ifsccode;
            $insert_data['aadhar'] = $request->aadhar;
            $insert_data['panno'] = $request->panno;
            $insert_data['individualcode'] = $request->individualcode;
        }


        $response = User::insertGetId($insert_data);

        if(!empty($response)){
            
            $user = User::find($response);
            $user->sendEmailVerificationNotification();
            
            return redirect()->route('user.registration')->with('success','Partner Added Successfully');
        }else{
            return redirect()->route('user.registration')->with('error','Change a few things up and submit again');
        }
    }

    //Password Change
    public function changePasswordSubmit(Request $request){
        // print_r($request->all());
        // die("----------");
        $this->validate($request, [
            'old_password' => 'required',
            'password'=> 'required_with:confirm_password|same:confirm_password',
            'confirm_password'=>'required'
        ]);
        $userDetails = Auth::user();
        if (Hash::check($request->old_password, $userDetails->password)) {
            $update_arr = array();
            $update_arr['password'] = Hash::make($request->password);
            $user = Auth::user($request['userId'])->update($update_arr);
            
            if(isset($user) && !empty($user)){
                return redirect()->route('user.userprofile')->with('succ_msg','Password Changed Successfully');
            }else{
                return redirect()->route('user.userprofile')->with('err_msg','Change a few things up and submit again');
            }
        }else{
            return redirect()->route('user.userprofile')->with('err_msg','Mismatch old password');
        }
        
    } 

    //Password Change

    // User Profile View start
    public function userprofile(){
        $userDetails = Auth::user();
        $categorylist = $this->categorymodel();

        return view('front.profile',['userDetails' => $userDetails , 'pcategory' => $categorylist]);
    } 

    public function categorymodel ()
    {   
        $category = array();
        $mastercategory = ProductCategory::where("product_parent_category", 0)->with("subCategory")->get()->toArray();
        return $mastercategory;
    }

    // User Profile View end

    // User Profile Submit start
    public function userprofilesubmit(Request $request){
        
        $this->validate($request, [
            'number' => 'required'
        ],[
            'number.required' => 'The Number is Required.'
        ]);

        if(isset($request->participation) &&  ($request->participation =='2' || $request->participation =='3' || $request->participation =='4')){
            $this->validate($request, [
                'bankname'  => 'required',
                'ifsccode'  => 'required',
                'acnumber'  => 'required',
                'aadhar'    => 'required',
                'panno'     => 'required'

            ],[
                'bankname.required' => 'The Bank Name is Required.',
                'ifsccode.required' => 'The IFSC is Required.',
                'acnumber.required' => 'The Ac Number is Required.',
                'aadhar.required'   => 'The Aadhar is Required.',
                'panno.required'    => 'The PAN must is Required.'
            ]);
        }

        $insert_data = array();

        $insert_data['name'] = $request->first_name.' '.$request->last_name;
        $insert_data['number'] = $request->number;
        $insert_data['email'] = $request->email;
        $insert_data['address'] = $request->address;
        $insert_data['organizationtype'] = $request->organizationtype;
        $insert_data['participation'] = $request->participation;
        $insert_data['created_at'] = Carbon::now()->toDateTimeString();

        if(isset($request->participation) &&  ($request->participation =='2' || $request->participation =='3' || $request->participation =='4')){
            $insert_data['bankname'] = $request->bankname;
            $insert_data['acnumber'] = $request->acnumber;
            $insert_data['ifsccode'] = $request->ifsccode;
            $insert_data['aadhar'] = $request->aadhar;
            $insert_data['panno'] = $request->panno;
            $insert_data['individualcode'] = $request->individualcode;
        }

        $response = User::where('id', $request->userId)->update($insert_data);

        if(!empty($response)){
            
            return redirect()->route('user.userprofile')->with('success','Partner Added Successfully');
        }else{
            return redirect()->route('user.userprofile')->with('error','Change a few things up and submit again');
        }



    }
    // User Profile Submit end
}

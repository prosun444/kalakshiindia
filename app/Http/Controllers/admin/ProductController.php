<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\ProductModel;
use App\Models\admin\ProductStory;
use App\Models\admin\ProductCategoryModel;
use App\Models\admin\ProductColour;
use Illuminate\Support\Str;
use App\Productoffer;
use Carbon\Carbon;
            
//use Image;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();       
        $product_details = ProductModel::select('*')->orderby('id', 'DESC')
                                    ->paginate(10);
        
        return view("admin.product.product_list", compact('product_details') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_product() {
        $product_category = ProductCategoryModel::select('*')->where('product_parent_category', '0')->get();
        $product_category->randval = rand(1000000, 9999999);
        $product_category->ProductColour = ProductColour::select("*")->where('status',1)->get();
        // print_r($product_category->ProductColour);die;
        return view('admin.product.product_create', compact('product_category'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_product_process(Request $request) {
        $validatedData = $request->validate([
            'product_name' => 'required',
            'product_code'=> 'required',
            'product_category' => 'required',
            'product_subcategory' => 'required',
            'product_type'=> 'required',
            'product_price' => 'required|numeric',
            'description' => 'required',
            'usages'=> 'required',
            'special_tips' => 'required',
            'product_images' => 'required'
        ]);

        //dd($request->all());

        if($request->file('product_images')){
            foreach ($request->product_images as $key => $value) {
                
                $image      = $value;
                $sku        = 'product_images_'.rand(10000,99999).time();
                $extension  = $image->getClientOriginalExtension(); // getting image extension
                $realpath   = $image->getRealPath();
                $fileName   = $sku.'.'.$extension; //$image->getClientOriginalName(); // renameing image
                
                //Image resize code
                /*$resize[0]['hight'] = '50';
                $resize[0]['width'] = '50';
                $resize[1]['hight'] = '100';
                $resize[1]['width'] = '100';
                $resize[2]['hight'] = '200';
                $resize[2]['width'] = '200';
                $resize[3]['hight'] = '300';
                $resize[3]['width'] = '400';

                if($request->hasFile('product_images')) {
                    foreach ($resize as $key1 => $size) {
                        $resizepath     = 'picture/product/resize/'.$size['hight'].$size['width'].'/';
                        $image_resize   = Image::make($realpath);
                        $image_resize->resize( $size['width'], $size['hight']);

                        if (!file_exists($resizepath)) {
                            mkdir($resizepath, 775, true);
                        }
                        $image_resize->save(public_path($resizepath.$fileName));
                    }
                }*/

                //Image resize code
                $destinationPath = 'picture/product'; // your destination path
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 775, true);
                }
                $image->move($destinationPath, $fileName);

                $product_images[$key] = $fileName;
            }

        }


        $postdata                   = $request->all();
        $postdata['product_slug']   = Str::slug(($postdata['product_name'].$postdata['product_code']), "-");
        $postdata['product_colour'] = $postdata['product_colour']?$postdata['product_colour']:'';
        $postdata['product_colour_name'] = $postdata['product_colour']?$postdata['product_colour']:'';
        $postdata['product_status'] = 1;
        $postdata['product_images'] = json_encode($product_images);
        $postdata['description']    = addslashes($postdata['description']);
        $postdata['usages']         = addslashes($postdata['usages']);
        $postdata['special_tips']   = addslashes($postdata['special_tips']);

        // print_r($postdata);
        // die;
        $product = ProductModel::create($postdata);

        return redirect()->route('manage_products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_product($id) {
        $product = ProductModel::find($id);
        $product->category = ProductCategoryModel::select('*')->where('product_parent_category', '0')->get();
        $product->allsubcategory = ProductCategoryModel::select('*')->where('product_parent_category', $product->product_category)->get();
        $product->randval = rand(1000, 9999);
        $product->colour = ProductColour::select("*")->where('status',1)->get();
        //print_r($product->allsubcategory);die;
        return view('admin.product.product_edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_product_process(Request $request) {

        //dd($request->all());

        $validatedData = $request->validate([
            'product_name' => 'required',
            'product_code'=> 'required',
            'product_category' => 'required',
            'product_subcategory' => 'required',
            'product_type'=> 'required',
            'product_price' => 'required|numeric',
            'description' => 'required',
            'usages'=> 'required',
            'special_tips' => 'required'
        ]);

        if($request->file('product_images')){
            foreach ($request->product_images as $key => $value) {
                
                $image      = $value;
                $sku        = 'product_images_'.rand(10000,99999).time();
                $extension  = $image->getClientOriginalExtension(); // getting image extension
                $realpath   = $image->getRealPath();
                $fileName   = $sku.'.'.$extension; //$image->getClientOriginalName(); // renameing image
                
                //Image resize code
                $destinationPath = 'picture/product'; // your destination path
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 775, true);
                }
                $image->move($destinationPath, $fileName);

                $product_images[$key] = $fileName;
            }

        }
        $postdata = $request->all();
        
        $product = ProductModel::findOrFail($postdata['id']);
        if(isset($product_images) && !empty($product_images)){
            $postdata['product_images'] = json_encode($product_images);
        }   
        $postdata['product_colour'] = $postdata['product_colour']?$postdata['product_colour']:'';
        $postdata['product_colour_name'] = $postdata['product_colour']?$postdata['product_colour']:''; 

        //dd($postdata);

        if($product->update($postdata)){
    		return redirect()->route('edit_product',$postdata['id'])->with("succ_msg","Product updated successfully");
        }else{
            return redirect()->route('edit_product',$postdata['id'])->with("err_msg","Product not updated successfully");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function product_delete(Request $request,$id) {
        $product = ProductModel::findOrFail($id);
        $product->delete();

        return redirect()->route('manage_products');
    }

    public function product_subcategory(Request $request) {
        $category_id = $request->category_id;
        $subcategory = ProductCategoryModel::select('*')->where('product_parent_category', $category_id)->get();
        //print_r($subcategory);die;
        echo json_encode($subcategory);
        exit(0);
    }

    public function change_product_status(Request $request, $id, $status) {
		if($status == 1)
		{
			$s = 2;
		}
		if($status == 2 || $status == 0)
		{
			$s = 1;
		}
		$product = ProductModel::find($id);
		$update = $product->update(['product_status'=>$s]);
		
		if($update > 0)
		 {
			$request->session()->flash('status', 'Product Status Updated Successfully.'); 
		 }
		 else
		 {
			 $request->session()->flash('status', 'Product Status Not Updated Successfully.');
		 }
		return redirect()->route('manage_products');
	}

    public function manage_product_story() {
        $data = array();
        if (isset($_GET['product']) && !empty($_GET['product'])) {
            $product_story = ProductStory::with('Product')->select('*')->where('product_id', $_GET['product'])->orderby('id', 'DESC')
                                    ->paginate(5);
        }else{
            $product_story = ProductStory::with('Product')->select('*')->orderby('id', 'DESC')
                                    ->paginate(5);
        }       
        return view("admin.product.product_story_list", compact('product_story') );
    }

	public function product_story() {
		$data = array();       
        $product_details = ProductModel::select('*')->where('product_type', 2)->get();
        //print_r($product_details);die;
        return view("admin.product.product_story", compact('product_details') );
	}

	public function product_story_process(Request $request) {
		$postdata = $request->all();
		$validatedData = $request->validate([
            'product_id' => 'required',
            'description'=> 'required',
            'story_image' => 'required'
        ]);

        if($request->file('story_image')){
            $image      = $request->story_image;
            $sku = 'product_story_images_'.rand(10000,99999).time();
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $realpath   = $image->getRealPath();
            $fileName = $sku.'.'.$extension; // renameing image
                
                

            $destinationPath = 'picture/productstory'; // your destination path
            $image->move($destinationPath, $fileName);
        }
        
        $postdata['status'] = 1;
        $postdata['story_image'] = $fileName;
        
        /*print_r($postdata);
        die;*/
        $productStory = ProductStory::create($postdata);
        if($productStory->id){
            return redirect()->route('manage_product_story')->with("succ_msg",'Product Story Submitted Successfully.');
        }else{
            return redirect()->route('manage_product_story')->with("err_msg",'Product Story not Submitted Successfully.');
        }
	}

    public function change_product_story_status(Request $request, $id, $status) {
        if($status == 1)
        {
            $s = 2;
        }
        if($status == 2 || $status == 0)
        {
            $s = 1;
        }
        $product = ProductStory::find($id);
        $update = $product->update(['status'=>$s]);
        
        if($update > 0)
         {
            $request->session()->flash('succ_msg', 'Product Story Status Updated Successfully.'); 
         }
         else
         {
             $request->session()->flash('err_msg', 'Product Story Status Not Updated Successfully.');
         }
        return redirect()->route('manage_product_story');
    }

    public function edit_product_story($id) {
        $story = ProductStory::find($id);
        $story->product_details = ProductModel::select('*')->where('product_type', 2)->get();
        
        return view('admin.product.product_story_edit', compact('story'));
    }
    
    public function edit_product_story_process(Request $request) {
        $validatedData = $request->validate([
            'product_id' => 'required',
            'description'=> 'required'
        ]);

        if($request->file('story_image')){
            
            $image      = $request->story_image;
            $sku        = 'product_story_images_'.rand(10000,99999).time();
            $extension  = $image->getClientOriginalExtension(); // getting image extension
            $realpath   = $image->getRealPath();
            $fileName   = $sku.'.'.$extension; //$image->getClientOriginalName(); // renameing image
            
            //Image resize code
            $destinationPath = 'picture/productstory'; // your destination path
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 775, true);
            }
            $image->move($destinationPath, $fileName);
        
        }
        $postdata = $request->all();
        $story = ProductStory::findOrFail($postdata['id']);
        if(isset($fileName) && !empty($fileName)){
            $postdata['story_image'] = $fileName;
        }  
        /*print_r($postdata);
        die;
        */
        if($story->update($postdata)){
            return redirect()->route('edit_product_story',$postdata['id'])->with("succ_msg","Product Story updated successfully");
        }else{
            return redirect()->route('edit_product_story',$postdata['id'])->with("err_msg","Product Story not updated successfully");
        }
    }   

    public function product_story_delete(Request $request,$id) {
        $product = ProductStory::findOrFail($id);
        if($product->delete()){
            return redirect()->route('manage_product_story')->with("succ_msg",'Product Story Deleted Successfully.');
        }else{
            return redirect()->route('manage_product_story')->with("err_msg",'Product Story notDeleted Successfully.');
        }
    }

    public function manage_product_offer($id) {   
        $productoffer = [];
        $productoffer = Productoffer::where(['productid' => $id])->whereNotNull('deleted_at')->orWhereNull('deleted_at')->first();
        return view('admin.product.product_offer' , ['productid' => $id , 'offer' => $productoffer]);
    }

    public function product_offer_process(Request $request) {   
        $validatedData = $request->validate([
            'productid' => 'required',
            'percentage'=> 'required'
        ]);
        $productoffer = Productoffer::where(['productid' => $request->productid])->first();
        if($request->offer_status == 1){
            $productoffer->deleted_at = NULL;
        }else if($request->offer_status == 2){
            $productoffer->deleted_at = Carbon::now("+330 minutes")->toDateTimeString();
        }
        if(!empty($productoffer)){

            $productoffer->productid = $request->productid;
            $productoffer->percentage = $request->percentage;
            $productoffer->save();
            return redirect()->route('manage_product_offer', $request->productid)->with("succ_msg",'Offer Updated Successfully.');
        }else{
            $productoffer = new Productoffer;
            $productoffer->productid = $request->productid;
            $productoffer->percentage = $request->percentage;
            $productoffer->save();
            return redirect()->route('manage_product_offer' , $request->productid)->with("succ_msg",'Offer Added Successfully.');
        }
    } 

    public function add_deals_or_exclusive_product($id) {
        
        if($id == 'deals_of_the_day'){
            $page_type = 'Deals Of The Day';
            $all_products = ProductModel::select('id','product_name')->where('deals_of_the_day', 0)->orderby('id', 'DESC')->get()->toArray();
            $product_details = ProductModel::select('*')->where('deals_of_the_day', 1)->orderby('id', 'DESC')->get();
        }else if($id == 'kalakshi_exclusive_product'){
            $page_type = 'Kalakshi Exclusive Product';
            $all_products = ProductModel::select('id','product_name')->where('kalakshi_exclusive_product' , 0)->orderby('id', 'DESC')->get()->toArray();
            $product_details = ProductModel::select('*')->where('kalakshi_exclusive_product' , 1)->orderby('id', 'DESC')->get();
        }
        
        //print_r($product_details);die;
        return view('admin.product.product_exclusive', ['products' => $all_products , 'product_details' => $product_details , 'page_type' => $page_type , 'page_id' => $id]); 
    }

    public function add_product_as_exclusive(Request $request){
        //print_r($request->all());die;
        $product = ProductModel::findOrFail($request['product_name']);

        if($request['page_id'] == 'deals_of_the_day'){
            $product->deals_of_the_day = 1;
        }else if($request['page_id'] == 'kalakshi_exclusive_product'){
            $product->kalakshi_exclusive_product = 1;
        }

        if($product->save()){
            return redirect()->route('add_deals_or_exclusive_product',[$request['page_id']])->with("succ_msg","Product added into ".$request['page_type']." successfully");
        }else{
            return redirect()->route('add_deals_or_exclusive_product',[$request['page_id']])->with("err_msg","Product does not added into ".$request['page_type']." successfully");
        }

    }

    public function delete_product_from_exclusive(Request $request,$id,$status) {
        $product = ProductModel::findOrFail($id);
        
        if($status == 'deals_of_the_day'){
            $product->deals_of_the_day = 0;
        }else if($status == 'kalakshi_exclusive_product'){
            $product->kalakshi_exclusive_product = 0;
        }
        
        $status_text = implode(' ', explode('_', $status));
        if($product->save()){
            return redirect()->route('add_deals_or_exclusive_product',[$status])->with("succ_msg","Product removed form ".$status_text." successfully");
        }else{
            return redirect()->route('add_deals_or_exclusive_product',[$status])->with("err_msg","Product does not removed from ".$status_text." successfully");
        }
    }

}  

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\SubmitProductCategoryRequest;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\admin\ProductCategoryModel as ProductCategory;

class ProductCategoryController extends Controller
{
    public function index()
   {
	   $data = array();
	   
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   $data['script'][] = "public/js/ckeditor/ckeditor.js";
	   
	   $data['product_category_details'] = DB::table('product_category_details')
									->select('*')
									->orderby('product_category_id', 'DESC')									
									->paginate(10);
	   
	   return view("admin/category/manage_product_category", $data);
   }
   
   public function add_product_category()
   {
	  $data = array();
	   
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   $data['script'][] = "public/js/ckeditor/ckeditor.js";
	   
	   $data['product_parent_category_details'] = ProductCategory::where("product_parent_category", 0)->get();

	   return view("admin/category/add_product_category", $data);	   
   }
   
   public function add_product_category_process(SubmitProductCategoryRequest $request)
   {
	   $submit = new ProductCategory();
	   
	    $submit->product_category_name = $request->input('product_category_name');
		$submit->product_category_slug = $request->input('product_category_slug');
		$submit->product_parent_category = $request->input('product_parent_category');
		$submit->product_category_status = 1;
		
		
		$submit->save();
		if ($submit->save()) {
            $request->session()->flash('status', 'Product Category Added Successfully.');
        } else {
            $request->session()->flash('status', 'Product Category Not Added Successfully.');
        }
		return redirect('/admin_manage7081/manage_product_category');
   }
   
   public function edit_product_category($id)
   {
	  $data = array();
	   
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   $data['script'][] = "public/js/ckeditor/ckeditor.js";
	   
	   $data['product_parent_category_details'] = ProductCategory::where("product_parent_category", 0)->get();
	   $data["product_category_detail"] = ProductCategory::where('product_category_id', $id)->first();

	   return view("admin/category/edit_product_category", $data); 
   }
   
   public function edit_product_category_process(SubmitProductCategoryRequest $request)
   {
		$submit = new ProductCategory();
		
		$product_category_id = $request->input("product_category_id");
		$product_category_name = $request->input('product_category_name');
		$product_category_slug = $request->input('product_category_slug');
		$product_parent_category = $request->input('product_parent_category');
		
		
		$update = DB::update('update product_category_details set product_category_name = ?, product_category_slug = ?, product_parent_category = ? where product_category_id = ?',[$product_category_name, $product_category_slug, $product_parent_category, $product_category_id]);
		
		if($update > 0) {
            $request->session()->flash('status', 'Product Category Updated Successfully.');
        } else {
            $request->session()->flash('status', 'Product Category Not Updated Successfully.');
        }
		return redirect('/admin_manage7081/manage_product_category');
   }
   
   public function product_category_delete(Request $request, $id)
	{
		$res=ProductCategory::where('product_category_id',$id)->delete();
		
		if($res > 0)
		 {
			$request->session()->flash('status', 'Product Category Deleted Successfully.'); 
		 }
		 else
		 {
			 $request->session()->flash('status', 'Product Category Not Deleted.');
		 }
		return redirect('/admin_manage7081/manage_product_category');
	}
	
	public function change_product_category_status(Request $request, $id, $status)
	{
		if($status == 1)
		{
			$s = 2;
		}
		if($status == 2)
		{
			$s = 1;
		}
		
		$update = DB::update('update product_category_details set product_category_status = ? where product_category_id = ?',[ $s, $id]);
		
		if($update > 0)
		 {
			$request->session()->flash('status', 'Product Category Status Updated Successfully.'); 
		 }
		 else
		 {
			 $request->session()->flash('status', 'Product Category Status Not Updated Successfully.');
		 }
		return redirect('/admin_manage7081/manage_product_category');
	}
}

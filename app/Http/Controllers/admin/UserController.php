<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
//use Image;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();       
        $user_details = User::select('*')->orderby('id', 'DESC')
                                    ->paginate(10);
        //print_r($user_details->data);die;
        return view("admin.user.user_list", compact('user_details') );
    }

    public function change_User_status(Request $request, $id, $status)
	{
        if($status == 1)
		{
			$s = 0;
		}
		if($status == 0)
		{
			$s = 1;
		}
		$User = User::find($id);
        $User->status = $s;
		
		if($User->save())
		{
            return redirect()->route('manage_user')->with("succ_msg","User Status Updated Successfully.");
		}
		else
		{
            return redirect()->route('manage_user')->with("err_msg","User Status Not Updated Successfully.");
		}
	}
     
}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\SubmitBlogRequest;
use App\Http\Requests\SubmitEditBlogRequest;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\admin\BlogModel as Blog;
use App\Models\admin\BlogCategoryModel as BlogCategory;

class BlogController extends Controller
{
    public function index()
   {
	   $data = array();
	   
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   $data['script'][] = "public/js/ckeditor/ckeditor.js";
	   
	   $data['blog_details'] = DB::table('blog_details')->select('*')->orderby('blog_id', 'DESC')->get()->toArray();												
	   
	   return view("admin.blog.manage_blog", $data);
   }
   
   public function add_blog()
   {
	  $data = array();
	   
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   $data['script'][] = "public/js/ckeditor/ckeditor.js";
	   
	   $data["category_details"] = BlogCategory::where("blog_category_status", 1)->get(); 

	   return view("admin.blog.add_blog", $data);	   
   }
   
   public function add_blog_process(SubmitBlogRequest $request)
   {
	   $submit = new Blog();
	   
	    $submit->blog_title = $request->input('blog_title');
		$submit->blog_slug = $request->input('blog_slug');
		$submit->blog_category = $request->input('blog_category');
		$submit->blog_description = $request->input('blog_description');
		$submit->blog_status = 1;
		$submit->blog_added_on = date("Y-m-d h:i:s");
		
		// Single Image Upload
		
		if($request->has('blog_img')) {
				$imageName = time().'.'.request()->blog_img->getClientOriginalExtension();  

				$res = request()->blog_img->move(public_path('uploads/blog_images'), $imageName);	
				
				$submit->blog_image = $imageName;
			}
			
		// Ends here
		
		//dd($submit);
		
		$submit->save();
		if ($submit->save()) {
            $request->session()->flash('status', 'Blog Added Successfully.');
        } else {
            $request->session()->flash('status', 'Blog not Added Successfully.');
        }
		return redirect('/admin_manage7081/manage_blog');
   }
   
   public function edit_blog($id)
   {
	  $data = array();
	   
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   $data['script'][] = "public/js/ckeditor/ckeditor.js";
	   
	   $data["category_details"] = BlogCategory::where("blog_category_status", 1)->get(); 
	   $data["blog_detail"] = Blog::where('blog_id', $id)->first();

	   return view("admin.blog.edit_blog", $data); 
   }
   
   public function edit_blog_process(SubmitEditBlogRequest $request)
   {
		$submit = new Blog();
		
		$blog_id = $request->input("blog_id");
		$blog_title = $request->input("blog_title");
		$blog_slug = $request->input("blog_slug");
		$blog_category = $request->input("blog_category");
		$blog_description = $request->input("blog_description");
		
		// Single Image Upload
		
		if($request->has('blog_img')) {
				$imageName = time().'.'.request()->blog_img->getClientOriginalExtension();  

				$res = request()->blog_img->move(public_path('uploads/blog_images'), $imageName);	
				
				$blog_image = $imageName;
			}
			
		// Ends here
		
		//dd($submit);
		
		if(!empty($blog_image)) {
			$update = DB::update('update blog_details set blog_title = ?, blog_slug = ?, blog_category =?, blog_description = ?, blog_image=? where blog_id = ?',[$blog_title, $blog_slug, $blog_category, $blog_description, $blog_image, $blog_id]);
		} else {
			$update = DB::update('update blog_details set blog_title = ?, blog_slug = ?, blog_category = ?, blog_description = ? where blog_id = ?',[$blog_title, $blog_slug, $blog_category, $blog_description, $blog_id]);
		}
		if($update > 0) {
            $request->session()->flash('status', 'Blog Updated Successfully.');
        } else {
            $request->session()->flash('status', 'Blog not Updated Successfully.');
        }
		return redirect('/admin_manage7081/manage_blog');
   }
   
   public function blog_delete(Request $request, $id)
	{
		$res=Blog::where('blog_id',$id)->delete();
		
		if($res > 0)
		 {
			$request->session()->flash('status', 'Blog Deleted Successfully.'); 
		 }
		 else
		 {
			 $request->session()->flash('status', 'Blog Not Deleted.');
		 }
		return redirect('/admin_manage7081/manage_blog');
	}
	
	public function change_blog_status(Request $request, $id, $status)
	{
		if($status == 1)
		{
			$s = 2;
		}
		if($status == 2)
		{
			$s = 1;
		}
		
		$update = DB::update('update blog_details set blog_status = ? where blog_id = ?',[ $s, $id]);
		
		if($update > 0)
		 {
			$request->session()->flash('status', 'Blog Status Updated Successfully.'); 
		 }
		 else
		 {
			 $request->session()->flash('status', 'Blog Status Not Updated Successfully.');
		 }
		return redirect('/admin_manage7081/manage_blog');
	}
}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests\SubmitBgRequest;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\admin\AdminLogin_Model as Admin;
use App\Models\admin\Place_Background_img_Model as PlaceBG;

class AdminLoginController extends Controller
{
    public function admin_login()
   {
	   $data['stylesheet'][] = "css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "css/admin_style.css";
	   $data['stylesheet'][] = "css/admin_font-awesome.css";
	   
	   $data['script'][] = "js/admin_jquery.min.js";
	   $data['script'][] = "js/admin_bootstrap.min.js";
	   
	   $data["title"] = "Kalakshi Creations Admin | Dashboard";
	   return view("admin/admin_login", $data);
   }
   
   public function dashboard()
   {
	   $data = array();
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   
	   return view("admin/admin_dashboard", $data);
   }
   
   public function logout()
	{
		if (session()->has('admin_id')) {
            session()->forget('admin_id');
			session()->forget('admin_name');
            session()->flush();
            return redirect('/admin_manage7081')->with('loginError', 'You have successfully logged out.');
        } else {
            return redirect('/admin_manage7081')->with('loginError', 'You are already logged out.');
        }
	}
	
	public function change_password()
	{
	   $data = array();
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   
	   
	   return view('admin/change_password', $data);
	}
	
	public function change_password_process(Request $request)
	{
		$submit = new Admin();
			
		$admin_id = Session::get('admin_id');
		$new_pass = md5($request->input('new_pass'));
		$confirm_pass = md5($request->input('confirm_pass'));
		
		
		//dd($submit);
		if($new_pass == $confirm_pass)
		{ 
			$update = DB::update('update user_details set password = ? where user_id = ?',[$confirm_pass, $admin_id]);				
				if($update > 0) {
					$request->session()->flash('status', 'Your Password Updated Successfully.');
				} else {
					$request->session()->flash('status', 'Your Password not Updated Successfully.');
				}
		}
		else
		{
			$request->session()->flash('status', 'Incorrect Passwoed Or Confirm Password.');
		}
		
		
		return redirect('/admin_manage7081/password_change');
	}
	
	public function bg_image_book_place()
	{
		$data = array();
	   $data['stylesheet'][] = "public/css/admin_bootstrap.min.css";
       $data['stylesheet'][] = "public/css/admin_style.css";
	   $data['stylesheet'][] = "public/css/admin_custom.css";
	   $data['stylesheet'][] = "public/css/admin_lines.css";
	   $data['stylesheet'][] = "public/css/admin_font-awesome.css";
	   $data['stylesheet'][] = "public/css/admin_clndr.css";
	   $data['stylesheet'][] = "public/css/admin_jqvmap.css";
	   
	   $data['script'][] = "public/js/admin_jquery.min.js";
	   $data['script'][] = "public/js/admin_bootstrap.min.js";
	   $data['script'][] = "public/js/admin_metisMenu.min.js";
	   $data['script'][] = "public/js/admin_custom.js";
	   $data['script'][] = "public/js/admin_d3.v3.js";
	   $data['script'][] = "public/js/admin_rickshaw.js";
	   $data['script'][] = "public/js/admin_underscore-min.js";
	   $data['script'][] = "public/js/admin_moment-2.2.1.js";
	   $data['script'][] = "public/js/admin_clndr.js";
	   $data['script'][] = "public/js/admin_site.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.sampledata.js";
	   $data['script'][] = "public/js/admin_jquery.vmap.world.js";
	   
	   
	   return view('admin/bg_img_place_book', $data);
	}
	
	public function bg_image_book_place_process(SubmitBgRequest $request)
	{
		$submit = new PlaceBG();
		
		// Single Image Upload
		
		if($request->has('bg_img')) {
				$imageName = time().'.'.request()->bg_img->getClientOriginalExtension();  

				$res = request()->bg_img->move(public_path('uploads/place_booking/'), $imageName);	
				
				$submit->place_bg_img = $imageName;
			}
			
		// Ends here
		$submit->save();
		if ($submit->save()) {
            $request->session()->flash('status', 'Background Image Added Successfully.');
        } else {
            $request->session()->flash('status', 'Background Image not Added Successfully.');
        }
		return redirect('/admin/dashboard');
	}
}

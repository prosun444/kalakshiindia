<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        //Add these following lines
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:front')->except('logout');
    }

    //ADMIN USER lOG IN
    public function userAuth(Request $request)
    {
        $errors = new MessageBag;
        $reqdata = $request->all();
        
        $this->validate($request, [
            'user_name' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->input('user_name'), 'password' => $request->input('password')]) ) {

                return redirect()->route('admin.dashboard');
        }else{
                $errors = new MessageBag(['password' => "Incorrect password"]);
                return redirect()->back()->withErrors($errors)->withInput($request->only('password'));
        }

    }

    //front USER lOG IN
    public function frontuserAuth(Request $request)
    {
        $errors = new MessageBag;
        $reqdata = $request->all();

       //dd($reqdata);
        
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::guard('front')->attempt(['email' => $request->input('email'), 'password' => $request->input('password') ,'status' => '1']) ) {
                return redirect()->route('user.userprofile');
        }else{
                $errors = new MessageBag(['password' => "Incorrect password." , 'email' => "Incorrect  Email."]);
                return redirect()->back()->withErrors($errors)->withInput($request->only('password'));
        }

    }

    //log Out
    public function logout(Request $request)
    {   
        if(Auth::guard('admin')->check()){

            $this->guard('admin')->logout();
            Session::flush();
            return redirect('/admin_manage7081');
    
        }elseif(Auth::guard('front')->check()){
            $this->guard('front')->logout();
            Session::flush();
            return redirect('/login');
        }

        //Login Status
        
    }

}

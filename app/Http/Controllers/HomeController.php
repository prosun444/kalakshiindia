<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubmitBlogRequest;
use App\Http\Requests\SubmitEditBlogRequest;
use DB;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\admin\BlogModel as Blog;
use App\Models\admin\BlogCategoryModel as BlogCategory;
use App\Models\admin\ProductCategoryModel as ProductCategory;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $data = array();
        $data['pcategory'] = $this->categorymodel();

        return view('front.home' , $data);
    }

    public function categorymodel ()
    {   
    	$category = array();
    	$mastercategory = ProductCategory::where("product_parent_category", 0)->with("subCategory")->get()->toArray();
    	return $mastercategory;
    }
    
    public function registration()
    {   
    	$data = array();
        $data['pcategory'] = $this->categorymodel();
        return view('front.registration' , $data);
    }
    public function blog_listing()
	{
	   $data = array();
	   
	   $data['stylesheet'][] = "public/css/blog_css/media_query.css";
       $data['stylesheet'][] = "public/css/blog_css/bootstrap.css";
	   $data['stylesheet'][] = "public/css/blog_css/animate.css";
	   $data['stylesheet'][] = "public/css/blog_css/owl.carousel.css";
	   $data['stylesheet'][] = "public/css/blog_css/owl.theme.default.css";
	   $data['stylesheet'][] = "public/css/blog_css/style_1.css";
	   
	   $data['script'][] = "public/js/blog_js/modernizr-3.5.0.min.js";
	   $data['script'][] = "public/js/blog_js/owl.carousel.min.js";
	   $data['script'][] = "public/js/blog_js/jquery.waypoints.min.js";
	   $data['script'][] = "public/js/blog_js/main.js";
	   
	   $data['title'] = "Kalakshi Creations | Blog";
	   $data['category_details'] = BlogCategory::where("blog_category_status", 1)->get();
	   $data['header_blog'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(3)->take(1)->get();
	   $data['big_thumb'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(0)->take(1)->get();
	   $data['thumb_blogs'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(1)->take(4)->get();
	   $data['trending_blogs'] = Blog::where("blog_category", 5)->where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(0)->take(5)->get();
	   $data['news_blog_1'] = Blog::where("blog_category", 3)->where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(0)->take(4)->get();
	   $data['news_blog_2'] = Blog::where("blog_category", 2)->where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(0)->take(4)->get();
	   $data['popular_posts'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(5)->take(5)->get();
		
	   return view('blog_homepage', $data);
	}
	
	public function blog_single($slug)
	{
		$data = array();
	   
	   $data['stylesheet'][] = "public/css/blog_css/media_query.css";
       $data['stylesheet'][] = "public/css/blog_css/bootstrap.css";
	   $data['stylesheet'][] = "public/css/blog_css/animate.css";
	   $data['stylesheet'][] = "public/css/blog_css/owl.carousel.css";
	   $data['stylesheet'][] = "public/css/blog_css/owl.theme.default.css";
	   $data['stylesheet'][] = "public/css/blog_css/style_1.css";
	   
	   $data['script'][] = "public/js/blog_js/modernizr-3.5.0.min.js";	   
	   $data['script'][] = "public/js/blog_js/owl.carousel.min.js";
	   $data['script'][] = "public/js/blog_js/jquery.waypoints.min.js";
	   $data['script'][] = "public/js/blog_js/jquery.stellar.min.js";
	   $data['script'][] = "public/js/blog_js/main.js";
	   
	   $data['title'] = "Kalakshi Creations | Blog Details";
	   $data['category_details'] = BlogCategory::where("blog_category_status", 1)->get();
	   $data['header_blog'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(3)->take(1)->get();
	   $data['blog_details'] = Blog::where("blog_slug", $slug)->first();
	   //echo "<pre>"; print_r($data['blog_details']); die;
	   $data['popular_posts'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(0)->take(5)->get();
	   $data['releted_posts'] = Blog::where("blog_status", "!=", $slug)->orderBy('blog_id', 'DESC')->skip(0)->take(5)->get();
		
	   return view('blog_single', $data);
	}
	
	public function blog_category_listing($slug)
	{
		$data['stylesheet'][] = "public/css/blog_css/media_query.css";
       $data['stylesheet'][] = "public/css/blog_css/bootstrap.css";
	   $data['stylesheet'][] = "public/css/blog_css/animate.css";
	   $data['stylesheet'][] = "public/css/blog_css/owl.carousel.css";
	   $data['stylesheet'][] = "public/css/blog_css/owl.theme.default.css";
	   $data['stylesheet'][] = "public/css/blog_css/style_1.css";
	   
	   $data['script'][] = "public/js/blog_js/modernizr-3.5.0.min.js";	   
	   $data['script'][] = "public/js/blog_js/owl.carousel.min.js";
	   $data['script'][] = "public/js/blog_js/jquery.waypoints.min.js";
	   $data['script'][] = "public/js/blog_js/jquery.stellar.min.js";
	   $data['script'][] = "public/js/blog_js/main.js";
	   
	   $data['title'] = "Kalakshi Creations | Category Blogs";
	   $data['header_blog'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(3)->take(1)->get();
	   $data['category_details'] = BlogCategory::where("blog_category_status", 1)->get();
	   $cat_details = BlogCategory::where('blog_category_slug', $slug)->first();
	   $blog_cat_id = $cat_details->blog_category_id;
	   $data['category_blog_details'] = Blog::where("blog_category", $blog_cat_id)->paginate(5);
	   $data['popular_posts'] = Blog::where("blog_status", 1)->orderBy('blog_id', 'DESC')->skip(0)->take(5)->get();
	   //echo "<pre>"; print_r($data['category_blog_details']); die;
	   
	   return view('category_blog', $data);
	}
}

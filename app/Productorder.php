<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productorder extends Model
{
    
	use  SoftDeletes;

    protected $table = 'product_order';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id' , 'user_id', 'tax_percentage', 'total_amount', 'status', 'shipping_charge', 'shipping_type'
    ];


    public function productOrderItem() {
        return $this->hasMany('App\Productorderitem', 'order_id', 'order_id')->with('productDetails');
    }
}

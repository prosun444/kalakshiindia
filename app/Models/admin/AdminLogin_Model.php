<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class AdminLogin_Model extends Model
{
    protected $table = 'user_details';
    protected $primaryKey = 'user_id';
	public $timestamps = false;
}

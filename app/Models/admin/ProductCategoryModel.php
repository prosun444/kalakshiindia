<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryModel extends Model
{
    protected $table = 'product_category_details';
    protected $primaryKey = 'product_category_id';
	public $timestamps = false;

	public function subCategory() {
        return $this->hasMany('App\Models\admin\ProductCategoryModel', 'product_parent_category', 'product_category_id');
    }
}

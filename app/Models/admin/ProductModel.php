<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductModel extends Model
{
	use SoftDeletes;
    protected $table = 'product_details';
    protected $primaryKey = 'id';
	//public $timestamps = false;

	protected $fillable = ['id','product_name','product_slug','product_code','product_category','product_subcategory','product_type','product_price','product_colour','product_colour_name','description','usages','special_tips','product_images','product_status'];
}

<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    protected $table = 'blog_details';
    protected $primaryKey = 'blog_id';
	public $timestamps = false;
}

<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryModel extends Model
{
    protected $table = 'blog_category_details';
    protected $primaryKey = 'blog_category_id';
	public $timestamps = false;
}

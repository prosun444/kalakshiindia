<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductColour extends Model
{
	use SoftDeletes;
    protected $table = 'product_colour';
    protected $primaryKey = 'id';
	//public $timestamps = false;

	protected $fillable = ['colour','colour_code','status'];
}
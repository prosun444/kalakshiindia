<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductStory extends Model
{
	use SoftDeletes;
    protected $table = 'product_story';
    protected $primaryKey = 'id';
	//public $timestamps = false;

	protected $fillable = ['product_id','story_image','description','status'];

	public function Product() {
        return $this->hasOne('App\Models\admin\ProductModel', 'id', 'product_id');
    }
}
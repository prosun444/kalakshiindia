<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Productoffer extends Model
{
    
    //use  SoftDeletes;

    protected $table = 'product_offer';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'productid' , 'percentage'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Productorderitem extends Model
{
    //use  SoftDeletes;

    protected $table = 'product_order_items';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id' , 'product_id', 'amount', 'discount', 'quantity', 'status'
    ];

    public function productDetails(){
        return $this->hasOne('App\Product', 'id', 'product_id')->with('offer')->with('subCategory');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingCharge extends Model
{
    
    //use  SoftDeletes;

    protected $table = 'shipping_charge';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'delivery_type' , 'delivery_charge' , 'min_delivery_days' , 'max_delivery_days' , 'status'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    
    use  SoftDeletes;

    protected $table = 'product_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name', 'product_slug', 'product_code', 'product_category', 'product_type', 'product_price'
    ];

    public function offer() {
        return $this->hasOne('App\Productoffer','productid','id');
    }

    public function subCategory() {
        return $this->hasOne('App\Models\admin\ProductCategoryModel','product_category_id','product_subcategory');
    }

}

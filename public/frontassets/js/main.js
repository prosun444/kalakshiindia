
$( document ).ready(function() {

  var participation = $("#participation option:selected").val();

  $("#success").fadeOut(4000);
  $("#error").fadeOut(4000);

  if(participation =='2' || participation =='3'|| participation =='4'){
    $('#bankinfo').css('display','block');
    $('#extrainfo').css('display','block');
  }else{
    $('#bankinfo').css('display','none');
    $('#extrainfo').css('display','none');
  }

  $(".dropdown-large").hover(function () {
        $(this).toggleClass("open");
  });

  $('#participation').on('change', function() {
    
    if(this.value =='2' || this.value =='3'|| this.value =='4'){
      $('#bankinfo').css('display','block');
      $('#extrainfo').css('display','block');

      $('.bankinfo').css('display','block');
      $('.extrainfo').css('display','block');
    }else{
      //$('#bankinfo').css('display','none');
      //$('#extrainfo').css('display','none');

      $('.bankinfo').css('display','none');
      $('.extrainfo').css('display','none');
    }
  });  

});

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
//document.getElementById("defaultOpen").click();


/*var words = document.getElementsByClassName("word");
var wordArray = [];
var currentWord = 0;

// words[currentWord].style.opacity = 1;
// for (var i = 0; i < words.length; i++) {
//   splitLetters(words[i]);
// }

// function changeWord() {
//   var cw = wordArray[currentWord];
//   var nw =
//     currentWord == words.length - 1 ? wordArray[0] : wordArray[currentWord + 1];
//   for (var i = 0; i < cw.length; i++) {
//     animateLetterOut(cw, i);
//   }

//   for (var i = 0; i < nw.length; i++) {
//     nw[i].className = "letter behind";
//     nw[0].parentElement.style.opacity = 1;
//     animateLetterIn(nw, i);
//   }

  currentWord = currentWord == wordArray.length - 1 ? 0 : currentWord + 1;
}*/

function animateLetterOut(cw, i) {
  setTimeout(function () {
    cw[i].className = "letter out";
  }, i * 80);
}

function animateLetterIn(nw, i) {
  setTimeout(function () {
    nw[i].className = "letter in";
  }, 340 + i * 80);
}

function splitLetters(word) {
  var content = word.innerHTML;
  word.innerHTML = "";
  var letters = [];
  for (var i = 0; i < content.length; i++) {
    var letter = document.createElement("span");
    letter.className = "letter";
    letter.innerHTML = content.charAt(i);
    word.appendChild(letter);
    letters.push(letter);
  }

  wordArray.push(letters);
}

//changeWord();
//setInterval(changeWord, 4000);

function logout () {
  document.logoutform.submit();
}
// For Filters
document.addEventListener("DOMContentLoaded", function() {
var filterBtn = document.getElementById('filter-btn');
var btnTxt = document.getElementById('btn-txt');
var filterAngle = document.getElementById('filter-angle');

$('#filterbar').collapse(false);
var count = 0, count2 = 0;
function changeBtnTxt() {
$('#filterbar').collapse(true);
count++;
if (count % 2 != 0) {
filterAngle.classList.add("fa-angle-right");
btnTxt.innerText = "show filters"
filterBtn.style.backgroundColor = "#36a31b";
}
else {
filterAngle.classList.remove("fa-angle-right")
btnTxt.innerText = "hide filters"
filterBtn.style.backgroundColor = "#ff935d";
}

}

// For Applying Filters
$('#inner-box').collapse(false);
$('#inner-box2').collapse(false);

// For changing NavBar-Toggler-Icon
var icon = document.getElementById('icon');

function chnageIcon() {
count2++;
if (count2 % 2 != 0) {
icon.innerText = "";
icon.innerHTML = '<span class="far fa-times-circle" style="width:100%"></span>';
icon.style.paddingTop = "5px";
icon.style.paddingBottom = "5px";
icon.style.fontSize = "1.8rem";


}
else {
icon.innerText = "";
icon.innerHTML = '<span class="navbar-toggler-icon"></span>';
icon.style.paddingTop = "5px";
icon.style.paddingBottom = "5px";
icon.style.fontSize = "1.2rem";
}
}

// Showing tooltip for AVAILABLE COLORS
$(function () {
$('[data-tooltip="tooltip"]').tooltip()
})

// For Range Sliders
var inputLeft = document.getElementById("input-left");
var inputRight = document.getElementById("input-right");

var thumbLeft = document.querySelector(".slider > .thumb.left");
var thumbRight = document.querySelector(".slider > .thumb.right");
var range = document.querySelector(".slider > .range");

var amountLeft = document.getElementById('amount-left')
var amountRight = document.getElementById('amount-right')

/*function setLeftValue() {
var _this = inputLeft,
min = parseInt(_this.min),
max = parseInt(_this.max);

_this.value = Math.min(parseInt(_this.value), parseInt(inputRight.value) - 1);

var percent = ((_this.value - min) / (max - min)) * 100;

thumbLeft.style.left = percent + "%";
range.style.left = percent + "%";
amountLeft.innerText = parseInt(percent * 100);
}
setLeftValue();

function setRightValue() {
var _this = inputRight,
min = parseInt(_this.min),
max = parseInt(_this.max);

_this.value = Math.max(parseInt(_this.value), parseInt(inputLeft.value) + 1);

var percent = ((_this.value - min) / (max - min)) * 100;

amountRight.innerText = parseInt(percent * 100);
thumbRight.style.right = (100 - percent) + "%";
range.style.right = (100 - percent) + "%";
}
setRightValue();

inputLeft.addEventListener("input", setLeftValue);
inputRight.addEventListener("input", setRightValue);

inputLeft.addEventListener("mouseover", function () {
thumbLeft.classList.add("hover");
});
inputLeft.addEventListener("mouseout", function () {
thumbLeft.classList.remove("hover");
});
inputLeft.addEventListener("mousedown", function () {
thumbLeft.classList.add("active");
});
inputLeft.addEventListener("mouseup", function () {
thumbLeft.classList.remove("active");
});

inputRight.addEventListener("mouseover", function () {
thumbRight.classList.add("hover");
});
inputRight.addEventListener("mouseout", function () {
thumbRight.classList.remove("hover");
});
inputRight.addEventListener("mousedown", function () {
thumbRight.classList.add("active");
});
inputRight.addEventListener("mouseup", function () {
thumbRight.classList.remove("active");
});*/
});
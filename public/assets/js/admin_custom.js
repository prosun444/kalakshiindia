$(document).ready(function(){
        var base_url = window.location.origin ;
        

        $(document).on("change","#product_category",function(){
            var product_category_id = $("#product_category option:selected").val();
            

            if(product_category_id != ''){
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
              $.ajax({
                  url: base_url + "/admin_manage7081/product_category_wise_subcategory",
                  type: "post",
                  datatype: "json",
                  data: { 'category_id': product_category_id },
                  success: function (response) {
                      var response = JSON.parse(response);
                      
                      var html = '<option>Product Subcategory</option>';
                      $.each(response, function(key, value) {
                        html += '<option value="'+value.product_category_id+'">'+value.product_category_name+'</option>';
                      });
                      console.log(html);
                      $("#product_subcategory").html(html);
                      $(".product_subcat_div").css("display", "block");
                      //$("#product_subcat_div").closest("label").css("display", "block");
                      //$("#product_category_div").css('display','block');
                  }
              });
            }else{
              $(".product_subcat_div").css("display", "none");
              //$("#product_subcat_div").closest("label").css("display", "none");
              
              //$("#product_category_div").css('display','none');
            }
        })
})

/*$(function() {

    $('#side-menu').metisMenu();

});*/

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
/*$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});*/

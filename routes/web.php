<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

/********** login Panel ************/

Route::post('/userlogin', 'Auth\LoginController@frontuserAuth')->name('user.frontlogin');
Route::get('/userprofile', 'UsersController@userprofile')->name('user.userprofile')->middleware('auth:front');
Route::post('/userprofilesubmit', 'UsersController@userprofilesubmit')->name('user.userprofilesubmit')->middleware('auth:front');
Route::post('/changePasswordSubmit', 'UsersController@changePasswordSubmit')->name('user.changePasswordSubmit')->middleware('auth:front');
Route::post('/frontlogout', 'Auth\LoginController@frontlogout')->name('user.frontlogout');

/********** front Panel ************/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/registration', 'HomeController@registration')->name('user.registration');
Route::post('/registration/store', 'UsersController@store')->name('registration.store');


Route::get('/blog', 'HomeController@blog_listing')->name('blogs');
Route::get('/blog_single/{slug}', 'HomeController@blog_single')->name('singleblogs');
Route::get('/category/{slug}', 'HomeController@blog_category_listing')->name('singleblogs.slug');

/********** Admin Panel ************/

Route::get('/admin_manage7081', 'admin\AdminLoginController@admin_login')->name('admin.adminlogin');
//LOGIN lOGOUT
Route::post('admin_manage7081/login',  'Auth\LoginController@userAuth')->name('adminuser.auth');
Route::post('admin_manage7081/logout', 'Auth\LoginController@logout')->name('adminuser.logout');
Route::get('/admin_manage7081/dashboard', 'admin\AdminLoginController@dashboard')->name('admin.dashboard')->middleware('auth:admin');

    
Route::get('/admin_manage7081/password_change', 'admin\AdminLoginController@change_password')->middleware('auth:admin');
Route::post('/admin_manage7081/process_password_change', 'admin\AdminLoginController@change_password_process')->middleware('auth:admin');
Route::get('/admin_manage7081/bg_image_book_place', 'admin\AdminLoginController@bg_image_book_place')->middleware('auth:admin');
Route::post('/admin_manage7081/bg_image_book_place_process', 'admin\AdminLoginController@bg_image_book_place_process')->middleware('auth:admin');



/***** Blog Section *****/

Route::get('/admin_manage7081/manage_blog', 'admin\BlogController@index')->name('admin.blogmanage')->middleware('auth:admin');
Route::get('/admin_manage7081/add_blog', 'admin\BlogController@add_blog')->name('addblogmanage')->middleware('auth:admin');
Route::post('/admin_manage7081/add_blog_process', 'admin\BlogController@add_blog_process')->middleware('auth:admin');
Route::get('/admin_manage7081/edit_blog/{id}', 'admin\BlogController@edit_blog')->name('editblogmanage')->middleware('auth:admin');
Route::post('/admin_manage7081/edit_blog_process', 'admin\BlogController@edit_blog_process')->middleware('auth:admin');
Route::get('/admin_manage7081/delete_blog/{id}', 'admin\BlogController@blog_delete')->middleware('auth:admin');
Route::get('/admin_manage7081/change_blog_status/{id}/{status}', 'admin\BlogController@change_blog_status')->middleware('auth:admin');

/***** Blog Section *****/


/***** Product Category Section *****/

Route::get('/admin_manage7081/manage_product_category', 'admin\ProductCategoryController@index')->name('manage_product_category')->middleware('auth:admin');
Route::get('/admin_manage7081/add_product_category', 'admin\ProductCategoryController@add_product_category')->name('add_product_category')->middleware('auth:admin');
Route::post('/admin_manage7081/add_product_category_process', 'admin\ProductCategoryController@add_product_category_process')->name('add_product_category_process')->middleware('auth:admin');
Route::get('/admin_manage7081/edit_product_category/{id}', 'admin\ProductCategoryController@edit_product_category')->name('edit_product_category')->middleware('auth:admin');
Route::post('/admin_manage7081/edit_product_category_process', 'admin\ProductCategoryController@edit_product_category_process')->name('edit_product_category_process')->middleware('auth:admin');
Route::get('/admin_manage7081/delete_product_category/{id}', 'admin\ProductCategoryController@product_category_delete')->middleware('auth:admin');
Route::get('/admin_manage7081/change_product_category_status/{id}/{status}', 'admin\ProductCategoryController@change_product_category_status')->middleware('auth:admin');


/***** Product Category Section *****/

/***** Product Section *****/

Route::get('/admin_manage7081/manage_products', 'admin\ProductController@index')->name('manage_products')->middleware('auth:admin');
Route::get('/admin_manage7081/add_product', 'admin\ProductController@add_product')->name('add_product')->middleware('auth:admin');
Route::post('/admin_manage7081/add_product_process', 'admin\ProductController@add_product_process')->name('add_product_process')->middleware('auth:admin');
Route::get('/admin_manage7081/edit_product/{id}', 'admin\ProductController@edit_product')->name('edit_product')->middleware('auth:admin');
Route::post('/admin_manage7081/edit_product_process', 'admin\ProductController@edit_product_process')->name("edit_product_process")->middleware('auth:admin');
Route::get('/admin_manage7081/delete_product/{id}', 'admin\ProductController@product_delete')->middleware('auth:admin');
Route::get('/admin_manage7081/change_product_status/{id}/{status}', 'admin\ProductController@change_product_status')->middleware('auth:admin');
Route::post('/admin_manage7081/product_category_wise_subcategory', 'admin\ProductController@product_subcategory')->middleware('auth:admin');

Route::get('/admin_manage7081/{id}', 'admin\ProductController@add_deals_or_exclusive_product')->name('add_deals_or_exclusive_product')->middleware('auth:admin');
Route::get('/admin_manage7081/delete_product_from_exclusive/{id}/{status}', 'admin\ProductController@delete_product_from_exclusive')->middleware('auth:admin');
Route::post('/admin_manage7081/add_product_as_exclusive', 'admin\ProductController@add_product_as_exclusive')->name('add_product_as_exclusive')->middleware('auth:admin');

/***** Product Section *****/

/***** Product offer *****/
Route::get('/admin_manage7081/manage_product_offer/{id}', 'admin\ProductController@manage_product_offer')->name("manage_product_offer")->middleware('auth:admin');
Route::post('/admin_manage7081/product_offer_process', 'admin\ProductController@product_offer_process')->name("product_offer_process")->middleware('auth:admin');

/***** Product offer *****/

/***** Product Story Section *****/
Route::get('/admin_manage7081/manage_product_story', 'admin\ProductController@manage_product_story')->name('manage_product_story')->middleware('auth:admin');
Route::get('/admin_manage7081/product_story', 'admin\ProductController@product_story')->name('product_story')->middleware('auth:admin');
Route::post('/admin_manage7081/product_story_process', 'admin\ProductController@product_story_process')->name('product_story_process')->middleware('auth:admin');
Route::get('/admin_manage7081/change_product_story_status/{id}/{status}', 'admin\ProductController@change_product_story_status')->middleware('auth:admin');
Route::get('/admin_manage7081/edit_product_story/{id}', 'admin\ProductController@edit_product_story')->name("edit_product_story")->middleware('auth:admin');
Route::post('/admin_manage7081/edit_product_story_process', 'admin\ProductController@edit_product_story_process')->name("edit_product_story_process")->middleware('auth:admin');
Route::get('/admin_manage7081/delete_product_story/{id}', 'admin\ProductController@product_story_delete')->middleware('auth:admin');


/***** Product Story Section *****/

Route::get('/contact', 'EmailController@index');
Route::post('/sendemail/send', 'EmailController@send');



/***** USER Section *****/

Route::get('/admin_manage7081/manage_user', 'admin\UserController@index')->name('manage_user');
Route::get('/admin_manage7081/change_user_status/{id}/{status}', 'admin\UserController@change_user_status')->name('change_user_status');
/***** USER Section *****/

/***** Product Section *****/

Route::get('/products', 'ProductController@index')->name('products');
Route::get('/product/{slug}', 'ProductController@productsdetails')->name('productsdetails');

/***** Cart Section *****/

Route::get('/carts', 'CartController@index')->name('carts')->middleware('auth:front');
Route::post('/addToCart', 'CartController@addToCart')->name('addToCart');//->middleware('auth:front');
Route::post('/quantity_increament','CartController@quantityIncreament')->name('quantityIncreament');
Route::post('/delete_product_from_cart','CartController@deleteProductFromCart')->name('deleteProductFromCart');
Route::post('/add_shipping_charge','CartController@addShippingCharge')->name('addShippingCharge');
Route::post('/cart_order_list', 'CartController@carts')->name('cartOrderList');



